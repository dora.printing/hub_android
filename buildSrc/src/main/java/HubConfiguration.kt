class Android {
    val buildToolsVersion = "27.0.3"
    val minSdkVersion = 21
    val targetSdkVersion = 27
    val compileSdkVersion = 27
    val applicationId = "io.iost.hub.android"
}

class Libs {
    val support = arrayOf("support-v4", "appcompat-v7", "design", "cardview-v7", "gridlayout-v7")
    val kotlin = arrayOf("kotlin-stdlib-jdk8", "kotlin-reflect")
    val anko = arrayOf("anko-sdk25", "anko-sdk25-coroutines", "anko-commons", "anko-appcompat-v7", "anko-appcompat-v7-coroutines", "anko-appcompat-v7-commons", "anko-support-v4", "anko-support-v4-commons", "anko-constraint-layout", "anko-design", "anko-design-coroutines", "anko-recyclerview-v7", "anko-cardview-v7", "anko-gridlayout-v7")
    val okHttp = arrayOf("okhttp", "logging-interceptor")
    val dagger = arrayOf("dagger", "dagger-android", "dagger-android-support")
    val daggerKapt = arrayOf("dagger-compiler", "dagger-android-processor")
    val retrofit = arrayOf("retrofit", "converter-gson")
    val facebook = arrayOf("facebook-core", "facebook-login", "facebook-share")
    val firebase = arrayOf("firebase-core", "firebase-messaging")
}

class Versions {
    val support = "27.1.1"
    val kotlin = "1.2.51"
    val anko = "0.10.5"
    val dagger = "2.12"
    val firebaseCore = "16.0.1"
    val firebaseMessage = "17.3.0"
    val crashlytics = "2.9.5"
    val okHttp = "3.11.0"
    val retrofit2 = "2.4.0"
    val glide = "4.7.1"
    val gson = "2.8.5"
    val immersionBar = "2.3.0"
    val smartRefresh = "1.1.0-alpha-14"
    val leakcanary = "1.6.1"
    val logger = "2.2.0"
    val zendesk = "2.1.1"
}

class HubConfiguration {
    val android = Android()
    val libs = Libs()
    val versions = Versions()
}