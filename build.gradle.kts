// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        maven("https://maven.fabric.io/public")
        maven("https://zendesk.jfrog.io/zendesk/repo")
        google()
        jcenter()
    }

    val config = HubConfiguration()

    dependencies {
        classpath("com.mob.sdk:MobSDK:2018.0319.1724")
        classpath("com.android.tools.build:gradle:3.1.4")
        classpath("com.google.gms:google-services:4.0.1")
        classpath("io.fabric.tools:gradle:1.25.4")
        classpath(kotlin("gradle-plugin", version = config.versions.kotlin))
        classpath(kotlin("android-extensions", version = config.versions.kotlin))
    }
}

allprojects {
    repositories {
        maven("https://maven.google.com")
        google()
        jcenter()
    }
}

task<Delete>("clean") {
    delete(rootProject.buildDir)
}