# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-optimizationpasses 7
-ignorewarnings
-keepattributes *Annotation*
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class **.R$* {
    public static <fields>;
    public static final int *;
}
-keep public class * extends android.app.Fragment {public *;}
-keepclassmembers class * extends android.app.Activity {
   public void *(android.viewCreateOrder.View);
}
-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembernames class kotlinx.** {
    volatile <fields>;
}
-keep class com.google.**
-dontwarn com.google.**
-keep class sun.misc.Unsafe { *; }
-keep class retrofit2.**{*;}
-keep class com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.**

-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn com.dora.pop.model.**
-dontwarn dagger.android.**
-dontwarn kotlin.reflect.**
-dontwarn okhttp3.**
-dontwarn retrofit2.**

-dontnote okhttp3.**, okio.**, retrofit2.**, pl.droidsonroids.**

#极光
-dontoptimize
-dontpreverify
-dontwarn cn.jpush.**
-keep class cn.jpush.** { *; }
-keep class * extends cn.jpush.android.helpers.JPushMessageReceiver { *; }
-dontwarn cn.jiguang.**
-keep class cn.jiguang.** { *; }
-dontwarn com.google.gson.**
-keep class org.apache.http.** { *;}
-keep class com.google.gson.** { *;}
-keep class android.**{*;}
-keep class com.adnroid.**{*;}
-keep class com.google.**{*;}

-keep class io.iost.hub.android.model.** {*;}
-keep class io.iost.hub.android.di.** {*;}
-keep class io.iost.hub.android.corountines.** {*;}
-keep class io.iost.hub.android.widget.**{*;}
-keep class io.iost.hub.android.push.**{*;}
-keep class io.iost.hub.android.extensions.**{*;}
-keep class io.iost.hub.android.ui.**{*;}

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-printmapping proguardMapping.txt
-optimizations !code/simplification/cast,!field/*,!class/merging/*
-keepattributes *Annotation*,InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-dontwarn kotlin.reflect.jvm.internal.**
-keep class kotlin.reflect.jvm.internal.** { *; }
-keep public class kotlin.reflect.jvm.internal.impl.builtins.* { public *; }
-keep class kotlin.reflect.jvm.internal.impl.builtins.BuiltInsLoaderImpl

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep public class com.android.vending.licensing.ILicensingService
-keep class android.support.** {*;}
-keep class org.apache.http.**

#fabric proguard
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keep class com.google.gson.stream.** { *; }
-keep class sun.misc.Unsafe { *; }

-keepclasseswithmembernames class * {
    native <methods>;
}
-keepclassmembers class * extends android.app.Activity{
    public void *(android.view.View);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
-keep class **.R$* {
 *;
}

#----------------------------------------------------------------------------

#---------------------------------webview------------------------------------
-keepclassmembers class fqcn.of.javascript.interface.for.Webview {
   public *;
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
    public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, jav.lang.String);
}

#--------- share sdk ------#
-keep class cn.sharesdk.**{*;}
-keep class com.sina.**{*;}
-keep class **.R$* {*;}
-keep class **.R{*;}
-keep class com.mob.**{*;}
-keep class m.framework.**{*;}
-dontwarn cn.sharesdk.**
-dontwarn com.sina.**
-dontwarn com.mob.**
-dontwarn **.R$*

#refresh layout
-keep class com.scwang.smartrefresh.**{*;}