import com.android.build.gradle.ProguardFiles.getDefaultProguardFile
import com.mob.products.sharesdk.DevInfo
import com.mob.products.sharesdk.InfoNode
import groovy.lang.Closure
import org.apache.commons.io.output.ByteArrayOutputStream
import org.gradle.internal.impldep.com.google.api.client.util.Data.mapOf
import org.gradle.internal.impldep.org.bouncycastle.asn1.iana.IANAObjectIdentifiers.experimental
import org.gradle.internal.impldep.org.junit.experimental.categories.Categories.CategoryFilter.exclude
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.internal.AndroidExtensionsExtension

plugins {
    id("com.android.application")
    id("kotlin-kapt")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("com.mob.sdk")
    id("com.google.gms.google-services")
    id("io.fabric")
}

val config = HubConfiguration()

androidExtensions {
    configure(delegateClosureOf<AndroidExtensionsExtension> {
        isExperimental = true
    })
}

android {

    fun provideVersionCode(): Int {
        val stdout = ByteArrayOutputStream()
        exec {
            commandLine("git", "rev-list", "--first-parent", "--count", "HEAD")
            standardOutput = stdout
        }
        return Integer.parseInt(stdout.toString().trim())
    }


    fun provideVersionName(): String {
        return try {
            val stdout = ByteArrayOutputStream()
            exec {
                commandLine("git", "describe", "--tags", "--dirty")
                standardOutput = stdout
            }
            stdout.toString().trim()
        } catch (e: Exception) {
            "1.0.0"
        }
    }



    compileSdkVersion(config.android.compileSdkVersion)
    buildToolsVersion(config.android.buildToolsVersion)
    defaultConfig {
        applicationId = config.android.applicationId
        minSdkVersion(config.android.minSdkVersion)
        targetSdkVersion(config.android.targetSdkVersion)
        versionCode = provideVersionCode()
        versionName = provideVersionName()
        multiDexEnabled = true
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"

        manifestPlaceholders = mapOf(
                "JPUSH_PKGNAME" to config.android.applicationId,
                "JPUSH_APPKEY" to "88e6f6de56d63d87f37252ad",
                "JPUSH_CHANNEL" to "developer-default"
        )
        resValue("string", "recaptcha_site_key", "6Ldb0HAUAAAAAG2zkQ17v_RQy9EDcb92h1p4AefX")
        resValue("string", "github_client_id", "9e45455b3e83e86554ac")
        resValue("string", "github_client_secret", "ef473a180983ec614090a04fc2e95f6d08d4bb71")
        resValue("string", "reddit_client_id", "Ik_HJKiivt1A5g")
        resValue("string", "reddit_redirect_uri", "iosthub://oauth/callback/reddit")
        resValue("string", "zendesk_app_id", "b52c4f76cc5277c161b5909811f56fa2801e94c9d689496a")
        resValue("string", "zendesk_client_id", "mobile_sdk_client_d8c2c480a6c7b19cc64f")

        setProperty("archivesBaseName", "iost-hub-$versionName")
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file("keystore.jks")
            storePassword = "doraprint"
            keyAlias = "beijing"
            keyPassword = "doraprint"
        }
        create("release") {
            storeFile = file("keystore.jks")
            storePassword = "doraprint"
            keyAlias = "beijing"
            keyPassword = "doraprint"
        }
    }

    buildTypes {
        getByName("debug") {
//            buildConfigField("String", "HOST", "\"http://hub.iost.network/api/\"")
            buildConfigField("String", "HOST", "\"https://hub.iost.io/api/\"")
            signingConfig = signingConfigs.getByName("debug")
        }

        getByName("release") {
            buildConfigField("String", "HOST", "\"https://hub.iost.io/api/\"")
            isZipAlignEnabled = true
            isMinifyEnabled = true
            isShrinkResources = true
            isDebuggable = false
            signingConfig = signingConfigs.getByName("release")
            proguardFile(getDefaultProguardFile("proguard-android.txt"))
            proguardFile(file("proguard-rules.pro"))
        }
    }

    androidExtensions {
        isExperimental = true
    }

    aaptOptions {
        noCompress("json")
    }

    lintOptions {
        isAbortOnError = false
        check("NewApi", "InlinedApi")
    }

    sourceSets {
        getByName("main") {
            java.srcDirs("src/main/java", "src/main/kotlin")
        }
    }
}

MobSDK {
    appKey("2717b5c3da1c3")
    appSecret("11f28aae93ded15dc0ec5edf6c1615a2")

    ShareSDK {
        gui(false)
        methodMissing("version", arrayOf("3.2.0"))
        methodMissing("devInfo", arrayOf(delegateClosureOf<DevInfo> {
            methodMissing("Facebook", arrayOf(delegateClosureOf<InfoNode> {
                methodMissing("appKey", arrayOf("266312700797212"))
                methodMissing("appSecret", arrayOf("84949185121b8cd4415fc567a33e85e1"))
                methodMissing("callbackUri", arrayOf("https://hub.iost.io/api/auth/facebook/callback"))
                methodMissing("ShareByAppClient", arrayOf(true))
            }))
            methodMissing("Twitter", arrayOf(delegateClosureOf<InfoNode> {
                methodMissing("appKey", arrayOf("EUbcaZ5VSE1UKd5SuQZP7SFbn"))
                methodMissing("appSecret", arrayOf("269J1UuXEa3ZO5MQ65FrQmcc9qyc7H7mzjzUxuBj2gF154J46l"))
                methodMissing("callbackUri", arrayOf("http://hub.iost.io/api/auth/twitter/callback"))
                methodMissing("ShareByAppClient", arrayOf(true))
            }))
            methodMissing("Telegram", arrayOf(delegateClosureOf<InfoNode> {
                methodMissing("ShareByAppClient", arrayOf(true))
            }))
            methodMissing("Message", arrayOf(delegateClosureOf<InfoNode> {
            }))
        }))
    }
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    config.libs.support.forEach {
        implementation("com.android.support:$it:${config.versions.support}")
    }

    config.libs.kotlin.forEach {
        implementation("org.jetbrains.kotlin:$it:${config.versions.kotlin}")
    }

    config.libs.anko.forEach {
        implementation("org.jetbrains.anko:$it:${config.versions.anko}")
    }

    config.libs.okHttp.forEach {
        implementation("com.squareup.okhttp3:$it:${config.versions.okHttp}")
    }

    config.libs.retrofit.forEach {
        implementation("com.squareup.retrofit2:$it:${config.versions.retrofit2}")
    }

    config.libs.dagger.forEach {
        implementation("com.google.dagger:$it:${config.versions.dagger}")
    }

    config.libs.daggerKapt.forEach {
        kapt("com.google.dagger:$it:${config.versions.dagger}")
    }

    implementation("com.google.firebase:firebase-core:${config.versions.firebaseCore}")
    implementation("com.google.firebase:firebase-messaging:${config.versions.firebaseMessage}")
    implementation("com.crashlytics.sdk.android:crashlytics:${config.versions.crashlytics}")
    implementation("com.github.bumptech.glide:glide:${config.versions.glide}")
    kapt("com.github.bumptech.glide:compiler:${config.versions.glide}")
    implementation("com.google.code.gson:gson:${config.versions.gson}")
    implementation("com.gyf.barlibrary:barlibrary:${config.versions.immersionBar}")
    implementation("com.scwang.smartrefresh:SmartRefreshLayout:${config.versions.smartRefresh}")
    implementation("com.orhanobut:logger:${config.versions.logger}")
    implementation("com.zendesk:support:${config.versions.zendesk}") {
        exclude(module = "appcompat-v7")
        exclude(module = "support-v4")
        exclude(module = "design")
        exclude(module = "recyclerview-v7")
    }

    implementation("com.google.android.gms:play-services-safetynet:15.0.1")
    implementation("com.jakewharton.retrofit:retrofit2-kotlin-coroutines-experimental-adapter:1.0.0")

    releaseImplementation("com.squareup.leakcanary:leakcanary-android-no-op:${config.versions.leakcanary}")
    debugImplementation("com.squareup.leakcanary:leakcanary-android:${config.versions.leakcanary}")
    debugImplementation("com.squareup.leakcanary:leakcanary-support-fragment:${config.versions.leakcanary}")
    implementation(project(":jpush"))

    implementation("com.android.support:multidex:1.0.3")
    testImplementation("junit:junit:4.12")
    testImplementation("com.android.support.test:rules:1.0.2")

    testImplementation("org.mockito:mockito-core:2.8.47")
    androidTestImplementation("com.android.support.test:runner:1.0.2") {
        exclude("com.android.support", "appcompat-v7")
    }
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
}


