package io.iost.hub.android.upgrade;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.runner.RunWith;

import io.iost.hub.android.ui.feature.navigation.NavigationActivity;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UpgradeCheckerTest {

    @Rule
    public ActivityTestRule<NavigationActivity> mActivityRule = new ActivityTestRule<>(NavigationActivity.class);
}
