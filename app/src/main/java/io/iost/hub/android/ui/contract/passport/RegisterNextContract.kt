package io.iost.hub.android.ui.contract.passport

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface RegisterNextContract {

    interface View : BaseView {
        fun onRegisterSuccess(phone: String, password: String)
    }

    interface Presenter : BasePresenter<View> {
        fun register(phone: String, areaCode: String, password: String, smsCode: String, inviteCode: String, email: String)
    }
}