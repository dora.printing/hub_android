package io.iost.hub.android.ui.base.adapter

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.R
import org.jetbrains.anko.*

class NoneAnkoComponent(override val view: ViewGroup) : AdapterAnkoComponent {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        verticalLayout {
            gravity = Gravity.CENTER_HORIZONTAL
            topPadding = dip(80)
            imageView(R.drawable.default_no_data)
            textView(R.string.no_data_sorry) {
                textSize = 24f
                textColorResource = R.color.black_666666
            }.lparams(wrapContent, wrapContent) {
                topMargin = dimen(R.dimen.dimen_10)
            }
            textView(R.string.no_data_yet) {
                textSizeDimen = R.dimen.material_text_body
                textColorResource = R.color.gray_999999
            }.lparams(wrapContent, wrapContent) {
                topMargin = dimen(R.dimen.dimen_10)
            }
            lparams(matchParent, matchParent)
        }
    }
}