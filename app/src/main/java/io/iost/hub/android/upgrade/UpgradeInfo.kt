package io.iost.hub.android.upgrade

data class UpgradeInfo(
        val isUpdate: Boolean,
        val isManual: Boolean,
        val remoteVersion: String,
        val size: Int,
        val updateTitle: String,
        val updateContent: String,
        val url: String,
        val isForceUpdate: Boolean = false)