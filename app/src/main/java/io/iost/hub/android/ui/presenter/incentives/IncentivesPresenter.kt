package io.iost.hub.android.ui.presenter.incentives

import io.iost.hub.android.R
import io.iost.hub.android.extensions.coroutine
import io.iost.hub.android.model.IncentivesData
import io.iost.hub.android.ui.contract.incentives.IncentivesContract
import io.iost.hub.android.ui.feature.incentives.IncentivesFragment
import kotlinx.coroutines.experimental.Job

class IncentivesPresenter(override val view: IncentivesFragment, override val job: Job) :
        IncentivesContract.Presenter {
    override fun loadData() {
        coroutine({
            ArrayList<IncentivesData>().apply {
                add(IncentivesData(view.getString(R.string.incentives_master_programmer), view.getString(R.string.incentives_master_programmer_content), 0, R.drawable.incentives_master_programmer))
                add(IncentivesData(view.getString(R.string.incentives_youtube_evangelism), view.getString(R.string.incentives_youtube_evangelism_content), 0, R.drawable.incentives_youtube_evangelism))
                add(IncentivesData(view.getString(R.string.incentives_write_about_us), view.getString(R.string.incentives_write_about_us_content), 0, R.drawable.incentives_write_about_us))
                add(IncentivesData(view.getString(R.string.incentives_community_evangelist), view.getString(R.string.incentives_community_evangelist_content), 0, R.drawable.incentives_community_evangelist))
                add(IncentivesData(view.getString(R.string.incentives_more_to_come), view.getString(R.string.incentives_more_to_come_content), 0, R.drawable.incentives_more_to_come))
            }
        }) {
            view.showData(it)
        }

    }

}