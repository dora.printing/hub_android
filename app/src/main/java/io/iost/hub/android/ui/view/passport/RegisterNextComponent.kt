package io.iost.hub.android.ui.view.passport

import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.hideKeyboard
import io.iost.hub.android.extensions.passwordType
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.feature.passport.RegisterNextActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class RegisterNextComponent : ActivityAnkoComponent<RegisterNextActivity> {
    override lateinit var toolbar: Toolbar

    override fun createView(ui: AnkoContext<RegisterNextActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.passport_register
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            imageView(R.drawable.passport_logo).lparams {
                topMargin = dip(25)
                gravity = Gravity.CENTER_HORIZONTAL
            }
            val password = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_password
                passwordType()
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dip(25)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            val passwordConfirm = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_password_confirm
                passwordType()
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            val email = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_email
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            button(R.string.passport_register) {
                padding = 0
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
                singleClick {
                    owner.register(password.content(), passwordConfirm.content(), email.content())
                    email.hideKeyboard()
                    password.hideKeyboard()
                    passwordConfirm.hideKeyboard()
                }
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                topMargin = dimen(R.dimen.dimen_20)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            view().lparams(width = 0, height = 0, weight = 1f)
            view {
                backgroundColorResource = R.color.grey_e8e8e8
            }.lparams(matchParent, dimen(R.dimen.divider))
            textView {
                text = ctx.getText(R.string.passport_to_login)
                textSizeDimen = R.dimen.material_text_body2
                gravity = Gravity.CENTER
                singleClick { startActivity<LoginActivity>() }
            }.lparams(matchParent, dimen(R.dimen.dimen_40))
        }
    }

}