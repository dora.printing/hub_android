package io.iost.hub.android.ui.view.daily

import android.graphics.Color
import android.support.v7.widget.GridLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.*
import io.iost.hub.android.ui.base.fragment.FragmentAnkoComponent
import io.iost.hub.android.widget.DailyCardView
import io.iost.hub.android.widget.DividerItemView
import io.iost.hub.android.widget.dailyCardView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.gridlayout.v7.gridLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView

class DailyComponent : FragmentAnkoComponent {

    lateinit var recyclerView: RecyclerView
    lateinit var rewardTxt: TextView
    lateinit var timeTxt: TextView
    lateinit var tipsImg: ImageView
    lateinit var gridLayout: GridLayout
    lateinit var earnedCard: DailyCardView
    lateinit var rewardCard: DailyCardView
    lateinit var rankingCard: DailyCardView
    lateinit var lifetimeCard: DailyCardView

    override fun createView(ui: AnkoContext<FrameLayout>): View = with(ui) {
        nestedScrollView {
            verticalLayout {
                cardView {
                    radius = dimen(R.dimen.cardview_radius).toFloat()
                    cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
                    setCardBackgroundColor(Color.WHITE)
                    relativeLayout {
                        textView(R.string.daily_reward_today) {
                            gravity = Gravity.CENTER_VERTICAL
                            textSize = 14f
                            textColorResource = R.color.black_666666
                            compoundDrawablePadding = dimen(R.dimen.dimen_8)
                            drawableLeft(R.drawable.daily_total_reward, dimen(R.dimen.dimen_16), dimen(R.dimen.dimen_16))
                        }.lparams {
                            margin = dimen(R.dimen.dimen_16)
                        }
                        rewardTxt = textView {
                            textColorResource = R.color.red_e73f3f
                            textSize = 24f
                            gravity = Gravity.CENTER
                            font(string(R.string.font_heavy))
                            setLineSpacing(10f, 1f)
                        }.lparams {
                            centerInParent()
                        }
                        timeTxt = textView {
                            textColorResource = R.color.black_333333
                            textSize = 14f
                            compoundDrawablePadding = dimen(R.dimen.dimen_8)
                            drawableLeft(R.drawable.daily_count_down, dimen(R.dimen.dimen_16), dimen(R.dimen.dimen_16))
                        }.lparams {
                            centerHorizontally()
                            alignParentBottom()
                            bottomMargin = dimen(R.dimen.dimen_16)
                        }
                        tipsImg = imageView {
                            imageResource = R.drawable.daily_tips
                            padding = dimen(R.dimen.dimen_8)
                            singleClick {
                                showInformation(string(R.string.daily_total_share_content))
                            }
                        }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                            alignParentRight()
                            alignParentBottom()
                            rightMargin = dimen(R.dimen.dimen_8)
                            bottomMargin = dimen(R.dimen.dimen_8)
                        }
                    }
                }.lparams(matchParent, dip(180)) {
                    topMargin = dimen(R.dimen.cardview_margin)
                    leftMargin = dimen(R.dimen.cardview_margin)
                    rightMargin = dimen(R.dimen.cardview_margin)
                }
                gridLayout = gridLayout {
                    earnedCard = dailyCardView {
                        titleTxt.textResource = R.string.daily_points_earned
                        infoClick(string(R.string.daily_points_earned_content))
                    }.lparams(GridLayout.spec(0, 1f), GridLayout.spec(0, 1f)) {
                        width = 0
                        height = 0
                        leftMargin = dimen(R.dimen.dimen_16)
                        topMargin = dimen(R.dimen.dimen_12)
                        rightMargin = dimen(R.dimen.dimen_6)
                    }
                    rewardCard = dailyCardView {
                        titleTxt.textResource = R.string.daily_estimated_reward
                        infoClick(string(R.string.daily_estimated_reward_content))
                    }.lparams(GridLayout.spec(0, 1f), GridLayout.spec(1, 1f)) {
                        width = 0
                        height = 0
                        rightMargin = dimen(R.dimen.dimen_16)
                        topMargin = dimen(R.dimen.dimen_12)
                        leftMargin = dimen(R.dimen.dimen_6)
                    }
                    rankingCard = dailyCardView {
                        titleTxt.textResource = R.string.daily_contribution
                        infoClick(string(R.string.daily_contribution_content))
                        with(contentTxt) {
                            compoundDrawablePadding = dimen(R.dimen.dimen_8)
                        }
                    }.lparams(GridLayout.spec(1, 1f), GridLayout.spec(0, 1f)) {
                        width = 0
                        height = 0
                        topMargin = dimen(R.dimen.dimen_12)
                        bottomMargin = dimen(R.dimen.dimen_16)
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_6)
                    }
                    lifetimeCard = dailyCardView {
                        titleTxt.textResource = R.string.daily_lifetime_reward
                        infoClick(string(R.string.daily_lifetime_reward_content))
                    }.lparams(GridLayout.spec(1, 1f), GridLayout.spec(1, 1f)) {
                        width = 0
                        height = 0
                        topMargin = dimen(R.dimen.dimen_12)
                        bottomMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                        leftMargin = dimen(R.dimen.dimen_6)
                    }
                }.lparams(matchParent, dip(300))
                textView(R.string.daily_quests) {
                    textSize = 18f
                    textColorResource = R.color.black
                }.lparams {
                    topMargin = dip(2)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
                cardView {
                    radius = dimen(R.dimen.cardview_radius).toFloat()
                    cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
                    recyclerView = recyclerView {
                        isFocusableInTouchMode = false
                        isNestedScrollingEnabled = false
                        layoutManager = LinearLayoutManager(context)
                        addItemDecoration(DividerItemView(ctx, ctx.drawable(R.color.gray_e5e5e5), dimen(R.dimen.divider)))
                    }.lparams(matchParent, wrapContent)
                }.lparams(matchParent, wrapContent) {
                    topMargin = dimen(R.dimen.cardview_margin_mid)
                    bottomMargin = dimen(R.dimen.cardview_margin)
                    leftMargin = dimen(R.dimen.cardview_margin)
                    rightMargin = dimen(R.dimen.cardview_margin)
                }
            }.lparams(matchParent, wrapContent)
            lparams(matchParent, matchParent)
        }
    }

}
