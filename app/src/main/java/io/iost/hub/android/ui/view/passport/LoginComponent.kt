package io.iost.hub.android.ui.view.passport

import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.passwordType
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.feature.passport.RegisterActivity
import io.iost.hub.android.ui.feature.passport.ResetPasswordActivity
import org.jetbrains.anko.*

class LoginComponent : ActivityAnkoComponent<LoginActivity> {

    override val toolbar: Toolbar? = null
    lateinit var statusView: View
    lateinit var countryTxt: TextView
    lateinit var phoneNumEdt: EditText
    lateinit var passwordEdt: EditText

    override fun createView(ui: AnkoContext<LoginActivity>): View = with(ui) {
        verticalLayout {
            statusView = view().lparams(matchParent, dip(0))
            imageView(R.drawable.alert_close) {
                padding = dimen(R.dimen.dimen_8)
                singleClick { owner.onBackPressed() }
            }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                margin = dimen(R.dimen.dimen_8)
            }
            imageView(R.drawable.passport_logo).lparams {
                topMargin = dip(80)
                gravity = Gravity.CENTER_HORIZONTAL
            }
            linearLayout {
                backgroundResource = R.drawable.bg_edittext
                countryTxt = textView {
                    textSizeDimen = R.dimen.material_text_body1
                    textColorResource = R.color.blue_0091dc
                    gravity = Gravity.CENTER
                    singleClick { owner.onCountryClick() }
                }.lparams(width = 0, height = matchParent, weight = 1f)
                view {
                    backgroundColorResource = R.color.grey_e8e8e8
                }.lparams(dimen(R.dimen.divider), matchParent) {
                    topMargin = dimen(R.dimen.dimen_4)
                    bottomMargin = dimen(R.dimen.dimen_4)
                }
                phoneNumEdt = editText {
                    background = null
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_PHONE
                    hintResource = R.string.passport_phone
                    textSizeDimen = R.dimen.material_text_body1
                    setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                }.lparams(width = 0, height = dimen(R.dimen.edit_height), weight = 3f)
            }.lparams(width = matchParent, height = dimen(R.dimen.edit_height)) {
                topMargin = dip(25)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }

            passwordEdt = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_password
                passwordType()
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            textView(R.string.passport_password_forget) {
                textColorResource = R.color.blue_0091dc
                textSizeDimen = R.dimen.material_text_body2
                singleClick {
                    startActivity<ResetPasswordActivity>(LoginActivity.PHONE_NUMBER to phoneNumEdt.content())
                }
            }.lparams {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                gravity = Gravity.RIGHT
            }
            button(R.string.passport_login) {
                padding = 0
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
                singleClick { owner.login(phoneNumEdt.content(), passwordEdt.content()) }
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                topMargin = dimen(R.dimen.dimen_20)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            view().lparams(width = 0, height = 0, weight = 1f)
            view {
                backgroundColorResource = R.color.grey_e8e8e8
            }.lparams(matchParent, dimen(R.dimen.divider))
            textView {
                text = ctx.getText(R.string.passport_to_register)
                textSizeDimen = R.dimen.material_text_body2
                gravity = Gravity.CENTER
                singleClick { startActivity<RegisterActivity>() }
            }.lparams(matchParent, dimen(R.dimen.dimen_40))
        }
    }

}