package io.iost.hub.android.ui.view.daily

import android.support.v7.widget.Toolbar
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.daily.OAuthActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout

class OAuthComponent : ActivityAnkoComponent<OAuthActivity> {
    override lateinit var toolbar: Toolbar
    override fun createView(ui: AnkoContext<OAuthActivity>) = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.daily_oauth_verify
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
        }
    }

}