package io.iost.hub.android.ui.view.webview

import android.support.v7.widget.Toolbar
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.webview.WebviewActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.webView

class WebviewComponent : ActivityAnkoComponent<WebviewActivity> {
    override lateinit var toolbar: Toolbar
    lateinit var webview: WebView
    override fun createView(ui: AnkoContext<WebviewActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.iost_upper
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            webview = webView {
                //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
                settings.javaScriptEnabled = true
                //设置自适应屏幕，两者合用
                settings.useWideViewPort = true //将图片调整到适合webview的大小
                settings.loadWithOverviewMode = true // 缩放至屏幕的大小

                //缩放操作
                settings.setSupportZoom(true) //支持缩放，默认为true。是下面那个的前提。
                settings.builtInZoomControls = true //设置内置的缩放控件。若为false，则该WebView不可缩放
                settings.displayZoomControls = false //隐藏原生的缩放控件

                //其他细节操作
//                settings.cacheMode = settings.LOAD_CACHE_ELSE_NETWORK //关闭webview中缓存
                settings.allowFileAccess = true //设置可以访问文件
                settings.javaScriptCanOpenWindowsAutomatically = true //支持通过JS打开新窗口
                settings.loadsImagesAutomatically = true //支持自动加载图片
                settings.defaultTextEncodingName = "utf-8"//设置编码格式
                settings.domStorageEnabled = true

                webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(webView: WebView, s: String): Boolean {
                        webView.loadUrl(s)
                        return true
                    }
                }
            }.lparams(matchParent, matchParent)
        }
    }
}