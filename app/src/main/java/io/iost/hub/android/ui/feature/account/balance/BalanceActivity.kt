package io.iost.hub.android.ui.feature.account.balance

import android.app.Activity
import android.content.Intent
import io.iost.hub.android.R
import io.iost.hub.android.extensions.decimal
import io.iost.hub.android.model.BalanceItemData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.account.BalanceContract
import io.iost.hub.android.ui.presenter.account.BalancePresenter
import io.iost.hub.android.ui.view.account.BalanceComponent
import org.jetbrains.anko.startActivityForResult

class BalanceActivity : BasePresenterActivity<BalancePresenter, BalanceComponent>(), BalanceContract.View {

    private companion object {
        const val WITHDRAW_REQUEST = 101
        const val WITHDRAW_RULES_REQUEST = 102
    }

    override val ui: BalanceComponent = BalanceComponent()

    private val mAdapter = BalanceAdapter()
    private var mIsWithdrawn = false

    override fun created() {
        with(ui) {
            refreshLayout.setOnRefreshListener {
                mPresenter.getBalanceData()
                mPresenter.getBalanceRecord()
            }
            loadMoreLayout.setOnLoadMoreListener {
                mPresenter.loadMoreRecord()
            }
            recyclerView.adapter = mAdapter
        }
        mPresenter.loadIsWithdrawn()
    }

    override fun loadData(): Boolean {
        mPresenter.getBalanceRecord()
        mPresenter.getBalanceData()
        return true
    }

    override fun showBalance(balance: Float) {
        ui.balanceCountTxt.text = balance.decimal(2)
    }

    override fun setBalanceList(list: List<BalanceItemData>) {
        mAdapter.items = list
        ui.refreshLayout.finishRefresh()
    }

    override fun loadMore(list: List<BalanceItemData>) {
        mAdapter.loadMore(list)
        ui.loadMoreLayout.finishLoadMore()
    }

    override fun isWithdrawn(isWithdrawn: Boolean) {
        mIsWithdrawn = isWithdrawn
    }

    fun startWithdraw() {
        if (mIsWithdrawn) {
            startActivityForResult<WithdrawActivity>(WITHDRAW_REQUEST)
        } else {
            startActivityForResult<WithdrawRulesActivity>(WITHDRAW_RULES_REQUEST)
            overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                WITHDRAW_REQUEST -> {
                    showLoadingLayout()
                    loadData()
                }
                WITHDRAW_RULES_REQUEST -> {
                    startActivityForResult<WithdrawActivity>(WITHDRAW_REQUEST)
                }
            }
        }
    }

}