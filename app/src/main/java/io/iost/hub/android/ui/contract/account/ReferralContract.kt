package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.model.PosterData
import io.iost.hub.android.model.ReferralData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface ReferralContract {
    interface View : BaseView {
        fun showInviteRewards(referredNum: Int, inviteReward: Int)
        fun showReferralList(list: List<ReferralData>)
        fun showPostAlert(list: ArrayList<PosterData>)
    }

    interface Presenter : BasePresenter<View> {
        fun getInviteRewards()
        fun getReferralData()
        fun getPosterData()
    }
}