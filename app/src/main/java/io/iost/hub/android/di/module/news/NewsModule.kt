package io.iost.hub.android.di.module.news

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.FragmentScope
import io.iost.hub.android.di.module.FragmentModule
import io.iost.hub.android.ui.feature.news.NewsFragment
import io.iost.hub.android.ui.presenter.news.NewsPresenter
import kotlinx.coroutines.experimental.Job

@Module
class NewsModule : FragmentModule() {

    @FragmentScope
    @Provides
    fun providePresenter(view: NewsFragment, job: Job, api: HubApiService): NewsPresenter = NewsPresenter(view, job, api)

}