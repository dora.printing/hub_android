package io.iost.hub.android.ui.presenter.passport

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.passport.ResetPasswordContract
import kotlinx.coroutines.experimental.Job

class ResetPasswordPresenter(override val view: ResetPasswordContract.View, override val job: Job, private val api: HubApiService) :
        ResetPasswordContract.Presenter {

    override fun resetPassword(phone: String, areaCode: String, password: String, smsCode: String) {
        callApi(job, api.resetPassword(phone, areaCode, password, smsCode), view) {
            if (it is DataResult) {
                view.resetSuccess(phone)
            }
        }
    }

}