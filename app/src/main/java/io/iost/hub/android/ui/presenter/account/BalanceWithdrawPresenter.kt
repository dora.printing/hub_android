package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.BalanceWithdrawContract
import kotlinx.coroutines.experimental.Job

class BalanceWithdrawPresenter(override val view: BalanceWithdrawContract.View, override val job: Job, private val api: HubApiService) :
        BalanceWithdrawContract.Presenter {

    private var mPage = 1
    private val mLimit = 20

    override fun getWithdrawRecord() {
        mPage = 1
        loadBalanceRecord()
    }

    override fun loadMoreRecord() {
        loadBalanceRecord()
    }

    private fun loadBalanceRecord() {
        callApi(api.getBalanceRecord(mPage, mLimit, "drawed")) {
            when (it) {
                is DataResult -> {
                    val items = it.data.items
                    if (mPage == 1) {
                        view.setWithdrawList(it.data.items)
                        if (it.data.items.isNotEmpty())
                            callApi(job, api.balanceAllRead(items[items.size - 1].id)) { }
                    } else {
                        view.onLoadMoreFinish(it.data.items)
                    }
                    if (it.data.items.isNotEmpty()) mPage++
                }
            }
        }
    }


}