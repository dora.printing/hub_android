package io.iost.hub.android.ui.contract.news

import io.iost.hub.android.model.Banner
import io.iost.hub.android.model.NewsData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface NewsContract {
    interface View : BaseView {
        fun showBanner(banners: List<Banner>)
        fun setNewsList(newsList: List<NewsData>)
        fun loadMoreNews(newsList: List<NewsData>)
    }

    interface Presenter : BasePresenter<View> {
        fun getBanner()
        fun getNewsData()
        fun loadMoreNews()
    }
}