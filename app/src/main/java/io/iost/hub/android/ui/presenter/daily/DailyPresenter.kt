package io.iost.hub.android.ui.presenter.daily

import android.os.CountDownTimer
import io.iost.hub.android.GlobalConfig.COUNT_SECOND
import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.model.MessageResult
import io.iost.hub.android.ui.contract.daily.DailyContract
import io.iost.hub.android.ui.feature.daily.DailyFragment
import io.iost.hub.android.ui.feature.passport.BasePassportActivity
import io.iost.hub.android.ui.presenter.BasePresenter
import io.iost.hub.android.ui.view.daily.DailyCheckInComponent
import io.iost.hub.android.widget.shareAlert
import kotlinx.coroutines.experimental.Job
import java.text.SimpleDateFormat
import java.util.*

class DailyPresenter(override val view: DailyContract.View, override val job: Job, private val api: HubApiService) :
        DailyContract.Presenter {

    override fun checkInInfo() {
        callApi(api.getCheckIn()) {
            when (it) {
                is DataResult -> {
                    view.showCheckIn(it.data)
                }
            }
        }
    }

    override fun getDailyData() {
        callApi(api.getDailyTasks()) {
            view.dismissLoading()
            when (it) {
                is DataResult -> {
                    view.showData(it.data)
                }
            }
        }
    }

    override fun getDailyCardData() {
        callApi(api.getDailyReward()) {
            when (it) {
                is DataResult -> {
                    view.showCardData(it.data)
                }
            }
        }
    }

    override fun checkIn(component: DailyCheckInComponent) {
        view.showLoadingDialog()
        callApi(job, api.checkIn()) {
            when (it) {
                is DataResult -> {
                    component.checkInView.setLevels(it.data.reward)
                    component.checkInBtn.isEnabled = false
                    view.viewCtx.shareAlert(true) {
                        code = (view as DailyFragment).userState.getCode()
                        setTaskTxt(view.viewCtx.getString(R.string.daily_task_check_in), it.data.reward, 0)
                    }.show()
                    getDailyCardData()
                }
                is MessageResult -> {
                    component.checkInBtn.isEnabled = false
                }
            }
            view.dismissLoading()
        }
    }

    private var mTimer: CountDownTimer? = null

    override fun countDown() {
        if (mTimer == null) {
            val midnight = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            midnight.time = Date()
            midnight.add(Calendar.DAY_OF_MONTH, 1)
            midnight.set(Calendar.HOUR_OF_DAY, 0)
            midnight.set(Calendar.MINUTE, 0)
            midnight.set(Calendar.SECOND, 0)
            midnight.set(Calendar.MILLISECOND, 0)
            val time = (midnight.time.time - Date().time)
            mTimer = object : CountDownTimer(time, COUNT_SECOND) {
                override fun onFinish() {
                    mTimer = null
                    countDown()
                }

                override fun onTick(millisUntilFinished: Long) {
                    val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
                    dateFormat.timeZone = TimeZone.getTimeZone("GMT")
                    view.onCountDown(dateFormat.format(millisUntilFinished))
                }

            }
        }
        mTimer?.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        mTimer?.cancel()
        mTimer = null
    }

}