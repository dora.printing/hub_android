package io.iost.hub.android.di.module

import dagger.Module
import dagger.Provides
import io.iost.hub.android.di.FragmentScope
import kotlinx.coroutines.experimental.Job

@Module
open class FragmentModule {

    @FragmentScope
    @Provides
    fun provideJob(): Job = Job()

}