package io.iost.hub.android.ui.feature.account.referral

import io.iost.hub.android.R
import io.iost.hub.android.model.PosterData
import io.iost.hub.android.model.ReferralData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.account.ReferralContract
import io.iost.hub.android.ui.presenter.account.ReferralPresenter
import io.iost.hub.android.ui.view.account.ReferralComponent
import io.iost.hub.android.widget.posterAlert

class ReferralActivity : BasePresenterActivity<ReferralPresenter, ReferralComponent>(), ReferralContract.View {

    override val ui: ReferralComponent = ReferralComponent()

    override fun created() {
        ui.refreshLayout.setOnRefreshListener {
            mPresenter.getReferralData()
            mPresenter.getInviteRewards()
        }
    }

    override fun loadData(): Boolean {
        mPresenter.getReferralData()
        mPresenter.getInviteRewards()
        return true
    }

    override fun showInviteRewards(referredNum: Int, inviteReward: Int) {
        with(ui) {
            referredTxt.text = referredNum.toString()
            inviteTxt.text = getString(R.string.account_invite_to_get, inviteReward)
        }
    }

    override fun showReferralList(list: List<ReferralData>) {
        val mAdapter = ReferralAdapter()
        ui.recyclerView.adapter = mAdapter
        mAdapter.items = list
        ui.refreshLayout.finishRefresh()
    }

    override fun showPostAlert(list: ArrayList<PosterData>) {
        posterAlert {
            code = userState.getCode()
            setPosterData(list)
        }.show()
    }

    fun sharePostAlert() {
        mPresenter.getPosterData()
    }


}