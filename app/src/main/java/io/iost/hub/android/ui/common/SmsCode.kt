package io.iost.hub.android.ui.common

import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.safetynet.SafetyNet
import com.orhanobut.logger.Logger
import io.iost.hub.android.GlobalConfig.COUNT_SECOND
import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.BaseView
import kotlinx.coroutines.experimental.Job

class SmsCode(private val view: BaseView, private val job: Job, private val api: HubApiService) {

    companion object {
        const val REGISTER_TYPE = "reg"
        const val WITHDRAW_TYPE = "withdraw"
        const val RESET_PASSWORD_TYPE = "reset"

        private const val COUNT_DOWN_TIME = 60_000L
    }

    private var mTimer: CountDownTimer? = null
    private var mCallback: SmsCodeCallback? = null

    fun getVerifyCode(phone: String, areaCode: String, type: String) {
        if (phone.isEmpty()) {
            view.toastMsg(R.string.passport_phone_empty)
            return
        }
        if (view is AppCompatActivity) {
            val act = view as AppCompatActivity
            SafetyNet.getClient(act).verifyWithRecaptcha(act.getString(R.string.recaptcha_site_key))
                    .addOnSuccessListener(act) { response ->
                        val token = response.tokenResult
                        if (!token.isEmpty()) {
                            getSmsCode(phone, areaCode, type, token)
                        }
                    }
                    .addOnFailureListener(act) { e ->
                        if (e is ApiException) {
                            val statusCode = e.statusCode
                            Logger.i("Error: " + CommonStatusCodes.getStatusCodeString(statusCode))
                        } else {
                            Logger.i("Error: " + e.message)
                        }
                        view.toastMsg(R.string.loading_problem_network)
                    }
        }

    }

    fun getSmsCode(phone: String, areaCode: String, type: String, googleCaptcha: String = "") {
        view.showLoadingDialog()
        callApi(job, api.sendSms(phone, areaCode, type, googleCaptcha), view) {
            when (it) {
                is DataResult -> {
                    countDown()
                    view.toastMsg(R.string.passport_sms_code_sent)
                }
            }
            view.dismissLoading()
        }
    }

    private fun countDown() {
        if (mTimer == null) {
            mTimer = object : CountDownTimer(COUNT_DOWN_TIME, COUNT_SECOND) {
                override fun onFinish() {
                    mCallback?.onCountDown(true)
                    mTimer = null
                }

                override fun onTick(millisUntilFinished: Long) {
                    mCallback?.onCountDown(false, (millisUntilFinished / COUNT_SECOND).toInt())
                }

            }
        }
        mTimer?.start()
    }

    fun registerCallback(callback: SmsCodeCallback) {
        this.mCallback = callback
    }

    fun unRegisterCallback() {
        mTimer?.cancel()
        mTimer = null
        mCallback = null
    }

    interface SmsCodeCallback {
        fun onCountDown(isFinish: Boolean, time: Int = 0)
    }
}