package io.iost.hub.android.ui.feature.account.social

import io.iost.hub.android.R
import io.iost.hub.android.model.SocialData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.common.DailyTask
import io.iost.hub.android.ui.contract.account.SocialContract
import io.iost.hub.android.ui.presenter.account.SocialPresenter
import io.iost.hub.android.ui.view.account.SocialComponent
import org.jetbrains.anko.toast
import javax.inject.Inject

class SocialActivity : BasePresenterActivity<SocialPresenter, SocialComponent>(), SocialContract.View {

    override val ui: SocialComponent = SocialComponent()

    @Inject
    lateinit var dailyTask: DailyTask

    private val mAdapter by lazy {
        SocialAdapter {
            when (it) {
                SocialAdapter.TELEGRAM -> {
                    dailyTask.verifyTelegram(this)
                }

                SocialAdapter.GITHUB -> {
                    dailyTask.verifyGitHub(this)
                }

                SocialAdapter.REDDIT -> {
                    dailyTask.verifyReddit(this)
                }

                SocialAdapter.TWITTER -> {
                    dailyTask.verifyTwitter(this)
                }

                else -> {
                    toast(R.string.verify_unsupport_msg)
                }
            }
        }
    }

    override fun created() {
    }

    override fun onResume() {
        super.onResume()
        mPresenter.getSocialAccount()
    }

    override fun showSocialAccount(data: List<SocialData>) {
        ui.recyclerView.adapter = mAdapter
        mAdapter.items = data
    }

}