package io.iost.hub.android.di.module

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.upgrade.UpgradeCheckInteractor
import io.iost.hub.android.upgrade.UpgradeCheckInteractorImp
import io.iost.hub.android.upgrade.UpgradeChecker
import javax.inject.Singleton

@Module
class UpgradeModule {

    @Provides
    @Singleton
    fun provideUpgradeCheckInteractor(hubApiService: HubApiService): UpgradeCheckInteractor = UpgradeCheckInteractorImp(hubApiService)

    @Provides
    @Singleton
    fun provideUpgradeChecker(upgradeCheckInteractor: UpgradeCheckInteractor): UpgradeChecker = UpgradeChecker(upgradeCheckInteractor)
}