package io.iost.hub.android.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.component.ActivitySubComponent
import io.iost.hub.android.di.module.account.*
import io.iost.hub.android.di.module.daily.DailyQuestModule
import io.iost.hub.android.di.module.navigation.NavigationModule
import io.iost.hub.android.di.module.passport.LoginModule
import io.iost.hub.android.di.module.passport.RegisterModule
import io.iost.hub.android.di.module.passport.RegisterNextModule
import io.iost.hub.android.di.module.passport.ResetPasswordModule
import io.iost.hub.android.ui.feature.account.aboutus.AboutUsActivity
import io.iost.hub.android.ui.feature.account.balance.BalanceActivity
import io.iost.hub.android.ui.feature.account.balance.BalanceWithdrawActivity
import io.iost.hub.android.ui.feature.account.balance.WithdrawActivity
import io.iost.hub.android.ui.feature.account.referral.ReferralActivity
import io.iost.hub.android.ui.feature.account.setting.SettingActivity
import io.iost.hub.android.ui.feature.account.social.SocialActivity
import io.iost.hub.android.ui.feature.daily.DailyQuestActivity
import io.iost.hub.android.ui.feature.daily.OAuthActivity
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import io.iost.hub.android.ui.feature.passport.*

@Module(subcomponents = [ActivitySubComponent::class])
abstract class AllActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [NavigationModule::class])
    internal abstract fun contributeNavigationActivityInjector(): NavigationActivity

    //Passport
    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginModule::class])
    internal abstract fun contributeLoginActivityInjector(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [RegisterModule::class])
    internal abstract fun contributeRegisterActivityInjector(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [RegisterNextModule::class])
    internal abstract fun contributeRegisterNextActivityInjector(): RegisterNextActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ResetPasswordModule::class])
    internal abstract fun contributeResetPasswordActivityInjector(): ResetPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector()
    internal abstract fun contributeAreaActivityInjector(): AreaActivity

    //account
    @ActivityScope
    @ContributesAndroidInjector(modules = [BalanceModule::class])
    internal abstract fun contributesBalanceActivityInjector(): BalanceActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [BalanceWithdrawModule::class])
    internal abstract fun contributeBalanceWithdrawActivityInjector(): BalanceWithdrawActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [WithDrawModule::class])
    internal abstract fun contributeWithdrawActivityInjector(): WithdrawActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ReferralModule::class])
    internal abstract fun contributesReferralActivityInjector(): ReferralActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SocialModule::class])
    internal abstract fun contributesSocialActivityInjector(): SocialActivity

    @ActivityScope
    @ContributesAndroidInjector()
    internal abstract fun contributesSettingActivityInjector(): SettingActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AboutUsModule::class])
    internal abstract fun contributesAboutUsActivityInjector(): AboutUsActivity

    //Daily
    @ActivityScope
    @ContributesAndroidInjector(modules = [DailyQuestModule::class])
    internal abstract fun contributesDailyQuestActivityInjector(): DailyQuestActivity

    @ActivityScope
    @ContributesAndroidInjector()
    internal abstract fun contributesOAuthActivityInjector(): OAuthActivity

}