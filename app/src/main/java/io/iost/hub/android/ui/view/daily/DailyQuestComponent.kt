package io.iost.hub.android.ui.view.daily

import android.support.v7.widget.Toolbar
import android.widget.Button
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.daily.DailyQuestActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class DailyQuestComponent : ActivityAnkoComponent<DailyQuestActivity> {

    override lateinit var toolbar: Toolbar

    lateinit var nameTxt: TextView
    lateinit var rewardsTxt: TextView
    lateinit var redemptionTxt: TextView
    lateinit var howTodoTxt: TextView
    lateinit var rulesTxt: TextView
    lateinit var buttonTxt: Button

    override fun createView(ui: AnkoContext<DailyQuestActivity>) = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.daily_quests_content
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            scrollView {
                isVerticalScrollBarEnabled = false
                verticalLayout {
                    nameTxt = textView {
                        textSize = 18f
                        textColorResource = R.color.black_333333
                    }.lparams(matchParent, wrapContent) {
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                        topMargin = dimen(R.dimen.dimen_16)
                    }
                    rewardsTxt = textView {
                        textSize = 13f
                        textColorResource = R.color.black_666666
                    }.lparams(matchParent, wrapContent) {
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                        topMargin = dimen(R.dimen.dimen_10)
                    }
                    redemptionTxt = textView {
                        textSize = 13f
                        textColorResource = R.color.black_666666
                    }.lparams(matchParent, wrapContent) {
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                        topMargin = dimen(R.dimen.dimen_4)
                        bottomMargin = dimen(R.dimen.dimen_16)
                    }
                    view { backgroundColorResource = R.color.gray_d8d8d8 }.lparams(matchParent, dimen(R.dimen.divider))
                    textView(R.string.daily_quests_how_todo) {
                        textSize = 16f
                        singleLine = true
                        textColorResource = R.color.black_333333
                    }.lparams(matchParent, wrapContent) {
                        topMargin = dimen(R.dimen.dimen_16)
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                    }
                    howTodoTxt = textView {
                        textSize = 13f
                        setLineSpacing(0f, 1.2f)
                        textColorResource = R.color.black_666666
                    }.lparams(matchParent, wrapContent) {
                        topMargin = dimen(R.dimen.dimen_10)
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                        bottomMargin = dimen(R.dimen.dimen_16)
                    }
                    view {
                        backgroundColorResource = R.color.gray_d8d8d8
                    }.lparams(matchParent, dimen(R.dimen.divider)) {
                        leftMargin = dimen(R.dimen.dimen_16)
                    }
                    textView(R.string.daily_quests_rules) {
                        textSize = 16f
                        singleLine = true
                        textColorResource = R.color.black_333333
                    }.lparams(matchParent, wrapContent) {
                        topMargin = dimen(R.dimen.dimen_16)
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                    }
                    rulesTxt = textView {
                        textSize = 13f
                        setLineSpacing(0f, 1.2f)
                        textColorResource = R.color.black_666666
                    }.lparams(matchParent, wrapContent) {
                        topMargin = dimen(R.dimen.dimen_10)
                        leftMargin = dimen(R.dimen.dimen_16)
                        rightMargin = dimen(R.dimen.dimen_16)
                    }
                }
            }.lparams(matchParent, 0, 1f)
            buttonTxt = button {
                padding = 0
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_blue_button
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                margin = dimen(R.dimen.dimen_16)
            }
        }
    }

}