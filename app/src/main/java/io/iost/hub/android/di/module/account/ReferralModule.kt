package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.referral.ReferralActivity
import io.iost.hub.android.ui.presenter.account.ReferralPresenter
import kotlinx.coroutines.experimental.Job

@Module
class ReferralModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: ReferralActivity, job: Job, api: HubApiService) = ReferralPresenter(view, job, api)
}