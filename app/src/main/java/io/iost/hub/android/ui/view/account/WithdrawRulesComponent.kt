package io.iost.hub.android.ui.view.account

import android.support.v7.widget.Toolbar
import android.widget.Button
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.balance.WithdrawRulesActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.collections.forEachWithIndex

class WithdrawRulesComponent : ActivityAnkoComponent<WithdrawRulesActivity> {

    private val mRulesList = arrayOf(
            R.string.account_withdrawal_rules_1,
            R.string.account_withdrawal_rules_2,
            R.string.account_withdrawal_rules_3,
            R.string.account_withdrawal_rules_4,
            R.string.account_withdrawal_rules_5)

    override lateinit var toolbar: Toolbar
    lateinit var readBtn: Button

    override fun createView(ui: AnkoContext<WithdrawRulesActivity>) = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                navigationIconResource = R.drawable.alert_close
                titleResource = R.string.account_withdraw_rules
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize)) {
                bottomMargin = dip(5)
            }
            mRulesList.forEachWithIndex { i, resId ->
                linearLayout {
                    textView("${i + 1}.") {
                        minWidth = dimen(R.dimen.dimen_16)
                        textSize = 13f
                        textColorResource = if (i == 0) R.color.red_b81414 else R.color.black_333333
                    }
                    textView(resId) {
                        textSize = 13f
                        textColorResource = if (i == 0) R.color.red_b81414 else R.color.black_333333
                    }
                }.lparams(matchParent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
            }
            view().lparams(0, 0, 1f)
            readBtn = button(R.string.account_have_read) {
                padding = dimen(R.dimen.dimen_4)
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
                isEnabled = false
                singleClick { owner.startWithdrawAct() }
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                margin = dimen(R.dimen.dimen_16)
            }
        }
    }

}