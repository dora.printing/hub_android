package io.iost.hub.android.push

interface IMessage {

    fun convert(any: Any): Message
}