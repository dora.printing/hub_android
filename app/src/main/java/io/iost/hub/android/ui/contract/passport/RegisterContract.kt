package io.iost.hub.android.ui.contract.passport

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface RegisterContract {

    interface View : BaseView {
        fun onNext(phone: String, code: String, inviteCode: String)
    }

    interface Presenter : BasePresenter<View> {
        fun next(phone: String, areaCode: String, code: String, inviteCode: String)
    }
}