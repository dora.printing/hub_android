package io.iost.hub.android.ui.feature.account.balance

import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import io.iost.hub.android.model.BalanceItemData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.account.BalanceWithdrawContract
import io.iost.hub.android.ui.presenter.account.BalanceWithdrawPresenter
import io.iost.hub.android.ui.view.account.BalanceWithdrawComponent

class BalanceWithdrawActivity : BasePresenterActivity<BalanceWithdrawPresenter, BalanceWithdrawComponent>(),
        BalanceWithdrawContract.View {

    override val ui: BalanceWithdrawComponent = BalanceWithdrawComponent()

    private val mAdapter = BalanceAdapter()

    override fun created() {
        with(ui) {
            refreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
                override fun onRefresh(refreshLayout: RefreshLayout) {
                    mPresenter.getWithdrawRecord()
                }

                override fun onLoadMore(refreshLayout: RefreshLayout) {
                    mPresenter.loadMoreRecord()
                }
            })
            recyclerView.adapter = mAdapter
        }
    }

    override fun loadData(): Boolean {
        mPresenter.getWithdrawRecord()
        return true
    }

    override fun setWithdrawList(list: List<BalanceItemData>) {
        mAdapter.items = list
        ui.refreshLayout.finishRefresh()
    }

    override fun onLoadMoreFinish(list: List<BalanceItemData>) {
        mAdapter.loadMore(list)
        ui.refreshLayout.finishLoadMore()
    }

}