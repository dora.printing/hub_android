package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.model.BalanceItemData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface BalanceWithdrawContract {
    interface View : BaseView {
        fun setWithdrawList(list: List<BalanceItemData>)
        fun onLoadMoreFinish(list: List<BalanceItemData>)
    }

    interface Presenter : BasePresenter<View> {
        fun getWithdrawRecord()
        fun loadMoreRecord()
    }
}