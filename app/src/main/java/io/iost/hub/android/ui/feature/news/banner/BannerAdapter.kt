package io.iost.hub.android.ui.feature.news.banner

import android.graphics.Typeface
import android.support.v4.view.PagerAdapter
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.R
import io.iost.hub.android.extensions.loadUrl
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.model.Banner
import io.iost.hub.android.ui.feature.webview.WebviewActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class BannerAdapter(private val mBanners: List<Banner>) : PagerAdapter() {
    override fun isViewFromObject(view: View, obj: Any) = view == obj

    override fun getCount() = mBanners.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val banner = mBanners[position]
        return with(container.context) {
            cardView {
                radius = dimen(R.dimen.cardview_radius).toFloat()
                cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
                verticalLayout {
                    imageView {
                        banner.converImage?.let {
                            loadUrl(it)
                        }
                        singleClick {
                            startActivity<WebviewActivity>(WebviewActivity.WEB_URL to banner.link, WebviewActivity.WEB_TITLE to banner.title)
                        }
                    }.lparams(width = matchParent, height = 0, weight = 11f)
                    textView(banner.title) {
                        gravity = Gravity.CENTER_VERTICAL
                        textSize = 14f
                        textColorResource = R.color.black_333333
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(width = matchParent, height = 0, weight = 7f) {
                        leftMargin = dip(10)
                        rightMargin = dip(10)
                    }
                }
            }
        }.apply {
            container.addView(this)
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }
}