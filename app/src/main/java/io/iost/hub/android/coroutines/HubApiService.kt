package io.iost.hub.android.coroutines

import io.iost.hub.android.model.*
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.*

interface HubApiService {

    @GET("news/features")
    fun getFeatures(): Deferred<ResponseData<List<Banner>>>

    @GET("news/news")
    fun getNews(@Query("page") page: Int,
                @Query("limit") limit: Int): Deferred<ResponseData<List<NewsData>>>

    @GET("users/checkIn")
    fun getCheckIn(): Deferred<ResponseData<CheckInData>>

    @POST("users/checkIn")
    fun checkIn(): Deferred<ResponseData<CheckIn>>

    @GET("users/tasks")
    fun getDailyTasks(): Deferred<ResponseData<List<DailyData>>>

    @GET("users/tasks/{id}")
    fun getTaskById(@Path("id") mId: Int): Deferred<ResponseData<DailyData>>

    @GET("users/rewardNums")
    fun getDailyReward(): Deferred<ResponseData<DailyCardData>>

    @FormUrlEncoded
    @POST("users/drawTask/{id}")
    fun drawTask(@Path("id") mId: String, @Field("id") id: String): Deferred<ResponseData<DrawTaskData>>

    @GET("users/balance")
    fun getBalance(): Deferred<ResponseData<BalanceData>>

    @GET("users/iostTx")
    fun getBalanceRecord(@Query("page") page: Int,
                         @Query("limit") limit: Int,
                         @Query("drawType") drawType: String): Deferred<ResponseData<BalanceItems>>

    @FormUrlEncoded
    @POST("users/iostTxCursor")
    fun balanceAllRead(@Field("id") id: Int): Deferred<ResponseData<String>>

    @GET("users/drawAbleAmount")
    fun getDrawableAmount(): Deferred<ResponseData<DrawAbleAmount>>

    @GET("androidVersion")
    fun checkVersion(): Deferred<ResponseData<Version?>>

    @FormUrlEncoded
    @POST("users/drawIost")
    fun drawIost(@Field("smsCode") smsCode: String,
                 @Field("transferAddress") transferAddress: String, @Field("f1") f1: String = ""): Deferred<ResponseData<String>>

    @GET("users/inviteHistory")
    fun getReferralData(): Deferred<ResponseData<List<ReferralData>>>

    @GET("users/inviteRewards")
    fun getInviteRewards(): Deferred<ResponseData<ReferralRewardData>>

    @GET("users/socialNames")
    fun getSocialAccount(): Deferred<ResponseData<List<SocialData>>>

    @GET("users/sharePictures")
    fun getSharePictures(): Deferred<ResponseData<ArrayList<PosterData>>>

    @FormUrlEncoded
    @POST("v2/users/login")
    fun login(@Field("username") username: String,
              @Field("password") password: String): Deferred<ResponseData<User>>

    @FormUrlEncoded
    @POST("v2/users/sendSms")
    fun sendSms(@Field("phone") phone: String,
                @Field("areaCode") areaCode: String,
                @Field("type") type: String,
                @Field("googleCaptcha") googleCaptcha: String): Deferred<ResponseData<String>>

    @FormUrlEncoded
    @POST("users/checkCode")
    fun checkCode(@Field("phone") phone: String,
                  @Field("areaCode") areaCode: String,
                  @Field("smsCode") smsCode: String,
                  @Field("inviterCode") inviteCode: String): Deferred<ResponseData<String>>

    @FormUrlEncoded
    @POST("users/register")
    fun register(@Field("phone") phone: String,
                 @Field("areaCode") areaCode: String,
                 @Field("password") password: String,
                 @Field("smsCode") smsCode: String,
                 @Field("inviterCode") inviteCode: String,
                 @Field("email") email: String): Deferred<ResponseData<String>>

    @FormUrlEncoded
    @POST("users/resetPassword")
    fun resetPassword(@Field("phone") phone: String,
                      @Field("areaCode") areaCode: String,
                      @Field("password") password: String,
                      @Field("smsCode") smsCode: String): Deferred<ResponseData<String>>

    @GET("users/info")
    fun getUserInfo(): Deferred<ResponseData<UserInfo>>

    @FormUrlEncoded
    @POST("users/questAnswer")
    fun postQa(@Field("answers") answers: IntArray): Deferred<ResponseData<AnswerData>>

    @FormUrlEncoded
    @POST("v2/users/socialBind")
    fun socialBind(@Field("type") type: String,
                   @Field("id") id: String,
                   @Field("name") name: String,
                   @Field("id_str") idStr: String,
                   @Field("access_token") accessToken: String,
                   @Field("access_token_secret") accessTokenSecret: String,
                   @Field("refresh_token") refreshToken: String): Deferred<ResponseData<String>>

    @FormUrlEncoded
    @POST("users/berminalTask")
    fun berminalCode(@Field("code") code: String): Deferred<ResponseData<String>>

    @GET("v2/config/staticPageConfig")
    fun pageConfig(): Deferred<ResponseData<ConfigPage>>


    @FormUrlEncoded
    @POST
    @Headers("Accept:application/json")
    fun getGithubToken(@Url url: String,
                       @Field("client_id") clientId: String,
                       @Field("client_secret") clientSecret: String,
                       @Field("code") code: String): Deferred<GithubToken>

    @GET
    @Headers("Accept:application/json")
    fun getGithubUser(@Url url: String,
                      @Query("access_token") token: String): Deferred<GithubUser>

    @FormUrlEncoded
    @POST
    fun getRedditToken(@Url url: String,
                       @Header("Authorization") auth: String,
                       @Field("grant_type") grantType: String,
                       @Field("code") code: String,
                       @Field("redirect_uri") redirectUri: String): Deferred<RedditToken>

    @GET
    @Headers("User-Agent:myRedditapp/0.1 by redditusername")
    fun getRedditUser(@Url url: String,
                      @Header("Authorization") auth: String): Deferred<RedditUser>
}
