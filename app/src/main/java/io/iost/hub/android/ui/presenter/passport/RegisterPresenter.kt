package io.iost.hub.android.ui.presenter.passport

import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.passport.RegisterContract
import kotlinx.coroutines.experimental.Job

class RegisterPresenter(override val view: RegisterContract.View, override val job: Job, private val api: HubApiService) :
        RegisterContract.Presenter {

    override fun next(phone: String, areaCode: String, code: String, inviteCode: String) {
        when {
            phone.isEmpty() -> view.toastMsg(R.string.passport_phone_empty)
            code.isEmpty() -> view.toastMsg(R.string.passport_code_empty)
            else -> {
                view.showLoadingDialog()
                callApi(job, api.checkCode(phone, areaCode, code, inviteCode), view) {
                    view.dismissLoading()
                    if (it is DataResult) {
                        view.onNext(phone, code, inviteCode)
                    }
                }
            }
        }
    }
}