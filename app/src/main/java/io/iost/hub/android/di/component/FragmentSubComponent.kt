package io.iost.hub.android.di.component

import android.support.v4.app.Fragment
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.iost.hub.android.di.FragmentScope

@Subcomponent(modules = [AndroidSupportInjectionModule::class])
interface FragmentSubComponent : AndroidInjector<Fragment> {

    @FragmentScope
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<Fragment>()
}