package io.iost.hub.android.ui.feature.account.social

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.model.SocialData
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import io.iost.hub.android.ui.base.adapter.BaseAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick

class SocialAdapter(verifyAction: (platform: String?) -> Unit) : BaseAdapter<SocialData, SocialAdapter.Component>() {

    companion object {
        const val WEIXIN = "Weixin"
        const val WEIBO = "Weibo"
        const val FACEBOOK = "Facebook"
        const val TWITTER = "Twitter"
        const val TELEGRAM = "Telegram"
        const val REDDIT = "Reddit"
        const val GITHUB = "Github"
    }

    private val mSocialArray = hashMapOf(
            Pair(TWITTER, R.drawable.social_twitter),
            Pair(TELEGRAM, R.drawable.social_telegram),
            Pair(REDDIT, R.drawable.social_reddit),
            Pair(GITHUB, R.drawable.social_github),
            Pair(WEIXIN, R.drawable.social_wechat),
            Pair(WEIBO, R.drawable.social_sina),
            Pair(FACEBOOK, R.drawable.social_facebook))


    override val bind: Component.(position: Int) -> Unit = { position ->
        getItem(position)?.let {
            val platform = it.platform
            mSocialArray[it.platform]?.let {
                socialImg.imageResource = it
            }
            nameTxt.text = it.userName
            with(button) {
                isEnabled = it.enable && !it.binded
                textResource = when {
                    !it.enable -> {
                        backgroundResource = R.drawable.bg_gray_button
                        R.string.iost_coming_soon
                    }
                    it.binded -> {
                        backgroundResource = R.drawable.bg_green_shape
                        R.string.iost_verified
                    }
                    else -> {
                        backgroundResource = R.drawable.bg_blue_shape
                        R.string.iost_verify
                    }
                }
                if (isEnabled) {
                    onClick {
                        verifyAction(platform)
                    }
                }
            }
        }
    }

    override fun onCreateComponent(parent: ViewGroup, viewType: Int) = Component(parent)

    class Component(override val view: ViewGroup) : AdapterAnkoComponent {

        lateinit var socialImg: ImageView
        lateinit var nameTxt: TextView
        lateinit var button: Button
        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            cardView {
                radius = dimen(R.dimen.cardview_radius).toFloat()
                cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
                verticalLayout {
                    gravity = Gravity.CENTER_HORIZONTAL
                    socialImg = imageView {
                    }.lparams(dip(80), dip(80)) {
                        topMargin = dimen(R.dimen.dimen_16)
                    }
                    nameTxt = textView {
                        textColorResource = R.color.black_666666
                        textSize = 14f
                    }.lparams {
                        topMargin = dip(-10)
                    }
                    button = button {
                        setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                        textSizeDimen = R.dimen.material_text_button
                        singleLine = true
                        textColorResource = R.color.white
                    }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                        topMargin = dimen(R.dimen.dimen_8)
                        bottomMargin = dip(20)
                    }
                }
                lparams(matchParent, wrapContent) { margin = dimen(R.dimen.dimen_8) }
            }
        }
    }
}