package io.iost.hub.android.ui.feature.daily

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.GridLayout
import android.util.Log
import android.view.Gravity
import android.widget.TextView
import cn.sharesdk.framework.Platform
import cn.sharesdk.framework.PlatformActionListener
import io.iost.hub.android.R
import io.iost.hub.android.extensions.setHtml
import io.iost.hub.android.extensions.shareThird
import io.iost.hub.android.extensions.singleClick
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex
import org.jetbrains.anko.gridlayout.v7.gridLayout
import java.util.HashMap

class ShareDialog(private val isSuccess: Boolean, ctx: Context) : AlertDialog(ctx) {

    private lateinit var contentTxt: TextView
    var code: String = ""

    companion object {
        const val SHARE_URL = "https://hub.iost.io/register?"
    }

    private val shareMap = arrayOf(
            Pair("Twitter", R.drawable.share_twitter),
            Pair("Facebook", R.drawable.share_facebook),
            Pair("Reddit", R.drawable.share_reddit),
            Pair("Telegram", R.drawable.share_telegram),
            Pair("email", R.drawable.share_mail),
            Pair("link", R.drawable.share_link))

    init {
        setView(ctx.UI {
            relativeLayout {
                textView {
                    id = R.id.share_alert_title
                    textSizeDimen = R.dimen.material_text_subtitle
                    textColorResource = R.color.black_333333
                    textResource = if (isSuccess) R.string.complete else R.string.daily_sorry
                }.lparams {
                    centerHorizontally()
                    topMargin = dimen(R.dimen.dimen_8)
                }
                imageView {
                    imageResource = R.drawable.alert_close
                    padding = dimen(R.dimen.dimen_8)
                    singleClick { dismiss() }
                }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                    alignParentRight()
                }
                imageView {
                    id = R.id.share_alert_finish_image
                    imageResource = if (isSuccess) R.drawable.alert_icon_success else R.drawable.alert_icon_error
                }.lparams(dip(60), dip(60)) {
                    topMargin = dimen(R.dimen.dimen_16)
                    centerHorizontally()
                    below(R.id.share_alert_title)
                }
                contentTxt = textView {
                    id = R.id.share_alert_earn_txt
                    padding = dimen(R.dimen.dimen_8)
                    setLineSpacing(0f, 1.2f)
                    gravity = Gravity.CENTER_HORIZONTAL
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.black_333333
                    backgroundResource = R.drawable.selector_bg_grey
                }.lparams(matchParent, wrapContent) {
                    below(R.id.share_alert_finish_image)
                    topMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
                textView(R.string.daily_invite_friends) {
                    id = R.id.share_alert_invite_text
                    textSizeDimen = R.dimen.material_text_body1
                    textColorResource = R.color.black_666666
                }.lparams {
                    below(R.id.share_alert_earn_txt)
                    topMargin = dimen(R.dimen.dimen_10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
                gridLayout {
                    backgroundResource = R.drawable.selector_bg_grey
                    setPadding(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_24), dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_4))
                    shareMap.forEachWithIndex { i, pair ->
                        imageView {
                            imageResource = pair.second
                            tag = pair.first
                            singleClick {
                                val params = "inviteCode=$code"
                                shareThird(ctx.getString(R.string.share_content) + SHARE_URL + params, object : PlatformActionListener {
                                    override fun onComplete(p0: Platform?, p1: Int, p2: HashMap<String, Any>?) {
                                        toast(R.string.share_operation_success)
                                    }

                                    override fun onCancel(p0: Platform?, p1: Int) {
                                        toast(R.string.share_operation_cancel)
                                    }

                                    override fun onError(p0: Platform?, p1: Int, p2: Throwable?) {
                                        toast(R.string.share_operation_failed)
                                        Log.e("share failed", p2?.toString())
                                    }
                                })
                                this@ShareDialog.dismiss()
                            }
                        }.lparams(GridLayout.spec(i / 3, 1f), GridLayout.spec(i % 3, 1f)) {
                            margin = dimen(R.dimen.dimen_4)
                            width = 0
                            height = 0
                        }
                    }
                }.lparams(matchParent, dip(140)) {
                    below(R.id.share_alert_invite_text)
                    topMargin = dimen(R.dimen.dimen_10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_16)
                }
                textView(R.string.daily_share_with) {
                    padding = dimen(R.dimen.dimen_8)
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.black_333333
                }.lparams(matchParent, wrapContent) {
                    below(R.id.share_alert_invite_text)
                    topMargin = dimen(R.dimen.dimen_10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
            }
        }.view)
    }

    fun setTaskTxt(taskName: String, cp: Int, iost: Int) {
        val bonus = (if (cp > 0) "<font color='#FFB100'>$cp</font> CP${if (iost > 0) ", " else ""}" else "") +
                (if (iost > 0) "<font color='#FFB100'>$iost</font> IOST" else "")
        contentTxt.setHtml(context.getString(R.string.daily_task_success, taskName, bonus))
    }

    fun setAnswerTxt(rightNum: Int, size: Int, earn: Int) {
        val earnStr = if (earn > 0) {
            "<font color='#FFB100'>$earn</font>"
        } else {
            earn.toString()
        }
        contentTxt.setHtml(context.getString(R.string.daily_answer_success, "$rightNum/$size", earnStr))
    }
}