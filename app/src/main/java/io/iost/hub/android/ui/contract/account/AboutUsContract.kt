package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.model.ConfigPage
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface AboutUsContract {
    interface View : BaseView {
        fun onPageConfig(data: ConfigPage)
    }

    interface Presenter : BasePresenter<View> {
        fun loadPageConfig()
    }
}