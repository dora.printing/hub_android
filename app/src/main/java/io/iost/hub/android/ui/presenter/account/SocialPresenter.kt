package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.SocialContract
import kotlinx.coroutines.experimental.Job

class SocialPresenter(override val view: SocialContract.View, override val job: Job, private val api: HubApiService) :
        SocialContract.Presenter {

    override fun getSocialAccount() {
        callApi(api.getSocialAccount()) {
            when (it) {
                is DataResult -> {
                    view.showSocialAccount(it.data)
                }
            }
        }
    }
}