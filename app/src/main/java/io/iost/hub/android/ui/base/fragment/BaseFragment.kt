package io.iost.hub.android.ui.base.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.common.UserState
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.widget.LoadStateLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

abstract class BaseFragment<out UI : FragmentAnkoComponent> :
        Fragment(), BaseView {

    abstract val ui: UI

    @Inject
    lateinit var userState: UserState

    private var layout: LoadStateLayout? = null

    private lateinit var mFragmentComponent: FragmentLoadingComponent

    override val viewCtx
        get() = ctx

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mFragmentComponent = FragmentLoadingComponent()
        return mFragmentComponent.createView(AnkoContext.create(ctx, this))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(mFragmentComponent.rootLayout) {
            addView(ui.createView(AnkoContext.create(ctx, this)))
            if (loadData()) {
                showLoadingLayout()
            }
        }
        activityCreated()
    }

    abstract fun activityCreated()

    open fun loadData(): Boolean = false

    override fun toastMsg(resId: Int) {
        toast(resId)
    }

    override fun toastMsg(msg: String) {
        toast(msg)
    }

    override fun showLoadingDialog() {
        (activity as BaseActivity<*>).showLoadingDialog()
    }

    override fun dismissLoading() {
        (activity as BaseActivity<*>).dismissLoading()
    }

    override fun showLoadingLayout() {
        if (layout == null) {
            layout = LoadStateLayout(ctx).apply {
                setRetryListener {
                    layout?.setLoadingState(LoadStateLayout.LOADING)
                }
            }
            mFragmentComponent.rootLayout.addView(layout)
        }
        layout?.setLoadingState(LoadStateLayout.LOADING)
    }

    override fun onLoadError() {
        layout?.setLoadingState(LoadStateLayout.LOAD_ERROR)
    }

    override fun onLoadSuccess() {
        layout?.setLoadingState(LoadStateLayout.LOAD_SUCCESS)
        if (layout != null) layout = null
    }

    override fun onLoginInvalid() {
        layout?.setLoadingState(LoadStateLayout.LOGIN_INVALID)
    }


}