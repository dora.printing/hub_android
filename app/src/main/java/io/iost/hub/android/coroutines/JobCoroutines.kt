@file:Suppress("NOTHING_TO_INLINE")

package io.iost.hub.android.coroutines

import io.iost.hub.android.model.*
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI
import retrofit2.HttpException
import java.io.IOException

/**
 * running as concurrent
 */
inline fun <T> jobAsync(parent: Job? = null, delayTime: Long = 0, noinline job: suspend CoroutineScope.() -> T) = async(CommonPool, parent = parent) {
    delay(delayTime)
    job()
}

inline fun <T> jobOnWorkThread(parent: Job? = null, delayTime: Long = 0, noinline job: suspend CoroutineScope.() -> T) = launch(CommonPool, parent = parent) {
    delay(delayTime)
    job()
}

/**
 * running on Main Thread with UI context
 */
inline fun <T> jobOnUiThread(parent: Job? = null, noinline job: suspend CoroutineScope.() -> T) = launch(UI, parent = parent) {
    job()
}

/**
 * running & block on Main Thread
 */
inline fun jobBlockOnMainThread(delayTime: Long = 0, noinline job: suspend CoroutineScope.() -> Unit) = runBlocking {
    delay(delayTime)
    job()
}

/**
 * running & block on Work Thread
 */
inline fun jobBlockOnWorkThread(delayTime: Long = 0, noinline job: suspend CoroutineScope.() -> Unit) = runBlocking(CommonPool) {
    delay(delayTime)
    job()
}

/**
 * running in coroutines as job list
 */
suspend inline fun <T> jobQueue(delayTime: Long = 0, crossinline job: suspend () -> T) {
    delay(delayTime)
    job()
}

fun <T> callApi(parent: Job?, apiDeferred: Deferred<ResponseData<T>>, view: BaseView? = null, onResult: suspend (Result<T>) -> Unit): Job {
    return jobOnUiThread(parent) {
        val asyncAwait = jobAsync(parent = parent) {
            try {
                val responseData = apiDeferred.await()
                if (responseData.code == 0 && responseData.data != null) {
                    view?.onLoadSuccess()
                    return@jobAsync DataResult(responseData.data)
                } else if (responseData.code == 0 && responseData.message != null) {
                    return@jobAsync DataResult(responseData.message as T)
                } else if (responseData.message != null) {
                    view?.let {
                        if (responseData.code == -1) {
                            it.onLoginInvalid()
                        } else {
                            it.toastUiThread(responseData.message)
                        }
                    }
                    return@jobAsync MessageResult<T>(responseData.message)
                } else {
                    return@jobAsync UnknownResult<T>(Throwable("ServerException:Response is null"))
                }
            } catch (t: Throwable) {
                view?.onLoadError()
                when (t) {
                    is HttpException -> {
                        return@jobAsync HttpResult<T>(t)
                    }
                    is IOException -> {
                        return@jobAsync IOResult<T>(t)
                    }
                    else -> {
                        return@jobAsync UnknownResult<T>(t)
                    }
                }
            }
        }
        onResult(asyncAwait.await())
    }
}

fun <T> BasePresenter<BaseView>.callApi(apiDeferred: Deferred<ResponseData<T>>, onResult: (suspend (Result<T>) -> Unit) = {}) {
    callApi(job, apiDeferred, view, onResult)
}

fun BaseView.toastUiThread(msg: String) {
    jobOnUiThread {
        toastMsg(msg)
    }
}

//inline fun <reified T> Any.asListOfT(): List<T>? {
//    if (this is List<*>) {
//        val list = mutableListOf<T>()
//        for (e in this) {
//            if (e is T) {
//                list.add(e)
//            }
//        }
//        return list
//    }
//    return null
//}
//
//inline fun <reified T> Any.asT(): T? {
//    if (this is T) {
//        return this
//    }
//    return null
//}