package io.iost.hub.android.ui.view.navigation

import android.support.design.internal.NavigationMenuPresenter
import android.support.design.internal.NavigationMenuView
import android.support.design.widget.AppBarLayout
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.support.v4.drawerLayout

class NavigationDrawerComponent : ActivityAnkoComponent<NavigationActivity> {

    override lateinit var toolbar: Toolbar
    lateinit var statusView: View
    lateinit var drawer: DrawerLayout
    lateinit var titleText: TextView
    lateinit var contentTxt: TextView
    lateinit var headView: ImageView
    lateinit var navigationView: NavigationView

    // TODO android.support.v7.widget.ListViewCompat not found
    override fun createView(ui: AnkoContext<NavigationActivity>): View = with(ui) {
        drawer = drawerLayout {
            id = R.id.drawer_layout
            if (isInEditMode) {
                openDrawer(GravityCompat.START)
            }
            verticalLayout {
                statusView = view {
                    backgroundColorResource = R.color.colorPrimary
                }.lparams(matchParent, dip(0))
                coordinatorLayout {
                    appBarLayout {
                        toolbar = hubToolbar {}.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize)) {
                            scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
                        }
                    }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
                    relativeLayout {
                        id = R.id.navigation_fragment
                    }.lparams(width = matchParent, height = matchParent) {
                        behavior = AppBarLayout.ScrollingViewBehavior()
                    }
                }
            }
            navigationView {
                val headerComponent = NavigationDrawerHeaderComponent()
                val headerView = headerComponent.createView(AnkoContext.create(ctx, this))
                getChildAt(0)?.isVerticalScrollBarEnabled = false
                titleText = headerComponent.titleTxt
                contentTxt = headerComponent.contentTxt
                headView = headerComponent.headView
                val listener: ((View?) -> Unit) = {
                    owner.startLoginAct()
                }
                titleText.singleClick(listener)
                contentTxt.singleClick(listener)
                headView.singleClick(listener)
                addHeaderView(headerView)
                inflateMenu(R.menu.menu_navigation_drawer)
                if (!isInEditMode) {
                    setNavigationItemSelectedListener(ui.owner)
                    setCheckedItem(R.id.nav_news)
                }

                val filedByPresenter = this::class.java.getDeclaredField("mPresenter")
                filedByPresenter.isAccessible = true
                val menuPresenter = filedByPresenter.get(this) as NavigationMenuPresenter
                val filedByMenuView = menuPresenter.javaClass.getDeclaredField("mMenuView")
                filedByMenuView.isAccessible = true
                val mMenuView = filedByMenuView.get(menuPresenter) as NavigationMenuView
                mMenuView.addOnChildAttachStateChangeListener(object: RecyclerView.OnChildAttachStateChangeListener {
                    override fun onChildViewDetachedFromWindow(view: View?) {

                    }

                    override fun onChildViewAttachedToWindow(view: View?) {
                        val holder = mMenuView.getChildViewHolder(view)
                        holder?.let {
                            if ("SeparatorViewHolder" == holder.javaClass.simpleName && holder.itemView != null) {
                                if (holder.itemView is FrameLayout) {
                                    val frameLayout = holder.itemView as FrameLayout
                                    val line = frameLayout.getChildAt(0)
                                    if (line.layoutParams is ViewGroup.MarginLayoutParams) {
                                        val p = line.layoutParams as ViewGroup.MarginLayoutParams
                                        p.leftMargin = dip(16)
                                        line.requestLayout()
                                    }
                                }
                            }
                        }
                    }
                })
            }.lparams(height = matchParent) {
                gravity = GravityCompat.START
            }

        }
        drawer
    }
}