package io.iost.hub.android.ui.view.navigation

import android.support.design.widget.NavigationView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import org.jetbrains.anko.*

class NavigationDrawerHeaderComponent : AnkoComponent<NavigationView> {
    lateinit var titleTxt: TextView
    lateinit var contentTxt: TextView
    lateinit var headView: ImageView
    override fun createView(ui: AnkoContext<NavigationView>): View = with(ui) {
        verticalLayout {
            backgroundColor = R.color.grey_f5f5f5
            headView = imageView(R.drawable.user_avatar).lparams {
                topMargin = dip(60)
                leftMargin = dimen(R.dimen.dimen_8)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            titleTxt = textView {
                textSize = 20f
                textColorResource = R.color.white
            }.lparams {
                topMargin = dimen(R.dimen.dimen_8)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            contentTxt = textView {
                textSize = 10f
                textColorResource = R.color.white
            }.lparams {
                topMargin = dimen(R.dimen.dimen_8)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dimen(R.dimen.dimen_16)
            }
            lparams(width = matchParent, height = wrapContent)
        }
    }

}