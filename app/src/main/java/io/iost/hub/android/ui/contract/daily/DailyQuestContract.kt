package io.iost.hub.android.ui.contract.daily

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface DailyQuestContract {
    interface View : BaseView {
        fun refreshStatus(status: Int)
    }

    interface Presenter : BasePresenter<View> {
        fun loadTaskById(id: Int)
    }
}