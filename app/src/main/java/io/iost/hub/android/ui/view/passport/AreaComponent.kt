package io.iost.hub.android.ui.view.passport

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.drawable
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.passport.AreaActivity
import io.iost.hub.android.widget.DividerItemView
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.verticalLayout

class AreaComponent : ActivityAnkoComponent<AreaActivity> {

    override lateinit var toolbar: Toolbar
    lateinit var recyclerView: RecyclerView

    override fun createView(ui: AnkoContext<AreaActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.passport_country_region
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            recyclerView = recyclerView {
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemView(ctx, ctx.drawable(R.color.gray_e5e5e5)))
            }
        }
    }

}