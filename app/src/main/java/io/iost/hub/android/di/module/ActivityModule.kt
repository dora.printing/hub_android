package io.iost.hub.android.di.module

import dagger.Module
import dagger.Provides
import io.iost.hub.android.di.ActivityScope
import kotlinx.coroutines.experimental.Job

@Module
open class ActivityModule {

    @ActivityScope
    @Provides
    fun provideJob(): Job = Job()

}