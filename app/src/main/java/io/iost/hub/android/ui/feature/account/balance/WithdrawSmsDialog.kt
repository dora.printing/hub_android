package io.iost.hub.android.ui.feature.account.balance

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import io.iost.hub.android.R
import io.iost.hub.android.extensions.*
import org.jetbrains.anko.*

class WithdrawSmsDialog(ctx: Context) : Dialog(ctx) {

    lateinit var confirmBtn: Button
    lateinit var codeBtn: Button
    lateinit var codeEdt: EditText

    init {
        setContentView(ctx.UI {
            relativeLayout {
                imageView {
                    imageResource = R.drawable.withdraw_back
                    padding = dip(10)
                    singleClick {
                        codeEdt.hideKeyboard()
                        dismiss()
                    }
                }.lparams(dip(36), dip(36))
                textView(R.string.account_withdraw_code) {
                    id = R.id.withdraw_alert_title
                    textSize = 16f
                    textColorResource = R.color.black_666666
                    gravity = Gravity.CENTER
                }.lparams(matchParent, dip(36))
                view {
                    id = R.id.withdraw_alert_line
                    backgroundColorResource = R.color.gray_d8d8d8
                }.lparams(matchParent, dimen(R.dimen.divider)) {
                    below(R.id.withdraw_alert_title)
                }
                textView(R.string.account_withdraw_send_to) {
                    id = R.id.withdraw_alert_description
                    textSize = 10f
                    textColorResource = R.color.black_666666
                }.lparams {
                    topMargin = dip(10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    below(R.id.withdraw_alert_line)
                }
                codeBtn = button(R.string.iost_get_code) {
                    id = R.id.withdraw_alert_code
                    padding = 0
                    textSize = 12f
                    singleLine = true
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_light_blue_button
                }.lparams(dip(120), dimen(R.dimen.button_height)) {
                    topMargin = dip(10)
                    leftMargin = dimen(R.dimen.dimen_8)
                    rightMargin = dimen(R.dimen.dimen_16)
                    below(R.id.withdraw_alert_description)
                    alignParentRight()
                }
                codeEdt = editText {
                    textSize = 11f
                    setPadding(dip(10), 0, dip(10), 0)
                    setRadiusBackgroundRes(R.color.white, R.dimen.circle_corners, R.color.grey_e8e8e8)
                    singleLine = true
                    hintResource = R.string.account_withdraw_enter_sms
                }.lparams(wrapContent, dimen(R.dimen.button_height)) {
                    topMargin = dip(10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    below(R.id.withdraw_alert_description)
                    leftOf(R.id.withdraw_alert_code)
                    alignParentLeft()
                }
                confirmBtn = button(R.string.account_confirm) {
                    padding = 0
                    singleLine = true
                    textSizeDimen = R.dimen.material_text_button
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_light_blue_button
                }.lparams(matchParent, dimen(R.dimen.button_height)) {
                    below(R.id.withdraw_alert_code)
                    margin = dimen(R.dimen.dimen_16)
                }

            }
        }.view)
        val params = window.attributes
        params.width = ctx.displayWidth
        params.height = wrapContent
        params.gravity = Gravity.BOTTOM
        window.attributes = params
        window.setBackgroundDrawableResource(R.color.grey_f5f5f5)
    }

    override fun show() {
        super.show()
        codeEdt.requestFocus()
        Handler().postDelayed({
            codeEdt.showKeyboard()
        }, 88)
    }

    override fun dismiss() {
        super.dismiss()
        codeEdt.hideKeyboard()
    }
}