package io.iost.hub.android.ui.feature.account

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.extensions.loadUrl
import io.iost.hub.android.model.PosterData
import org.jetbrains.anko.UI
import org.jetbrains.anko.imageView

class PosterAdapter(private val mPosters: ArrayList<PosterData>) : PagerAdapter() {

    init {
        if (mPosters.size > 1) {
            // 添加最后一页到第一页
            mPosters.add(0, mPosters[mPosters.size - 1])
            // 添加第一页(经过上行的添加已经是第二页了)到最后一页
            mPosters.add(mPosters[1])
        }
    }

    override fun isViewFromObject(view: View, obj: Any) = view == obj

    override fun getCount() = mPosters.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val data = mPosters[position]
        return container.context.UI {
            imageView {
                loadUrl(data.image)
            }
        }.view.apply {
            container.addView(this)
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }
}