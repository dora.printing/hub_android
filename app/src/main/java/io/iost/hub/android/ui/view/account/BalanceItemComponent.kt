package io.iost.hub.android.ui.view.account

import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import org.jetbrains.anko.*

class BalanceItemComponent(override val view: ViewGroup) : AdapterAnkoComponent {
    lateinit var title: TextView
    lateinit var time: TextView
    lateinit var withDrawCount: TextView
    lateinit var balanceCount: TextView
    lateinit var state: TextView
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        relativeLayout {
            title = textView {
                id = R.id.balance_item_title
                textSize = 14f
                singleLine = true
                textColorResource = R.color.black_333333
            }.lparams(wrapContent, wrapContent) {
                topMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dip(120)
                alignParentLeft()
            }
            time = textView {
                textSize = 10f
                singleLine = true
                textColorResource = R.color.gray_999999
            }.lparams(wrapContent, wrapContent) {
                below(R.id.balance_item_title)
                alignParentLeft()
                topMargin = dimen(R.dimen.dimen_4)
                bottomMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
            }
            withDrawCount = textView {
                textSize = 14f
                typeface = Typeface.DEFAULT_BOLD
                singleLine = true
                maxWidth = dip(200)
                textColorResource = R.color.red_b81414
            }.lparams(wrapContent, wrapContent) {
                rightMargin = dimen(R.dimen.dimen_16)
                centerVertically()
                alignParentRight()
            }
            balanceCount = textView {
                id = R.id.balance_item_count
                textSize = 14f
                typeface = Typeface.DEFAULT_BOLD
                singleLine = true
                maxWidth = dip(200)
                textColorResource = R.color.green_14b815
            }.lparams(wrapContent, wrapContent) {
                rightMargin = dimen(R.dimen.dimen_16)
                topMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
                alignParentRight()
            }
            state = textView {
                textSize = 10f
                singleLine = true
                textColorResource = R.color.gray_999999
            }.lparams(wrapContent, wrapContent) {
                below(R.id.balance_item_count)
                alignParentRight()
                rightMargin = dimen(R.dimen.dimen_16)
                topMargin = dimen(R.dimen.dimen_4)
                bottomMargin = dimen(R.dimen.dimen_16)
            }
            lparams(matchParent, wrapContent)
        }
    }
}