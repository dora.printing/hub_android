package io.iost.hub.android.ui.base.activity

import android.support.v7.app.AppCompatActivity
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter
import javax.inject.Inject

/**
 * mvp基类
 */
abstract class BasePresenterActivity<P : BasePresenter<BaseView>, out UI : ActivityAnkoComponent<out AppCompatActivity>> :
        BaseActivity<UI>() {

    @Inject
    lateinit var mPresenter: P

    override val isInject: Boolean = true

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }

}
