package io.iost.hub.android.ui.feature.account.referral

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dataFormat
import io.iost.hub.android.model.ReferralData
import io.iost.hub.android.ui.base.adapter.BaseAdapter
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import org.jetbrains.anko.*

class ReferralAdapter :
        BaseAdapter<ReferralData, ReferralAdapter.Component>() {
    override val bind: Component.(position: Int) -> Unit = { position ->
        getItem(position)?.let {
            numberTxt.text = "${it.areaCode} ${it.phone}"
            timeTxt.text = it.createdAt.dataFormat()
            if (it.answersAllRight == 0) {
                stateTxt.textResource = R.string.account_failed
                stateTxt.textColorResource = R.color.red_b81414
            } else {
                stateTxt.textResource = R.string.account_succeed
                stateTxt.textColorResource = R.color.green_14b815
            }
        }
    }

    override fun onCreateComponent(parent: ViewGroup, viewType: Int): Component = Component(parent)

    class Component(override val view: ViewGroup) : AdapterAnkoComponent {

        lateinit var stateTxt: TextView
        lateinit var numberTxt: TextView
        lateinit var timeTxt: TextView

        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            relativeLayout {
                stateTxt = textView {
                    id = R.id.referral_history_state
                    textSize = 14f
                }.lparams {
                    rightMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                    alignParentRight()
                    centerVertically()
                }
                numberTxt = textView {
                    id = R.id.referral_history_number
                    textSize = 14f
                    textColorResource = R.color.black_333333
                }.lparams {
                    alignParentLeft()
                    leftOf(R.id.referral_history_state)
                    rightMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                }
                timeTxt = textView {
                    textSize = 10f
                    textColorResource = R.color.gray_999999
                }.lparams {
                    alignStart(R.id.referral_history_number)
                    alignEnd(R.id.referral_history_number)
                    below(R.id.referral_history_number)
                }
                lparams {
                    topMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_16)
                }
            }
        }
    }
}