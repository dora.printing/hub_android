package io.iost.hub.android.ui.feature.daily

import android.content.Intent
import android.widget.Button
import io.iost.hub.android.R
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.model.DailyData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.daily.DailyQuestContract
import io.iost.hub.android.ui.presenter.daily.DailyQuestPresenter
import io.iost.hub.android.ui.common.DailyTask
import io.iost.hub.android.ui.view.daily.DailyQuestComponent
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textResource
import javax.inject.Inject

class DailyQuestActivity :
        BasePresenterActivity<DailyQuestPresenter, DailyQuestComponent>(), DailyQuestContract.View, DailyTask.TaskCallback {

    companion object {
        const val DAILY_QUEST = "daily_quest"
    }

    override val ui: DailyQuestComponent = DailyQuestComponent()

    @Inject
    lateinit var dailyTask: DailyTask

    private var id: Int? = null

    override fun created() {
        val daily = intent.getParcelableExtra<DailyData>(DAILY_QUEST)
        dailyTask.addTaskCallback(this)
        daily?.let { initData(it) }
    }

    private fun initData(daily: DailyData) {
        id = daily.id
        with(ui) {
            nameTxt.text = daily.detail
            var rewards = getString(R.string.daily_quests_reward)
            if (daily.bonus > 0) {
                rewards += getString(R.string.daily_quests_iost, daily.bonus)
                if (daily.coinBonus > 0) rewards += ", "
            }
            if (daily.coinBonus > 0) {
                rewards += if (daily.jobFrequecy == 1 && daily.bonus > 0) {
                    getString(R.string.daily_quests_cp_per_day, daily.coinBonus)
                } else {
                    getString(R.string.daily_quests_cp, daily.coinBonus)
                }
            }
            rewardsTxt.text = rewards
            val time = if (daily.jobFrequecy == 1) getString(R.string.daily_quests_lifetime) else getString(R.string.daily_quests_daily)
            redemptionTxt.text = getString(R.string.daily_quests_max, time)
            howTodoTxt.text = daily.howTo.replace("<br/> ", "\n")
            rulesTxt.text = daily.rules.replace("<br/>", "\n")
            with(buttonTxt) {
                text = daily.shortCutBtn
                setStatus(daily.status)
                singleClick { dailyTask.doOrClaimTask(this@DailyQuestActivity, daily) }
            }
        }
    }

    private fun Button.setStatus(status: Int) {
        when (status) {
            DailyTask.GO -> {
                //去完成
                isEnabled = true
                backgroundResource = R.drawable.bg_blue_shape
            }
            DailyTask.CLAIM -> {
                //完成 待领取
                textResource = R.string.iost_claim
                isEnabled = true
                backgroundResource = R.drawable.bg_yellow_shape
            }
            DailyTask.CLAIMED -> {
                //完成 已领取
                textResource = R.string.iost_claimed
                isEnabled = false
                backgroundResource = R.drawable.bg_gray_button
            }
            else -> {
                isEnabled = false
                backgroundResource = R.drawable.bg_gray_button
            }
        }
    }

    override fun refreshTaskStatus() {
        id?.let {
            mPresenter.loadTaskById(it)
        }
    }

    override fun refreshStatus(status: Int) {
        ui.buttonTxt.setStatus(status)
    }

    override fun onDestroy() {
        super.onDestroy()
        dailyTask.removeTaskCallback(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            DailyTask.DAILY_REQUEST_CODE -> dailyTask.refreshTaskStatus()
            DailyTask.BERMINAL_REQUEST_CODE -> dailyTask.showBerminalAlert(this)
        }
    }

}