package io.iost.hub.android.model

import retrofit2.HttpException
import java.io.IOException

sealed class Result<T>

data class HttpResult<T>(val httpException: HttpException) : Result<T>()

data class IOResult<T>(val ioException: IOException) : Result<T>()

data class UnknownResult<T>(val throwable: Throwable) : Result<T>()

data class MessageResult<T>(val message: String) : Result<T>()

data class DataResult<T>(val data: T) : Result<T>()

data class ResponseData<out T>(val code: Int, val data: T?, val message: String?)