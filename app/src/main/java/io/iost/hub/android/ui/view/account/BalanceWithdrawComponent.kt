package io.iost.hub.android.ui.view.account

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.drawable
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.balance.BalanceWithdrawActivity
import io.iost.hub.android.widget.DividerItemView
import io.iost.hub.android.widget.hubToolbar
import io.iost.hub.android.widget.smartRefreshLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.recyclerview.v7.recyclerView

class BalanceWithdrawComponent : ActivityAnkoComponent<BalanceWithdrawActivity> {
    override lateinit var toolbar: Toolbar

    lateinit var refreshLayout: SmartRefreshLayout
    lateinit var recyclerView: RecyclerView

    override fun createView(ui: AnkoContext<BalanceWithdrawActivity>) = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_balance_withdraw
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            refreshLayout = smartRefreshLayout {
                backgroundColorResource = R.color.white
                recyclerView = recyclerView {
                    layoutManager = LinearLayoutManager(context)
                    addItemDecoration(DividerItemView(ctx, ctx.drawable(R.color.gray_e5e5e5), dimen(R.dimen.divider)).apply {
                        dividerPadding = dimen(R.dimen.dimen_8)
                        showLastItemDivider = false
                    })
                }
            }.lparams(matchParent, matchParent)
        }
    }

}