package io.iost.hub.android.upgrade

import io.iost.hub.android.model.Version
interface UpgradeCheckInteractor {
     suspend fun check(mapper: (Version?) -> UpgradeInfo?): UpgradeInfo?
}