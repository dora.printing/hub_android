package io.iost.hub.android.ui.common

import android.content.Context
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import io.iost.hub.android.extensions.genericType
import io.iost.hub.android.model.Country
import io.iost.hub.android.model.CountryList
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AreaManager @Inject constructor(private val ctx: Context, private val gson: Gson) {

    private var list: WeakReference<CountryList>? = null

    fun getCountryList(): Deferred<List<Country>?> {
        return async(CommonPool) {
            if (list?.get()?.countries == null) {
                val jsonReader = JsonReader(InputStreamReader(ctx.assets.open("country.json")))
                list = WeakReference(CountryList(gson.fromJson<List<Country>>(jsonReader, genericType<List<Country>>().type)))
                jsonReader.close()
            }
            list?.get()?.countries
        }
    }

    fun getCountry(): Deferred<Country?> {
        return async(CommonPool) {
            var country: Country? = null
            getCountryList().await()?.let {
                run findDefault@{
                    it.forEach continuing@{
                        if ("CN" == it.id) {
                            country = it
                            return@findDefault
                        }
                    }
                }
            }
            country
        }
    }

    fun getCountryCode(): String = if (Locale.getDefault().language.startsWith("zh")) {
        "+86"
    } else {
        "+1"
    }
}