package io.iost.hub.android.extensions

import android.content.*
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Build
import android.text.Html
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.sharesdk.facebook.Facebook
import cn.sharesdk.framework.Platform
import cn.sharesdk.framework.PlatformActionListener
import cn.sharesdk.framework.ShareSDK
import cn.sharesdk.telegram.Telegram
import cn.sharesdk.twitter.Twitter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.google.gson.reflect.TypeToken
import io.iost.hub.android.R
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.*
import java.text.SimpleDateFormat
import java.util.*

fun ImageView.loadUrl(
        url: String,
        round: Boolean = false,
        requestOptions: RequestOptions = RequestOptions()
) {
    requestOptions.centerCrop()
    if (round) {
        requestOptions.transform(CircleCrop()).placeholder(R.drawable.default_avatar)// 正在加载中的图片
                .error(R.drawable.default_avatar)
    } else {
        requestOptions.placeholder(R.drawable.default_picture)// 正在加载中的图片
                .error(R.drawable.default_picture)
    }
    Glide.with(context).load(url).apply(requestOptions).into(this)
}

fun ImageView.shareThird(text: String, listener: PlatformActionListener) {
    val params = Platform.ShareParams()
    params.text = text
    params.title = "IOST"
    params.shareType = Platform.SHARE_TEXT
    when (tag) {
        "Twitter" -> {
            val share = ShareSDK.getPlatform(Twitter.NAME)
            share.platformActionListener = listener
            share.share(params)
        }
        "Facebook" -> {
            val share = ShareSDK.getPlatform(Facebook.NAME)
            share.platformActionListener = listener
            share.share(params)
        }
        "Telegram" -> {
            val share = ShareSDK.getPlatform(Telegram.NAME)
            share.platformActionListener = listener
            share.share(params)
        }

        "link" -> {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("IOST", text)
            clipboard.primaryClip = clipData
            context.toast(R.string.account_poster_copy_success)
        }


        "email" -> {
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.type = "text/html"
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "IOST")
            emailIntent.putExtra(Intent.EXTRA_TEXT, text)
            context.startActivity(Intent.createChooser(emailIntent, "Share by email"))
        }
    }
}

fun ImageView.showInformation(content: String) {
    // 获取当面界面的主布局
    val popView = with(AnkoContext.create(context)) {
        relativeLayout {
            textView(content) {
                textColorResource = R.color.white
                textSize = 13f
                gravity = Gravity.START
                padding = dimen(R.dimen.dimen_32)
            }.lparams {
                centerInParent()
            }
        }
    }
    val pop = PopupWindow(popView, context.displayWidth - 200, wrapContent, false)
    // 需要设置一下此参数，点击外边可消失
    // 如果不设置PopupWindow的背景，有些版本就会出现一个问题：无论是点击外部区域还是Back键都无法dismiss弹框
    pop.setBackgroundDrawable(context.drawable(R.drawable.daily_infomation_bg))
    // 设置此参数获得焦点，否则无法点击
    pop.isFocusable = true
    // 设置点击窗口外边窗口消失
    pop.isOutsideTouchable = true
    pop.showAtLocation(this, Gravity.TOP, 0, context.dip(200))
}

fun TextView.font(font: String) {
    typeface = Typeface.createFromAsset(context.assets, "fonts/$font.ttf")
}

fun TextView.setHtml(content: String) {
    if (content.isNotEmpty()) {
        val html = content.replace("\n", "<br>")
        aboveApi(Build.VERSION_CODES.N, {
            text = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        }) {
            text = Html.fromHtml(html)
        }
    }
}

fun Button.countdown(isFinish: Boolean, time: Int) {
    isEnabled = isFinish
    text = if (isFinish) {
        context.getString(R.string.iost_get_code)
    } else {
        context.getString(R.string.iost_get_code_, time.supplementZero(2))
    }
}

fun Number.supplementZero(d: Int) = String.format("%0${d}d", this)
fun Number.decimal(d: Int) = String.format("%,.${d}f", this.toFloat())

fun String.isEmail(): Boolean {
    val p = "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)\$".toRegex()
    return matches(p)
}

fun String.dataFormat(pattern: String = "yyyy-MM-dd HH:mm"): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    val date = sdf.parse(this)
    val output = SimpleDateFormat(pattern, Locale.ENGLISH)
    return output.format(date)
}

inline fun SharedPreferences.edit(preferApply: Boolean = false, f: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.f()
    if (preferApply) editor.apply() else editor.commit()
}

fun EditText.content() = text.toString().trim()
fun EditText.passwordType() {
    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun Context.isAppInstall(packageName: String): Boolean {
    return try {
        packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}

/**
 * simple extension for async block
 *
 * async(CommonPool, start = CoroutineStart.DEFAULT) {
 *
 * }
 * launch(UI, start = CoroutineStart.DEFAULT) {
 *     val result = async.await()
 * }
 *
 */
fun <T> coroutine(block: suspend CoroutineScope.() -> T, uiBlock: suspend (T) -> Unit): Deferred<T> {
    val deferred = async(CommonPool, CoroutineStart.DEFAULT, null, block)
    launch(UI, CoroutineStart.DEFAULT) {
        uiBlock(deferred.await())
    }
    return deferred
}

/**
 * easy to create TypeToken object
 */
inline fun <reified T> genericType() = object : TypeToken<T>() {}

/**
 * Extension method to check is aboveApi.
 */
inline fun aboveApi(api: Int, block: () -> Unit, elseBlock: () -> Unit = {}) {
    if (Build.VERSION.SDK_INT >= api) {
        block()
    } else {
        elseBlock()
    }
}