package io.iost.hub.android.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import io.iost.hub.android.HubApplication
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: HubApplication): Context = application.applicationContext
}