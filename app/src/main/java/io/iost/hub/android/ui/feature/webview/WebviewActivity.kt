package io.iost.hub.android.ui.feature.webview

import android.webkit.WebChromeClient
import android.webkit.WebView
import com.orhanobut.logger.Logger
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.view.webview.WebviewComponent

class WebviewActivity : BaseActivity<WebviewComponent>() {

    companion object {
        const val WEB_TITLE = "webview_title"
        const val WEB_URL = "webview_url"
    }

    override val ui: WebviewComponent = WebviewComponent()

    override fun created() {
        val url = intent.getStringExtra(WEB_URL)
        val title = intent.getStringExtra(WEB_TITLE)
        Logger.d(title)
        title?.let {
            ui.toolbar.title = it
        }
        with(ui.webview) {
            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)
                    if (newProgress > 50) {
                        onLoadSuccess()
                    }
                }
            }
            url?.let {
                showLoadingLayout()
                loadUrl(it)
            }
        }
    }

}