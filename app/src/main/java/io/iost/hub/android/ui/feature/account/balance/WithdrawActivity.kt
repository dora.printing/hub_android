package io.iost.hub.android.ui.feature.account.balance

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import io.iost.hub.android.extensions.*
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.account.WithdrawContract
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.presenter.account.WithdrawPresenter
import io.iost.hub.android.ui.common.SmsCode
import io.iost.hub.android.ui.view.account.WithdrawComponent
import io.iost.hub.android.widget.withdrawSmsAlert
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class WithdrawActivity : BasePresenterActivity<WithdrawPresenter, WithdrawComponent>(),
        WithdrawContract.View, SmsCode.SmsCodeCallback {

    override val ui: WithdrawComponent = WithdrawComponent()

    @Inject
    lateinit var smsCode: SmsCode

    private lateinit var phone: String
    private var mAmount: Float = 0f

    private val alert: WithdrawSmsDialog  by lazy {
        withdrawSmsAlert {
            confirmBtn.singleClick {
                mPresenter.drawIost(codeEdt.content(), ui.addressEdit.content())
            }
            codeBtn.singleClick {
                smsCode.getVerifyCode(phone, userState.getAreaCode(), SmsCode.WITHDRAW_TYPE)
            }
        }
    }

    override fun created() {
        phone = userState.getPhone()
        if (phone.isEmpty()) {
            startActivity<LoginActivity>()
            finish()
        } else {
            smsCode.registerCallback(this)
            mPresenter.getDrawAbleAmount()
            with(ui) {
                addressEdit.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        s?.let {
                            withdrawBtn.isEnabled = it.length > 20 && mAmount >= 1000
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    }

                })
                withdrawBtn.singleClick {
                    alert.show()
                }
            }
        }
    }

    override fun showDrawAbleAmount(amount: Float) {
        mAmount = amount
        ui.withdrawBtn.isEnabled = amount >= 1000 && ui.addressEdit.text.length > 20
        ui.availableWithDraw.text = amount.decimal(2)
    }

    override fun onWithdrawSuccess() {
        alert.dismiss()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onCountDown(isFinish: Boolean, time: Int) {
        alert.codeBtn.countdown(isFinish, time)
    }

    override fun onPause() {
        super.onPause()
        ui.addressEdit.hideKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
        smsCode.unRegisterCallback()
    }

}