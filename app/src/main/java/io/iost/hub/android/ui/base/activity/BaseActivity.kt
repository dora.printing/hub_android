package io.iost.hub.android.ui.base.activity

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.ViewGroup
import com.gyf.barlibrary.ImmersionBar
import dagger.android.AndroidInjection
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.ui.common.UserState
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.widget.LoadStateLayout
import org.jetbrains.anko.*
import javax.inject.Inject

abstract class BaseActivity<out UI : ActivityAnkoComponent<out AppCompatActivity>> :
        AppCompatActivity(), BaseView {

    abstract val ui: UI

    open val isInject: Boolean = false

    private lateinit var mImmersionBar: ImmersionBar

    @Inject
    lateinit var userState: UserState

    private var layout: LoadStateLayout? = null

    override val viewCtx by lazy {
        ctx
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isInject) AndroidInjection.inject(this)
        (ui as ActivityAnkoComponent<AppCompatActivity>).setContentView(this)
        initImmersionBar()
        created()
        if (loadData()) {
            showLoadingLayout()
        }
    }

    abstract fun created()

    open fun loadData(): Boolean = false

    override fun onDestroy() {
        super.onDestroy()
        mImmersionBar.destroy()
        loadingDialog?.dismiss()
    }

    private fun initImmersionBar() {
        ImmersionBar.with(this).apply {
            mImmersionBar = this
            setupImmersionBar(this)
            init()
        }
    }

    open fun setupImmersionBar(immersionBar: ImmersionBar) {
        ui.toolbar?.let {
            setSupportActionBar(it)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            immersionBar.titleBar(it)
            it.elevation = 8f
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun toastMsg(resId: Int) {
        toast(resId)
    }

    override fun toastMsg(msg: String) {
        toast(msg)
    }

    private var loadingDialog: Dialog? = null

    override fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = indeterminateProgressDialog("Loading…")
        } else {
            loadingDialog?.show()
        }
    }

    override fun dismissLoading() {
        loadingDialog?.dismiss()
    }

    override fun showLoadingLayout() {
        if (layout == null) {
            layout = LoadStateLayout(ctx).apply {
                setRetryListener {
                    layout?.setLoadingState(LoadStateLayout.LOADING)
                }
            }
            addContentView(layout, ViewGroup.MarginLayoutParams(matchParent, matchParent).apply {
                topMargin = (dimenAttr(R.attr.actionBarSize) + ImmersionBar.getStatusBarHeight(act))
            })
        }
        layout?.setLoadingState(LoadStateLayout.LOADING)
    }

    override fun onLoadError() {
        layout?.setLoadingState(LoadStateLayout.LOAD_ERROR)
    }

    override fun onLoadSuccess() {
        layout?.setLoadingState(LoadStateLayout.LOAD_SUCCESS)
        if (layout != null) layout = null
    }

    override fun onLoginInvalid() {
        layout?.setLoadingState(LoadStateLayout.LOGIN_INVALID)
    }

}