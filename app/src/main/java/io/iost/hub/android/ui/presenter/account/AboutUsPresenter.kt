package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.AboutUsContract
import kotlinx.coroutines.experimental.Job

class AboutUsPresenter(override val view: AboutUsContract.View, override val job: Job, private val api: HubApiService) :
        AboutUsContract.Presenter {
    override fun loadPageConfig() {
        callApi(api.pageConfig()) {
            if (it is DataResult) {
                view.onPageConfig(it.data)
            }
        }
    }
}