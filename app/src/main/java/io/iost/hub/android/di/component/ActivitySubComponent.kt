package io.iost.hub.android.di.component

import android.support.v7.app.AppCompatActivity
import dagger.Subcomponent
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import io.iost.hub.android.di.ActivityScope

@Subcomponent(modules = [AndroidInjectionModule::class])
interface ActivitySubComponent : AndroidInjector<AppCompatActivity> {

    @ActivityScope
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<AppCompatActivity>()
}