package io.iost.hub.android.push

enum class PushTarget {

    FCM {
        override fun type() = FCM
    },

    JPUSH {
        override fun type() = JPUSH
    };

    abstract fun type(): PushTarget
}