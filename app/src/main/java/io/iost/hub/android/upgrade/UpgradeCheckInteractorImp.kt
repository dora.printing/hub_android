package io.iost.hub.android.upgrade

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.model.Version

class UpgradeCheckInteractorImp(private val hubApiService: HubApiService) : UpgradeCheckInteractor {
    override suspend fun check(mapper: (Version?) -> UpgradeInfo?): UpgradeInfo? {
        val version = try {
            val remoteRes = hubApiService.checkVersion().await()
            if (remoteRes.code == 0) remoteRes.data else null
        } catch (e: Exception) {
            null
        }
        return mapper(version)
    }
}