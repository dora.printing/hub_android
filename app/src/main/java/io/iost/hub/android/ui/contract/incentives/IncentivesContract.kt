package io.iost.hub.android.ui.contract.incentives

import io.iost.hub.android.model.IncentivesData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface IncentivesContract {
    interface View : BaseView {
        fun showData(list: List<IncentivesData>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadData()
    }
}