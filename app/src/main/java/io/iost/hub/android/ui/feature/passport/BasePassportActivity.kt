package io.iost.hub.android.ui.feature.passport

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter
import io.iost.hub.android.ui.common.AreaManager
import org.jetbrains.anko.startActivityForResult
import javax.inject.Inject

abstract class BasePassportActivity<P : BasePresenter<BaseView>, out UI : ActivityAnkoComponent<out AppCompatActivity>> :
        BasePresenterActivity<P, UI>() {

    companion object {
        private const val AREA_REQUEST_CODE = 1001
    }

    @Inject
    lateinit var areaManager: AreaManager

    protected var mAreaCode = ""

    override fun created() {
        mAreaCode = areaManager.getCountryCode()
        setCountryCode(if (mAreaCode == "+86") "CN +86" else "US +1")
    }

    fun onCountryClick() {
        startActivityForResult<AreaActivity>(AREA_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AREA_REQUEST_CODE -> {
                    data?.let {
                        val id = it.getStringExtra(AreaActivity.COUNTRY_ID)
                        val code = it.getStringExtra(AreaActivity.COUNTRY_CODE)
                        mAreaCode = "+$code"
                        setCountryCode("$id +$code")
                    }
                }
            }
        }
    }

    abstract fun setCountryCode(countryArea: String)

}