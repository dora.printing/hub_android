package io.iost.hub.android.ui.view.account

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.social.SocialActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.recyclerview.v7.recyclerView

class SocialComponent : ActivityAnkoComponent<SocialActivity> {

    override lateinit var toolbar: Toolbar

    lateinit var recyclerView: RecyclerView

    override fun createView(ui: AnkoContext<SocialActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_social_accounts
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            recyclerView = recyclerView {
                layoutManager = GridLayoutManager(ctx, 2)
            }.lparams(matchParent, matchParent) {
                margin = dimen(R.dimen.dimen_8)
            }
            lparams(matchParent, matchParent)
            clipChildren = false
        }
    }
}