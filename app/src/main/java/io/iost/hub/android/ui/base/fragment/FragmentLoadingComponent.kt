package io.iost.hub.android.ui.base.fragment

import android.support.v4.app.Fragment
import android.view.View
import android.widget.FrameLayout
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent

class FragmentLoadingComponent : AnkoComponent<Fragment> {

    lateinit var rootLayout: FrameLayout

    override fun createView(ui: AnkoContext<Fragment>): View = with(ui) {
        rootLayout = frameLayout {
            lparams(matchParent, matchParent)
        }
        rootLayout
    }

}