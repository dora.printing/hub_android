package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.model.SocialData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface SocialContract {
    interface View : BaseView {
        fun showSocialAccount(data: List<SocialData>)
    }

    interface Presenter : BasePresenter<View> {
        fun getSocialAccount()
    }
}