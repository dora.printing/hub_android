package io.iost.hub.android.ui.view.incentives

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import io.iost.hub.android.ui.base.fragment.FragmentAnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView

class IncentivesComponent : FragmentAnkoComponent {

    lateinit var recyclerView: RecyclerView

    override fun createView(ui: AnkoContext<FrameLayout>): View = with(ui) {
        recyclerView = recyclerView {
            lparams(matchParent, matchParent)
            layoutManager = LinearLayoutManager(context)
        }
        return recyclerView
    }

}