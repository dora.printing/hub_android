package io.iost.hub.android.ui.base.adapter

import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext

interface AdapterAnkoComponent : AnkoComponent<ViewGroup> {

    val view: ViewGroup

    fun inflate(): View {
        return createView(AnkoContext.create(view.context, view))
    }
}
