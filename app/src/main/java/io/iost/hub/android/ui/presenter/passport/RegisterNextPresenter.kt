package io.iost.hub.android.ui.presenter.passport

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.passport.RegisterNextContract
import kotlinx.coroutines.experimental.Job

class RegisterNextPresenter(override val view: RegisterNextContract.View, override val job: Job, private val api: HubApiService) :
        RegisterNextContract.Presenter {

    override fun register(phone: String, areaCode: String, password: String, smsCode: String, inviteCode: String, email: String) {
        view.showLoadingDialog()
        callApi(job, api.register(phone, areaCode, password, smsCode, inviteCode, email), view) {
            if (it is DataResult) {
                view.onRegisterSuccess(phone, password)
            }
            view.dismissLoading()
        }
    }

}