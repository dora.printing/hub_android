package io.iost.hub.android.upgrade

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import io.iost.hub.android.BuildConfig
import io.iost.hub.android.R
import io.iost.hub.android.extensions.edit
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.alert
import org.jetbrains.anko.progressDialog
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class UpgradeChecker constructor(private val upgradeCheckInteractor: UpgradeCheckInteractor) {

    companion object {
        const val VERSION_CHECK_SPF = "version_check_spf"
        const val IGNORE_VERSION_KEY = "ignore_version"
        const val VERSION_UPGRADE_FILE_DIR = "hub_iost"
        const val PACKAGES = "packages"
        const val VERSION_UPGRADE_PACKAGE = "upgrade_tmp.apk"
        val TAG = UpgradeChecker::class.simpleName
    }

    suspend fun check(activity: Activity, isManual: Boolean) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            activity.alert(R.string.upgrade_version_write_permission_content, R.string.upgrade_version_write_permission_title) {
                positiveButton(R.string.confirm) { dialog ->
                    val intent = Intent()
                    intent.action = (Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", activity.packageName, null)
                    intent.data = uri
                    activity.startActivity(intent)
                    dialog.dismiss()
                }
                negativeButton(R.string.cancel) { dialog ->
                    dialog.dismiss()
                }
            }.show()
            return
        }
        executeCheck(activity, isManual)
    }

    private suspend fun executeCheck(activity: Activity, isManual: Boolean) {
        val upgradeInfo = upgradeCheckInteractor.check {
            if (null == it) {
                return@check null
            }
            val isUpdate = it.versionCode > BuildConfig.VERSION_CODE
            return@check UpgradeInfo(
                    isUpdate,
                    isManual,
                    it.version,
                    (it.size * 1024 * 1024).toInt(),
                    activity.getString(R.string.upgrade_version_new_title),
                    it.updateContent, it.url, it.forceUpgrade)

        }

        if (null == upgradeInfo) {
            if (isManual) {
                activity.toast(R.string.upgrade_version_check_failed)
            }
            return
        }

        if (!upgradeInfo.isUpdate) {
            if (isManual) {
                activity.toast(R.string.upgrade_version_check_newest)
            }
            return
        }

        val spf = activity.getSharedPreferences(VERSION_CHECK_SPF, Context.MODE_PRIVATE)

        if (spf.getString(IGNORE_VERSION_KEY, "") == upgradeInfo.remoteVersion) {
            if (isManual) {
                activity.toast(R.string.upgrade_ignored_version)
            }
            return
        }

        if (upgradeInfo.isForceUpdate) {
            forceUpdateAlert(activity, upgradeInfo)
        } else {
            updateAlert(activity, upgradeInfo)
        }
    }

    private suspend fun downloadAndInstall(activity: Activity, upgradeInfo: UpgradeInfo, progressDialog: ProgressDialog) {
        val request = Request.Builder().url(upgradeInfo.url).build()

        val root = File(Environment.getExternalStorageDirectory(), VERSION_UPGRADE_FILE_DIR)
        val dir = File(root, PACKAGES)
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                activity.toast(R.string.permission_storage_denied_message)
                progressDialog.dismiss()
                return
            }
        }
        val file = File(dir, VERSION_UPGRADE_PACKAGE)
        if (file.exists()) {
            file.delete()
        }
        file.createNewFile()
        val call = OkHttpClient().newCall(request)
        val downTask = async {
            download(call, file, progressDialog)
        }
        val success = downTask.await()
        progressDialog.dismiss()
        if (success) {
            try {
                val updateIntent = Intent(Intent.ACTION_INSTALL_PACKAGE)
                val data = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file)
                updateIntent.setDataAndType(data, "application/vnd.android.package-archive")
                updateIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                updateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                activity.startActivity(updateIntent)
                if (upgradeInfo.isForceUpdate) {
                    activity.finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                activity.toast("unknown err")
            }
        } else {
            if (upgradeInfo.isManual) {
                activity.toast(R.string.upgrade_download_failed)
            }
        }
    }

    private fun forceUpdateAlert(activity: Activity, upgradeInfo: UpgradeInfo) {
        val alertDialog: Dialog = activity.alert(title = activity.getString(R.string.upgrade_force_title), message = upgradeInfo.updateContent) {
            positiveButton(R.string.update) { dialog ->
                dialog.dismiss()
                activity.progressDialog(R.string.upgrade_fetch_data, R.string.downloading) {
                    launch(UI) {
                        max = upgradeInfo.size
                        setCancelable(false)
                        setCanceledOnTouchOutside(false)
                        downloadAndInstall(activity, upgradeInfo, this@progressDialog)
                    }
                }
            }
        }.build()
        alertDialog.setCancelable(false)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun updateAlert(activity: Activity, updateInfo: UpgradeInfo) {
        val alertDialog = activity.alert(title = updateInfo.updateTitle, message = updateInfo.updateContent) {
            positiveButton(R.string.update) { dialog ->
                dialog.dismiss()
                activity.progressDialog(R.string.upgrade_fetch_data, R.string.downloading) {
                    launch(UI) {
                        max = updateInfo.size
                        setCancelable(false)
                        setCanceledOnTouchOutside(false)
                        downloadAndInstall(activity, updateInfo, this@progressDialog)
                    }
                }
            }
            negativeButton(R.string.latter) { dialog ->
                dialog.dismiss()
            }

            neutralPressed(R.string.ignore) { dialog ->
                dialog.dismiss()
                activity.getSharedPreferences(VERSION_CHECK_SPF, Context.MODE_PRIVATE).edit {
                    putString(IGNORE_VERSION_KEY, updateInfo.remoteVersion)
                }
            }
        }.build()
        alertDialog.setCancelable(false)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun download(call: Call, file: File, progressDialog: ProgressDialog): Boolean {
        try {
            val response = call.execute()
            if (null == response || !response.isSuccessful) {
                return false
            }
            val buf = ByteArray(2048)
            val inStream = response.body()?.byteStream()
            val outStream = FileOutputStream(file)
            var current = 0
            try {
                inStream?.let { stream ->
                    var len = stream.read(buf)
                    while (len != -1) {
                        current += len
                        progressDialog.progress = current
                        outStream.write(buf, 0, len)
                        len = stream.read(buf)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return false
            } finally {
                outStream.flush()
                inStream?.close()
                outStream.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        return true
    }
}