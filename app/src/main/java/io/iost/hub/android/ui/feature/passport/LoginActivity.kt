package io.iost.hub.android.ui.feature.passport

import android.content.Intent
import com.gyf.barlibrary.ImmersionBar
import io.iost.hub.android.R
import io.iost.hub.android.extensions.hideKeyboard
import io.iost.hub.android.ui.contract.passport.LoginContract
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import io.iost.hub.android.ui.presenter.passport.LoginPresenter
import io.iost.hub.android.ui.view.passport.LoginComponent
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : BasePassportActivity<LoginPresenter, LoginComponent>(), LoginContract.View {

    companion object {
        const val PHONE_NUMBER = "phone_number"
        const val PHONE_PASSWORD = "phone_password"
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            val phoneNum = it.getStringExtra(PHONE_NUMBER)
            val password = it.getStringExtra(PHONE_PASSWORD)
            if (phoneNum == null) return
            ui.phoneNumEdt.setText(phoneNum)
            ui.passwordEdt.requestFocus()
            if (password != null) {
                ui.passwordEdt.setText(password)
                login(phoneNum, password)
            }
        }
    }

    override val ui: LoginComponent = LoginComponent()

    fun login(phone: String, password: String) {
        mPresenter.login(phone, password)
    }

    override fun setupImmersionBar(immersionBar: ImmersionBar) {
        immersionBar.statusBarView(ui.statusView)
        immersionBar.statusBarDarkFont(true, 0.2f)
    }

    override fun onLoginSuccess() {
        ui.passwordEdt.hideKeyboard()
        startActivity<NavigationActivity>()
    }

    override fun onLoginFailure(errorMessage: String) {
        toast(errorMessage)
    }

    override fun setCountryCode(countryArea: String) {
        ui.countryTxt.text = countryArea
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom)
    }

}