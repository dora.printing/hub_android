package io.iost.hub.android.ui.view.account

import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.balance.WithdrawActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class WithdrawComponent : ActivityAnkoComponent<WithdrawActivity> {

    override lateinit var toolbar: Toolbar

    lateinit var availableWithDraw: TextView
    lateinit var withdrawBtn: Button
    lateinit var addressEdit: EditText

    override fun createView(ui: AnkoContext<WithdrawActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_withdraw
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            addressEdit = editText {
                textSize = 12f
                hintResource = R.string.account_wallet_address
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                backgroundResource = R.drawable.bg_edittext
            }.lparams(matchParent, dimen(R.dimen.dimen_32)) {
                topMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            textView(R.string.account_double_check_address) {
                textSize = 10f
                textColorResource = R.color.red_b22121
            }.lparams {
                topMargin = dimen(R.dimen.dimen_8)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            availableWithDraw = textView {
                gravity = Gravity.CENTER_VERTICAL
                textSize = 12f
                hintResource = R.string.account_available_withdraw
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                backgroundResource = R.drawable.bg_edittext
            }.lparams(matchParent, dimen(R.dimen.dimen_32)) {
                topMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            textView(R.string.account_withdraw_minimum) {
                textSize = 10f
                textColorResource = R.color.red_b22121
            }.lparams {
                topMargin = dimen(R.dimen.dimen_8)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            withdrawBtn = button(R.string.account_withdraw) {
                isEnabled = false
                padding = dimen(R.dimen.dimen_4)
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                topMargin = dimen(R.dimen.dimen_16)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
        }
    }

}