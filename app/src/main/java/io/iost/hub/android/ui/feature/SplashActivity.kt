package io.iost.hub.android.ui.feature

import android.app.Activity
import android.os.Bundle
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import org.jetbrains.anko.startActivity

class SplashActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity<NavigationActivity>()
        finish()
    }

}
