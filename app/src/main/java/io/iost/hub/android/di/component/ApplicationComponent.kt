package io.iost.hub.android.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import io.iost.hub.android.HubApplication
import io.iost.hub.android.di.module.*
import javax.inject.Singleton

@Singleton
@Component(modules = [
    // base module
    ApplicationModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    AllActivityModule::class,
    AllFragmentModule::class,
    // libs module
    DataModule::class,
    UpgradeModule::class
])
interface ApplicationComponent {

    fun inject(hubApplication: HubApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(hubApplication: HubApplication): Builder

        fun build(): ApplicationComponent
    }
}