package io.iost.hub.android.widget

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.showInformation
import io.iost.hub.android.extensions.singleClick
import org.jetbrains.anko.*


class DailyCardView constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        CardView(context, attrs, defStyleAttr) {

    init {
        initLayout()
    }

    lateinit var titleTxt: TextView
    lateinit var contentTxt: TextView
    private lateinit var infoIcon: ImageView

    private fun initLayout() = AnkoContext.createDelegate(this).apply {
        radius = dimen(R.dimen.cardview_radius).toFloat()
        cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
        relativeLayout {
            titleTxt = textView {
                textSize = 13f
                textColorResource = R.color.black_666666
            }.lparams {
                margin = dimen(R.dimen.dimen_8)
            }
            contentTxt = textView {
                textSize = 24f
                textColorResource = R.color.black_333333
                gravity = Gravity.CENTER
                typeface = Typeface.DEFAULT_BOLD
                setLineSpacing(10f, 1f)
            }.lparams {
                centerInParent()
            }
            infoIcon = imageView {
                imageResource = R.drawable.daily_tips
                padding = dimen(R.dimen.dimen_8)
            }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                alignParentRight()
                alignParentBottom()
            }
        }
    }

    fun infoClick(content: String) {
        with(infoIcon) {
            singleClick { showInformation(content) }
        }
    }

}