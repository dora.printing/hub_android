package io.iost.hub.android.ui.feature.account.balance

import android.app.Activity
import android.os.CountDownTimer
import io.iost.hub.android.BuildConfig
import io.iost.hub.android.GlobalConfig
import io.iost.hub.android.R
import io.iost.hub.android.extensions.supplementZero
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.view.account.WithdrawRulesComponent
import org.jetbrains.anko.textResource
import java.text.SimpleDateFormat
import java.util.*

class WithdrawRulesActivity : BaseActivity<WithdrawRulesComponent>() {

    companion object {
        private val COUNTDOWN_TIME = if (BuildConfig.DEBUG) 3_100L else 30_100L
    }

    override val ui: WithdrawRulesComponent = WithdrawRulesComponent()

    private var mTimer: CountDownTimer? = null

    override fun created() {
        if (mTimer == null) {
            mTimer = object : CountDownTimer(COUNTDOWN_TIME, GlobalConfig.COUNT_SECOND) {
                override fun onFinish() {
                    mTimer = null
                    ui.readBtn.isEnabled = true
                    ui.readBtn.textResource = R.string.account_have_read
                }

                override fun onTick(millisUntilFinished: Long) {
                    val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
                    dateFormat.timeZone = TimeZone.getTimeZone("GMT")
                    ui.readBtn.text = getString(R.string.account_have_read_, (millisUntilFinished / 1000).supplementZero(2))
                }
            }
            mTimer?.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mTimer?.cancel()
        mTimer = null
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom)
    }

    fun startWithdrawAct() {
        setResult(Activity.RESULT_OK)
        finish()
    }

}