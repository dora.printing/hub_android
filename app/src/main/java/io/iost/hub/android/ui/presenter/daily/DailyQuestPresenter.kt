package io.iost.hub.android.ui.presenter.daily

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.daily.DailyQuestContract
import kotlinx.coroutines.experimental.Job

class DailyQuestPresenter(override val view: DailyQuestContract.View, override val job: Job, private val api: HubApiService) :
        DailyQuestContract.Presenter {
    override fun loadTaskById(id: Int) {
        view.showLoadingDialog()
        callApi(api.getTaskById(id)) {
            view.dismissLoading()
            if (it is DataResult) {
                view.refreshStatus(it.data.status)
            }
        }
    }

}