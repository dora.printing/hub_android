package io.iost.hub.android.ui.feature.news

import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import io.iost.hub.android.model.Banner
import io.iost.hub.android.model.NewsData
import io.iost.hub.android.ui.base.fragment.BasePresenterFragment
import io.iost.hub.android.ui.contract.news.NewsContract
import io.iost.hub.android.ui.feature.webview.WebviewActivity
import io.iost.hub.android.ui.presenter.news.NewsPresenter
import io.iost.hub.android.ui.view.news.NewsComponent
import org.jetbrains.anko.support.v4.startActivity

class NewsFragment : BasePresenterFragment<NewsPresenter, NewsComponent>(), NewsContract.View {

    override val ui: NewsComponent = NewsComponent()

    private val mAdapter = NewsAdapter {
        startActivity<WebviewActivity>(WebviewActivity.WEB_URL to it.link, WebviewActivity.WEB_TITLE to it.title)
    }

    override fun activityCreated() {
        with(ui) {
            refreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
                override fun onLoadMore(refreshLayout: RefreshLayout) {
                    mPresenter.loadMoreNews()
                }

                override fun onRefresh(refreshLayout: RefreshLayout) {
                    mPresenter.getNewsData()
                }
            })
            recyclerView.adapter = mAdapter
        }
    }

    override fun loadData(): Boolean {
        mPresenter.getBanner()
        mPresenter.getNewsData()
        return true
    }

    override fun showBanner(banners: List<Banner>) {
        mAdapter.mBanner = banners
        mAdapter.notifyItemChanged(0)
    }

    override fun setNewsList(newsList: List<NewsData>) {
        mAdapter.items = newsList
        ui.refreshLayout.finishRefresh()
    }

    override fun loadMoreNews(newsList: List<NewsData>) {
        mAdapter.loadMore(newsList)
        ui.refreshLayout.finishLoadMore()
    }

}