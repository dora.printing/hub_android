package io.iost.hub.android.ui.presenter.navigation

import com.orhanobut.logger.Logger
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.navigation.NavigationContract
import kotlinx.coroutines.experimental.Job


class NavigationPresenter(override val view: NavigationContract.View, override val job: Job, private val api: HubApiService) :
        NavigationContract.Presenter {

    override fun showQa() {
        callApi(job, api.getUserInfo()) {
            if (it is DataResult) {
                if (!it.data.questAnswers) {
                    view.onShowQa()
                }
            }
        }
    }

    override fun postQa(answers: IntArray) {
        view.showLoadingDialog()
        callApi(api.postQa(answers)) {
            view.dismissLoading()
            Logger.d(it)
            if (it is DataResult) {
                view.onAnswerQuestion(it.data)
            }
        }
    }
}
