package io.iost.hub.android.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class ActivityScope

@Scope
@kotlin.annotation.Retention
annotation class FragmentScope