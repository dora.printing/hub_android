package io.iost.hub.android.ui.feature.account.aboutus

import io.iost.hub.android.R
import io.iost.hub.android.model.ConfigPage
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.account.AboutUsContract
import io.iost.hub.android.ui.feature.webview.WebviewActivity
import io.iost.hub.android.ui.presenter.account.AboutUsPresenter
import io.iost.hub.android.ui.view.account.AboutUsComponent
import io.iost.hub.android.upgrade.UpgradeChecker
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.startActivity
import zendesk.support.request.RequestActivity
import javax.inject.Inject

class AboutUsActivity : BasePresenterActivity<AboutUsPresenter, AboutUsComponent>(), AboutUsContract.View {

    override val ui: AboutUsComponent = AboutUsComponent()

    @Inject
    lateinit var upgradeChecker: UpgradeChecker

    private var mData: ConfigPage? = null

    override fun created() {
    }

    override fun loadData(): Boolean {
        mPresenter.loadPageConfig()
        return true
    }

    override fun onPageConfig(data: ConfigPage) {
        this.mData = data
    }

    fun contactUs() {
        RequestActivity.builder().show(this)
    }

    fun policy() {
        mData?.let {
            startActivity<WebviewActivity>(WebviewActivity.WEB_URL to it.policy,
                    WebviewActivity.WEB_TITLE to getString(R.string.about_us_privacy_policy))
        }
    }

    fun terms() {
        mData?.let {
            startActivity<WebviewActivity>(WebviewActivity.WEB_URL to it.terms,
                    WebviewActivity.WEB_TITLE to getString(R.string.about_us_terms_conditions))
        }
    }

    fun updateCheck() {
        launch(UI, CoroutineStart.DEFAULT, mPresenter.job) {
            upgradeChecker.check(this@AboutUsActivity, true)
        }
    }
}