package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.model.BalanceItemData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface BalanceContract {
    interface View : BaseView {
        fun showBalance(balance: Float)
        fun setBalanceList(list: List<BalanceItemData>)
        fun loadMore(list: List<BalanceItemData>)
        fun isWithdrawn(isWithdrawn: Boolean)
    }

    interface Presenter : BasePresenter<View> {
        fun getBalanceData()
        fun getBalanceRecord()
        fun loadMoreRecord()
        fun loadIsWithdrawn()
    }
}