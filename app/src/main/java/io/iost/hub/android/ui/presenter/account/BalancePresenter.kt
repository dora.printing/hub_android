package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.BalanceContract
import kotlinx.coroutines.experimental.Job

class BalancePresenter(override val view: BalanceContract.View, override val job: Job, private val api: HubApiService) :
        BalanceContract.Presenter {

    private var mPage = 1
    private val mLimit = 20

    override fun getBalanceData() {
        callApi(api.getBalance()) {
            when (it) {
                is DataResult -> {
                    view.showBalance(it.data.balance)
                }
            }
        }
    }

    override fun getBalanceRecord() {
        mPage = 1
        loadBalanceRecord()
    }

    override fun loadMoreRecord() {
        loadBalanceRecord()
    }

    private fun loadBalanceRecord() {
        callApi(api.getBalanceRecord(mPage, mLimit, "undrawed")) {
            when (it) {
                is DataResult -> {
                    val items = it.data.items
                    if (mPage == 1) {
                        view.setBalanceList(it.data.items)
                        if (it.data.items.isNotEmpty())
                            callApi(job, api.balanceAllRead(items[items.size - 1].id)) { }
                    } else {
                        view.loadMore(it.data.items)
                    }
                    if (it.data.items.isNotEmpty()) mPage++
                }
            }
        }
    }

    override fun loadIsWithdrawn() {
        callApi(api.getBalanceRecord(1, 1, "drawed")) {
            when (it) {
                is DataResult -> {
                    val items = it.data.items
                    view.isWithdrawn(items.isNotEmpty())
                }
            }
        }
    }

}