package io.iost.hub.android.ui.contract.navigation

import io.iost.hub.android.model.AnswerData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface NavigationContract {

    interface View : BaseView {
        fun onShowQa()
        fun onAnswerQuestion(data: AnswerData)
    }

    interface Presenter : BasePresenter<View> {
        fun showQa()
        fun postQa(answers: IntArray)
    }
}