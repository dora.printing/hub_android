package io.iost.hub.android.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout

class DividerItemView(context: Context, divider: Drawable? = null, private var mDividerSize: Int = 0, private val mOrientation: Int = LinearLayout.VERTICAL) :
        RecyclerView.ItemDecoration() {

    companion object {
        private const val HORIZONTAL = LinearLayout.HORIZONTAL
        private const val VERTICAL = LinearLayout.VERTICAL
        private val ATTRS = intArrayOf(android.R.attr.listDivider)
    }

    var dividerPadding = 0
    var showLastItemDivider = true

    private val mBounds = Rect()
    private var mDivider: Drawable

    init {
        val a = context.obtainStyledAttributes(ATTRS)
        mDivider = divider ?: a.getDrawable(0)
        if (mDividerSize == 0) {
            mDividerSize = if (mOrientation == VERTICAL) {
                mDivider.intrinsicHeight
            } else {
                mDivider.intrinsicWidth
            }
        }
        a.recycle()
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        if (parent.layoutManager == null) {
            return
        }
        if (mOrientation == VERTICAL) {
            drawVertical(c, parent, state)
        } else {
            drawHorizontal(c, parent, state)
        }
    }

    private fun drawVertical(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        canvas.save()
        val left: Int
        val right: Int

        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            canvas.clipRect(left, parent.paddingTop, right,
                    parent.height - parent.paddingBottom)
        } else {
            left = 0
            right = parent.width
        }
        val realCount = when {
            state == null -> 0
            showLastItemDivider -> state.itemCount
            else -> state.itemCount - 1
        }
        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            if (showLastItemDivider || parent.getChildAdapterPosition(child) < realCount) {
                parent.getDecoratedBoundsWithMargins(child, mBounds)
                val bottom = mBounds.bottom + Math.round(child.translationY)
                val top = bottom - mDividerSize
                mDivider.setBounds(left + dividerPadding, top, right - dividerPadding, bottom)
                mDivider.draw(canvas)
            }
        }
        canvas.restore()
    }

    private fun drawHorizontal(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        canvas.save()
        val top: Int
        val bottom: Int

        if (parent.clipToPadding) {
            top = parent.paddingTop
            bottom = parent.height - parent.paddingBottom
            canvas.clipRect(parent.paddingLeft, top,
                    parent.width - parent.paddingRight, bottom)
        } else {
            top = 0
            bottom = parent.height
        }

        val realCount = when {
            state == null -> 0
            showLastItemDivider -> state.itemCount
            else -> state.itemCount - 1
        }
        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            if (showLastItemDivider || parent.getChildAdapterPosition(child) < realCount) {
                parent.layoutManager.getDecoratedBoundsWithMargins(child, mBounds)
                val right = mBounds.right + Math.round(child.translationX)
                val left = right - mDividerSize
                mDivider.setBounds(left, top + dividerPadding, right, bottom - dividerPadding)
                mDivider.draw(canvas)
            }
        }
        canvas.restore()
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val realCount = when {
            state == null -> 0
            showLastItemDivider -> state.itemCount
            else -> state.itemCount - 1
        }
        val childPosition = parent.getChildAdapterPosition(view)
        if (!showLastItemDivider && childPosition == realCount) {
            outRect.set(0, 0, 0, 0)
        } else {
            if (mOrientation == VERTICAL) {
                outRect.set(0, 0, 0, mDividerSize)
            } else {
                outRect.set(0, 0, mDividerSize, 0)
            }
        }
    }
}