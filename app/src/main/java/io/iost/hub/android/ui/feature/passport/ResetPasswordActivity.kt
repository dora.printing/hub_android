package io.iost.hub.android.ui.feature.passport

import io.iost.hub.android.R
import io.iost.hub.android.extensions.countdown
import io.iost.hub.android.ui.contract.passport.ResetPasswordContract
import io.iost.hub.android.ui.presenter.passport.ResetPasswordPresenter
import io.iost.hub.android.ui.common.SmsCode
import io.iost.hub.android.ui.view.passport.ResetPasswordComponent
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import javax.inject.Inject

class ResetPasswordActivity : BasePassportActivity<ResetPasswordPresenter, ResetPasswordComponent>(),
        ResetPasswordContract.View, SmsCode.SmsCodeCallback {

    override val ui: ResetPasswordComponent = ResetPasswordComponent()

    @Inject
    lateinit var smsCode: SmsCode

    override fun created() {
        super.created()
        val phone = intent.getStringExtra(LoginActivity.PHONE_NUMBER)
        smsCode.registerCallback(this)
        phone?.let {
            ui.phoneNumEdt.setText(it)
        }
    }

    override fun setCountryCode(countryArea: String) {
        ui.countryTxt.text = countryArea
    }

    override fun onCountDown(isFinish: Boolean, time: Int) {
        ui.getCode.countdown(isFinish, time)
    }

    fun onGetCode(phone: String) {
        smsCode.getVerifyCode(phone, mAreaCode, SmsCode.RESET_PASSWORD_TYPE)
    }

    fun confirm(phone: String, smsCode: String, password: String, confirmPassword: String) {
        when {
            phone.isEmpty() -> toast(R.string.passport_phone_empty)
            smsCode.isEmpty() -> toast(R.string.passport_code_empty)
            password.isEmpty() -> toast(R.string.passport_password_empty)
            password.length < 8 -> toast(R.string.passport_password_length)
            confirmPassword != password -> toast(R.string.passport_password_not_mark)
            else -> mPresenter.resetPassword(phone, mAreaCode, password, smsCode)
        }
    }

    override fun resetSuccess(phone: String) {
        startActivity<LoginActivity>(LoginActivity.PHONE_NUMBER to phone)
    }

    override fun onDestroy() {
        super.onDestroy()
        smsCode.unRegisterCallback()
    }

}