package io.iost.hub.android.ui.feature.daily

import android.app.AlertDialog
import android.content.Context
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.hideKeyboard
import io.iost.hub.android.extensions.singleClick
import org.jetbrains.anko.*

class BerminalDialog(ctx: Context) : AlertDialog(ctx) {

    private lateinit var editTxt: EditText
    private lateinit var button: Button

    init {
        setView(ctx.UI {
            frameLayout {
                imageView {
                    imageResource = R.drawable.alert_close
                    padding = dimen(R.dimen.dimen_8)
                    singleClick { dismiss() }
                }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                    gravity = Gravity.END
                }
                verticalLayout {
                    gravity = Gravity.CENTER_HORIZONTAL
                    textView(R.string.daily_berminal_title) {
                        textSizeDimen = R.dimen.material_text_subtitle
                        textColorResource = R.color.black_333333
                    }.lparams {
                        topMargin = dimen(R.dimen.dimen_8)
                    }
                    textView(R.string.daily_berminal_content) {
                        gravity = Gravity.CENTER
                        textSizeDimen = R.dimen.material_text_body
                        textColorResource = R.color.black_666666
                    }.lparams {
                        topMargin = dimen(R.dimen.dimen_16)
                    }
                    textView(R.string.daily_berminal_invite_code) {
                        textSizeDimen = R.dimen.material_text_body
                        textColorResource = R.color.black_333333
                    }.lparams {
                        topMargin = dimen(R.dimen.dimen_20)
                    }
                    editTxt = editText {
                        singleLine = true
                        gravity = Gravity.CENTER
                        textColorResource = R.color.black_666666
                        textSizeDimen = R.dimen.material_text_body1
                        backgroundResource = R.drawable.bg_edittext
                        setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                    }.lparams(dip(210), dimen(R.dimen.edit_height)) {
                        topMargin = dimen(R.dimen.dimen_10)
                    }
                    button = button(R.string.iost_submit) {
                        padding = 0
                        textColorResource = R.color.white
                        backgroundResource = R.drawable.bg_blue_button
                    }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                        topMargin = dimen(R.dimen.dimen_10)
                        bottomMargin = dimen(R.dimen.dimen_10)
                    }
                }.lparams(matchParent, wrapContent) {
                    rightMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                }
            }
        }.view)
    }

    fun setSubmitListener(listener: (String) -> Unit) {
        button.singleClick {
            val text = editTxt.content()
            if (text.isEmpty()) {
                context.toast(R.string.daily_berminal_code_empty)
            } else {
                dismiss()
                listener(text)
            }
        }
    }

    override fun dismiss() {
        editTxt.hideKeyboard()
        super.dismiss()
    }

}