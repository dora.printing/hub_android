package io.iost.hub.android.ui.feature.navigation

import android.app.AlertDialog
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.singleClick
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.viewPager

class QaDialog(ctx: Context) : AlertDialog(ctx) {

    private val mList = arrayOf(intArrayOf(R.string.q_1, R.string.a_1_1, R.string.a_1_2, R.string.a_1_3, R.string.a_all),
            intArrayOf(R.string.q_2, R.string.a_2_1, R.string.a_2_2, R.string.a_2_3, R.string.a_2_4, R.string.a_all))
    private val mAnswers = intArrayOf(-1, -1)

    private lateinit var mSubmitBtn: Button
    private lateinit var mViewpager: ViewPager

    init {
        setView(ctx.UI {
            verticalLayout {
                backgroundColorResource = R.color.white
                gravity = Gravity.CENTER_HORIZONTAL
                textView(R.string.qa_title) {
                    singleLine = true
                    textSizeDimen = R.dimen.material_text_subtitle
                    textColorResource = R.color.black_333333
                    gravity = Gravity.CENTER
                }.lparams(matchParent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
                mViewpager = viewPager {
                    adapter = QaAdapter()
                }.lparams(matchParent, dip(280)) {
                    topMargin = dimen(R.dimen.dimen_16)
                }
                mSubmitBtn = button(R.string.iost_submit) {
                    padding = 0
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_blue_button
                    isEnabled = false
                }.lparams(width = matchParent, height = dimen(R.dimen.edit_height)) {
                    margin = dimen(R.dimen.dimen_16)
                }
            }
        }.view)
        setCanceledOnTouchOutside(false)
    }

    fun setSubmitClick(listener: (IntArray) -> Unit) {
        mSubmitBtn.singleClick {
            listener(mAnswers)
        }
    }

    inner class QaAdapter : PagerAdapter() {

        override fun isViewFromObject(view: View, obj: Any) = view == obj

        override fun getCount() = mList.size

        override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
            container.removeView(obj as View)
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val qaString = mList[position]
            return with(container.context) {
                verticalLayout {
                    gravity = Gravity.CENTER_HORIZONTAL
                    textView(qaString[0]) {
                        textColorResource = R.color.black_333333
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams(width = matchParent, height = wrapContent)
                    radioGroup {
                        orientation = LinearLayout.VERTICAL
                        gravity = Gravity.CENTER
                        for (i in 1 until qaString.size) {
                            radioButton {
                                buttonDrawable = null
                                gravity = Gravity.START or Gravity.CENTER_VERTICAL
                                textResource = qaString[i]
                                textSizeDimen = R.dimen.material_text_body1
                                textColorResource = R.drawable.selector_text_color
                                backgroundResource = R.drawable.selector_bg_grey
                                padding = dimen(R.dimen.dimen_8)
                                gravity = Gravity.CENTER
                                singleClick {
                                    if (position < count - 1) {
                                        mViewpager.currentItem = position + 1
                                    }
                                    mAnswers[position] = i - 1
                                    mSubmitBtn.isEnabled = mAnswers[0] != -1 && mAnswers[1] != -1
                                }
                            }.lparams(width = matchParent, height = wrapContent) {
                                topMargin = dimen(R.dimen.dimen_8)
                            }
                        }
                    }
                }
            }.apply {
                container.addView(this)
            }
        }
    }

}

