package io.iost.hub.android.ui.presenter.news

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.news.NewsContract
import kotlinx.coroutines.experimental.Job

class NewsPresenter(override val view: NewsContract.View, override val job: Job, private val api: HubApiService) :
        NewsContract.Presenter {

    private var mPage = 1
    private val mLimit = 20

    override fun getBanner() {
        callApi(api.getFeatures()) {
            when (it) {
                is DataResult -> {
                    view.showBanner(it.data)
                }
            }
        }
    }

    override fun getNewsData() {
        mPage = 1
        loadNews()
    }

    override fun loadMoreNews() {
        loadNews()
    }

    private fun loadNews() {
        callApi(api.getNews(mPage, mLimit)) {
            when (it) {
                is DataResult -> {
                    if (mPage == 1) {
                        view.setNewsList(it.data)
                    } else {
                        view.loadMoreNews(it.data)
                    }
                    if (it.data.isNotEmpty()) mPage++
                }
            }
        }
    }

}