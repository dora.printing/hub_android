package io.iost.hub.android.ui.feature.passport

import io.iost.hub.android.R
import io.iost.hub.android.extensions.isEmail
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.passport.RegisterNextContract
import io.iost.hub.android.ui.presenter.passport.RegisterNextPresenter
import io.iost.hub.android.ui.view.passport.RegisterNextComponent
import org.jetbrains.anko.startActivity

class RegisterNextActivity : BasePresenterActivity<RegisterNextPresenter, RegisterNextComponent>(),
        RegisterNextContract.View {

    companion object {
        const val PHONE_CODE = "phone_code"
        const val AREA_CODE = "area_code"
        const val PHONE_INVITE = "phone_invite"
    }

    override val ui: RegisterNextComponent = RegisterNextComponent()

    private lateinit var phoneNum: String
    private lateinit var smsCode: String
    private lateinit var areaCode: String
    private lateinit var inviteCode: String

    override fun created() {
        phoneNum = intent.getStringExtra(LoginActivity.PHONE_NUMBER)
        smsCode = intent.getStringExtra(PHONE_CODE)
        areaCode = intent.getStringExtra(AREA_CODE)
        inviteCode = intent.getStringExtra(PHONE_INVITE)
    }

    fun register(password: String, passwordConfirm: String, email: String) {
        when {
            password.isEmpty() -> toastMsg(R.string.passport_password_empty)
            password.length < 8 -> toastMsg(R.string.passport_password_length)
            password != passwordConfirm -> toastMsg(R.string.passport_password_not_mark)
            email.isEmpty() -> toastMsg(R.string.passport_email_empty)
            !email.isEmail() -> toastMsg(R.string.passport_email_invalid)
            else -> mPresenter.register(phoneNum, areaCode, password, smsCode, inviteCode, email)
        }
    }

    override fun onRegisterSuccess(phone: String, password: String) {
        startActivity<LoginActivity>(LoginActivity.PHONE_NUMBER to phone,
                LoginActivity.PHONE_PASSWORD to password)
    }

}