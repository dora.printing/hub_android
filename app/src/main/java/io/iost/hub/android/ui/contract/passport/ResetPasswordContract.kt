package io.iost.hub.android.ui.contract.passport

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface ResetPasswordContract {
    interface View : BaseView {
        fun resetSuccess(phone: String)
    }

    interface Presenter : BasePresenter<View> {
        fun resetPassword(phone: String, areaCode: String, password: String, smsCode: String)
    }
}