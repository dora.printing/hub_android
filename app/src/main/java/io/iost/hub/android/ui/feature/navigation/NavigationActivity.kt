package io.iost.hub.android.ui.feature.navigation

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.gyf.barlibrary.ImmersionBar
import io.iost.hub.android.R
import io.iost.hub.android.model.AnswerData
import io.iost.hub.android.ui.base.activity.BasePresenterActivity
import io.iost.hub.android.ui.contract.navigation.NavigationContract
import io.iost.hub.android.ui.feature.account.aboutus.AboutUsActivity
import io.iost.hub.android.ui.feature.account.balance.BalanceActivity
import io.iost.hub.android.ui.feature.account.referral.ReferralActivity
import io.iost.hub.android.ui.feature.account.setting.SettingActivity
import io.iost.hub.android.ui.feature.account.social.SocialActivity
import io.iost.hub.android.ui.feature.daily.DailyFragment
import io.iost.hub.android.ui.feature.incentives.IncentivesFragment
import io.iost.hub.android.ui.feature.news.NewsFragment
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.presenter.navigation.NavigationPresenter
import io.iost.hub.android.ui.view.navigation.NavigationDrawerComponent
import io.iost.hub.android.upgrade.UpgradeChecker
import io.iost.hub.android.widget.qaAlert
import io.iost.hub.android.widget.shareAlert
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import zendesk.support.guide.HelpCenterActivity
import zendesk.support.request.RequestActivity
import javax.inject.Inject

class NavigationActivity : BasePresenterActivity<NavigationPresenter, NavigationDrawerComponent>(),
        NavigationView.OnNavigationItemSelectedListener,
        NavigationContract.View {

    companion object {
        private const val CURRENT_TAG = "current"
    }

    override val ui = NavigationDrawerComponent()

    @Inject
    lateinit var upgradeChecker: UpgradeChecker

    private val newsFragment by lazy {
        supportFragmentManager.findFragmentByTag(NewsFragment::class.java.simpleName)
                ?: NewsFragment()
    }

    private val incentivesFragment by lazy {
        supportFragmentManager.findFragmentByTag(IncentivesFragment::class.java.simpleName)
                ?: IncentivesFragment()
    }

    private val dailyFragment by lazy {
        supportFragmentManager.findFragmentByTag(DailyFragment::class.java.simpleName)
                ?: DailyFragment()
    }

    private var itemId = 0

    override fun created() {
        val toggle = object : ActionBarDrawerToggle(
                this,
                ui.drawer,
                ui.toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            override fun onDrawerClosed(drawerView: View) {
                when (itemId) {
                    R.id.nav_news -> {
                        switchFragment(newsFragment)
                        ui.toolbar.titleResource = R.string.navigation_news
                    }
                    R.id.nav_daily -> {
                        if (checkHaveLoginOtherwiseExecuteLogin()) {
                            switchFragment(dailyFragment)
                            ui.toolbar.titleResource = R.string.navigation_daily
                        }
                    }
                    R.id.nav_incentives -> {
                        if (checkHaveLoginOtherwiseExecuteLogin()) {
                            switchFragment(incentivesFragment)
                            ui.toolbar.titleResource = R.string.navigation_incentives
                        }
                    }
                }
                itemId = 0
            }
        }
        ui.drawer.addDrawerListener(toggle)
        toggle.syncState()
        mPresenter.showQa()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            currentFragment = supportFragmentManager.findFragmentByTag(savedInstanceState.getString(CURRENT_TAG))
        } else {
            switchFragment(newsFragment)
            ui.toolbar.titleResource = R.string.navigation_news
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        } else {
            checkUpgrade()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 0) {
            permissions.forEachIndexed { index, s ->
                if (s == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    if (grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                        checkUpgrade()
                    } else {
                        toast(R.string.permission_storage_denied_message)
                    }
                } else {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                }
            }
        }
    }

    override fun setupImmersionBar(immersionBar: ImmersionBar) {
        immersionBar.statusBarView(ui.statusView)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        currentFragment?.let {
            outState?.putString(CURRENT_TAG, it.javaClass.simpleName)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        mPresenter.showQa()
    }

    override fun onResume() {
        super.onResume()
        setUserName()
        if (dailyFragment is DailyFragment && dailyFragment.isAdded) {
            (dailyFragment as DailyFragment).refreshFragment()
        }
    }

    private fun setUserName() {
        if (userState.isLogin) {
            ui.titleText.text = getString(R.string.navigation_hi, userState.getPhone())
            ui.contentTxt.text = getString(R.string.account_change_number)
        } else {
            ui.titleText.text = getString(R.string.navigation_please_login)
            ui.contentTxt.text = getString(R.string.navigation_welcome_to_hub)
            switchFragment(newsFragment)
        }
    }

    private var currentFragment: Fragment? = null

    private fun switchFragment(targetFragment: Fragment) {
        if (targetFragment == currentFragment) return
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (!targetFragment.isAdded) {
            if (currentFragment != null) transaction.hide(currentFragment)
            transaction.add(R.id.navigation_fragment, targetFragment, targetFragment.javaClass.simpleName)
                    .commitAllowingStateLoss()
        } else {
            transaction.hide(currentFragment)
                    .show(targetFragment)
                    .commitAllowingStateLoss()
        }
        currentFragment = targetFragment
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        ui.drawer.closeDrawer(GravityCompat.START)
        when (item.itemId) {
            R.id.nav_balance -> {
                if (checkHaveLoginOtherwiseExecuteLogin()) startActivity<BalanceActivity>()
            }
            R.id.nav_referral -> {
                if (checkHaveLoginOtherwiseExecuteLogin()) startActivity<ReferralActivity>()
            }
            R.id.nav_social -> if (checkHaveLoginOtherwiseExecuteLogin()) startActivity<SocialActivity>()
            R.id.nav_setting -> if ((checkHaveLoginOtherwiseExecuteLogin())) startActivity<SettingActivity>()
            R.id.nav_about_us -> if ((checkHaveLoginOtherwiseExecuteLogin())) startActivity<AboutUsActivity>()
            R.id.nav_support -> {
                startActivity(HelpCenterActivity.builder().intent(this))
            }
            R.id.nav_contact -> {
                RequestActivity.builder().show(this)
            }
            else -> itemId = item.itemId
        }
        return true
    }

    private fun checkHaveLoginOtherwiseExecuteLogin(): Boolean {
        if (!userState.isLogin) {
            startActivity<LoginActivity>()
            (ctx as AppCompatActivity).overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out)
        }
        return userState.isLogin
    }

    private var exitTime: Long = 0

    override fun onBackPressed() {
        if (ui.drawer.isDrawerOpen(GravityCompat.START)) {
            ui.drawer.closeDrawer(GravityCompat.START)
        } else {
            if (System.currentTimeMillis() - exitTime > 2000) {
                toast(R.string.press_again_exit)
                exitTime = System.currentTimeMillis()
            } else {
                finish()
                System.exit(0)
            }
        }
    }

    override fun onShowQa() {
        qaAlert {
            setSubmitClick {
                mPresenter.postQa(it)
                dismiss()
            }
        }.show()
    }

    override fun onAnswerQuestion(data: AnswerData) {
        val resultList = data.questResult
        var rightNum = 0
        resultList.forEach {
            if (it) rightNum++
        }
        shareAlert(rightNum == resultList.size) {
            setAnswerTxt(rightNum, resultList.size, data.rewardIostNum)
        }.show()
    }

    private fun checkUpgrade() {
        launch(UI, CoroutineStart.DEFAULT, mPresenter.job) {
            upgradeChecker.check(this@NavigationActivity, false)
        }
    }

    fun startLoginAct() {
        checkHaveLoginOtherwiseExecuteLogin()
    }
}