package io.iost.hub.android.push.jpush

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import cn.jpush.android.api.JPushInterface
import com.orhanobut.logger.Logger
import io.iost.hub.android.push.*


class JPushReceiver : BroadcastReceiver(), IMessage {

    override fun convert(any: Any): Message {
        val intent = any as Intent
        val bundle = intent.extras
        val title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE)
        val content = bundle.getString(JPushInterface.EXTRA_EXTRA)
        val extras = bundle.getString(JPushInterface.EXTRA_ALERT)
        val target = PushTarget.JPUSH.toString()
        return Message(title, content, extras, target)
    }

    override fun onReceive(context: Context, intent: Intent?) {
        intent?.apply {
            val message = convert(this)
            when {
                JPushInterface.ACTION_REGISTRATION_ID == this.action -> {
                    val id = this.extras.getString(JPushInterface.EXTRA_REGISTRATION_ID)
                    id?.let {
                        PushManager.instance.token = id
                    }
                }
                JPushInterface.ACTION_MESSAGE_RECEIVED == this.action -> {
                    showNotification(context, message)
                }
                JPushInterface.ACTION_NOTIFICATION_RECEIVED == this.action -> {
                    showNotification(context, message)
                }
                JPushInterface.ACTION_NOTIFICATION_OPENED == this.action -> {

                }
            }
        }
    }
}