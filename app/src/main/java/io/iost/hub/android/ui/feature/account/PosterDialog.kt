package io.iost.hub.android.ui.feature.account

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.support.v4.view.ViewPager
import android.support.v7.widget.GridLayout
import android.widget.EditText
import android.widget.ImageView
import cn.sharesdk.framework.Platform
import cn.sharesdk.framework.PlatformActionListener
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.shareThird
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.model.PosterData
import io.iost.hub.android.ui.feature.daily.ShareDialog
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import okhttp3.*
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex
import org.jetbrains.anko.gridlayout.v7.gridLayout
import org.jetbrains.anko.support.v4.onPageChangeListener
import org.jetbrains.anko.support.v4.viewPager
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.util.*

class PosterDialog(ctx: Context) : AlertDialog(ctx) {

    private lateinit var mViewPager: ViewPager
    lateinit var description: EditText

    var code: String = ""

    private var position = 1
    private var mData: List<PosterData>? = null

    companion object {
        const val SHARE_IMAGE_DIR = "iost_hub_share"
    }

    private val shareMap = arrayOf(
            Pair("Twitter", R.drawable.share_twitter),
            Pair("Facebook", R.drawable.share_facebook),
            Pair("Reddit", R.drawable.share_reddit),
            Pair("Telegram", R.drawable.share_telegram),
            Pair("email", R.drawable.share_mail),
            Pair("download", R.drawable.ic_cloud_download))

    init {
        setView(ctx.UI {
            relativeLayout {
                textView(R.string.account_poster_title) {
                    id = R.id.poster_alert_title
                    textSize = 15f
                    textColorResource = R.color.black_666666
                }.lparams {
                    centerHorizontally()
                    topMargin = dimen(R.dimen.dimen_8)
                }
                imageView {
                    imageResource = R.drawable.alert_close
                    padding = dimen(R.dimen.dimen_8)
                    singleClick {
                        dismiss()
                    }
                }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                    alignParentRight()
                }
                linearLayout {
                    id = R.id.poster_alert_layout
                    imageView {
                        imageResource = R.drawable.account_poster_left
                        scaleType = ImageView.ScaleType.CENTER_INSIDE
                        padding = dip(38)
                        singleClick {
                            if (position > 0)
                                mViewPager.currentItem = --position
                        }
                    }.lparams(width = 0, height = matchParent, weight = 1f)
                    mViewPager = viewPager {
                        backgroundColorResource = R.color.grey_f5f5f5
                    }.lparams(width = 0, height = matchParent, weight = 1.2f)
                    imageView {
                        imageResource = R.drawable.account_poster_right
                        scaleType = ImageView.ScaleType.CENTER_INSIDE
                        padding = dip(38)
                        singleClick {
                            mViewPager.currentItem = ++position
                        }
                    }.lparams(width = 0, height = matchParent, weight = 1f)
                }.lparams(matchParent, dip(240)) {
                    below(R.id.poster_alert_title)
                    topMargin = dimen(R.dimen.dimen_8)
                }
                gridLayout {
                    id = R.id.poster_alert_share
                    backgroundResource = R.drawable.selector_bg_grey
                    setPadding(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_16))
                    shareMap.forEachWithIndex { i, pair ->
                        imageView {
                            imageResource = pair.second
                            tag = pair.first
                            singleClick {
                                if (tag == "download") {
                                    mData?.let {
                                        val imageUrl = it[position].image
                                        val client = OkHttpClient()
                                        val request = Request.Builder().url(imageUrl).build()
                                        val call = client.newCall(request)
                                        val file = File(Environment.getExternalStorageDirectory(), SHARE_IMAGE_DIR)
                                        if (!file.exists() && !file.mkdirs()) {
                                            toast(R.string.permission_storage_denied_message)
                                            return@singleClick
                                        }
                                        val uri = URI(imageUrl)
                                        val segments = uri.path.split("/")
                                        val imageFile = File(file, segments[segments.size - 1])
                                        if (imageFile.exists()) {
                                            toast(R.string.download_msg_file_reduplicated)
                                            return@singleClick
                                        }
                                        launch(UI) {
                                            toast(R.string.downloading)
                                            val success = async {
                                                download(call, imageFile)
                                            }.await()
                                            if (success) {
                                                ctx.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(imageFile)))
                                            }
                                            toast(if (success) R.string.download_msg_complete else R.string.download_msg_failed)
                                            this@PosterDialog.dismiss()
                                        }
                                    }
                                } else {
                                    val params = "inviteCode=$code"
                                    shareThird(ctx.getString(R.string.share_content) + ShareDialog.SHARE_URL + params, object : PlatformActionListener {
                                        override fun onComplete(p0: Platform?, p1: Int, p2: HashMap<String, Any>?) {
                                            toast(R.string.share_operation_success)
                                        }

                                        override fun onCancel(p0: Platform?, p1: Int) {
                                            toast(R.string.share_operation_cancel)
                                        }

                                        override fun onError(p0: Platform?, p1: Int, p2: Throwable?) {
                                            toast(R.string.share_operation_failed)
                                        }

                                    })
                                }
                            }
                            this@PosterDialog.dismiss()
                        }.lparams(GridLayout.spec(i / 3, 1f), GridLayout.spec(i % 3, 1f)) {
                            margin = dimen(R.dimen.dimen_4)
                            width = 0
                            height = 0
                        }
                    }
                }.lparams(matchParent, dip(140)) {
                    below(R.id.poster_alert_layout)
                    margin = dimen(R.dimen.dimen_16)
                }
                textView(R.string.daily_share_with) {
                    padding = dimen(R.dimen.dimen_8)
                }.lparams(matchParent, wrapContent) {
                    below(R.id.poster_alert_layout)
                    margin = dimen(R.dimen.dimen_16)
                }
                description = editText {
                    id = R.id.poster_alert_description
                    padding = dimen(R.dimen.dimen_4)
                    textColorResource = R.color.black_666666
                    textSizeDimen = R.dimen.material_text_body1
                    backgroundResource = R.drawable.selector_bg_grey
                    isCursorVisible = false
                    singleClick { isCursorVisible = true }
                }.lparams(matchParent, dip(40)) {
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                    below(R.id.poster_alert_share)
                }
                button(R.string.account_poster_copy) {
                    backgroundResource = R.drawable.bg_blue_shape
                    textColorResource = R.color.white
                    textSizeDimen = R.dimen.material_text_button
                    padding = 0
                    singleClick {
                        val cm = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        cm.primaryClip = ClipData.newPlainText("Label", description.content())
                        context.toast(R.string.account_poster_copy_success)
                    }
                }.lparams(matchParent, dimen(R.dimen.button_height)) {
                    topMargin = dimen(R.dimen.dimen_8)
                    leftMargin = dimen(R.dimen.dimen_16)
                    rightMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_16)
                    below(R.id.poster_alert_description)
                }
            }
        }.view)
    }

    fun setPosterData(posterDataList: ArrayList<PosterData>) {
        if (posterDataList.isNotEmpty())
            with(mViewPager) {
                val size = posterDataList.size + 2
                description.setText(posterDataList[0].text)
                adapter = PosterAdapter(posterDataList) //此时list里已经加入了两条为了无限循环的item
                mData = posterDataList
                mViewPager.setCurrentItem(1, false)
                onPageChangeListener {
                    onPageSelected {
                        position = it
                        description.clearFocus()
                        description.isCursorVisible = false
                        description.setText(posterDataList[it].text)
                    }
                    onPageScrollStateChanged { state ->
                        //        若viewpager滑动停止，进行如下操作
                        if (state == ViewPager.SCROLL_STATE_IDLE) {
                            //  若当前为第一张，设置页面为倒数第二张
                            if (position == 0) {
                                mViewPager.setCurrentItem(size - 2, false)
                            } else if (position == posterDataList.size - 1) {
                                //  若当前为倒数第一张，设置页面为第二张
                                mViewPager.setCurrentItem(1, false)
                            }
                        }
                    }
                }
            }
    }

    private fun download(call: Call, imageFile: File): Boolean {
        try {
            val response = call.execute()
            if (null == response || !response.isSuccessful) {
                return false
            }
            val buf = ByteArray(2048)
            val inStream = response.body()?.byteStream()
            val outStream = FileOutputStream(imageFile)
            var current = 0
            try {
                inStream?.let { stream ->
                    var len = stream.read(buf)
                    while (len != -1) {
                        current += len
                        outStream.write(buf, 0, len)
                        len = stream.read(buf)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return false
            } finally {
                outStream.flush()
                inStream?.close()
                outStream.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }
}