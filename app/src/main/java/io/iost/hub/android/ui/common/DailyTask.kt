package io.iost.hub.android.ui.common

import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import cn.sharesdk.framework.Platform
import cn.sharesdk.framework.PlatformActionListener
import cn.sharesdk.framework.ShareSDK
import cn.sharesdk.twitter.Twitter
import com.orhanobut.logger.Logger
import io.iost.hub.android.HubApplication
import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.extensions.isAppInstall
import io.iost.hub.android.model.DailyData
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.widget.berminalAlert
import io.iost.hub.android.widget.shareAlert
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList


@Singleton
class DailyTask @Inject constructor(
        private val ctx: HubApplication,
        private val api: HubApiService,
        private val userState: UserState) {

    private val mJob = Job()
    private val mCallbackList = ArrayList<TaskCallback>()

    companion object {
        const val DAILY_REQUEST_CODE = 3663
        const val BERMINAL_REQUEST_CODE = 3664

        const val GO = 0
        const val CLAIM = 1
        const val CLAIMED = 2
        /**
         * DailyData.type 任务类型
         */
        private const val FAKE = 0
        private const val GITHUB = 1
        private const val REDDIT = 2
        private const val TELEGRAM = 3
        private const val TWITTER = 4
        private const val FOLLOW_TELEGRAM = 5
        private const val LIKE_TWEET = 6
        private const val RE_TWEET = 7
        private const val CHECK_IN = 8
        private const val DRAW = 9
        private const val REGISTER = 10
        private const val INVITE = 11
        private const val DAILY_SHARE = 12
        private const val LINK = 13
        private const val BASE = 14
        private const val FOLLOW_TWITTER = 15
        private const val FOLLOW_REDDIT = 16
        private const val FOLLOW_TWITTER_JIMMY = 17
        private const val LIKE_TWEET_JIMMY = 18
        private const val RE_TWEET_JIMMY = 19
        private const val WEIBO = 20
        private const val FACEBOOK = 21
        private const val WEIXIN = 22
        private const val BERMINAL_LINK = 23

        private const val PKG_TELEGRAM = "org.telegram.messenger"
        private const val PKG_TELEGRAM_X = "org.thunderdog.challegram"
        private const val PKG_TWITTER = "com.twitter.android"
        private const val PKG_GOOGLE_PLAY = "com.android.vending"
        private const val PKG_BERMINAL = "com.berminal.android"
        private const val PKG_BERMINAL_CLASS = "com.dora.berminal.MainActivity"
        private const val PKG_REDDIT = "com.reddit.frontpage"

        private const val TWITTER_TYPE = "twitter"
        private const val TWITTER_IOST = "IOStoken"
        private const val TWITTER_JIMMY = "jimmyzhong_iost"

        private const val GITHUB_OAUTH_URL = "https://github.com/login/oauth/authorize?client_id=%s"
        private const val REDDIT_OAUTH_URL = "https://www.reddit.com/api/v1/authorize"
        private const val TELEGRAM_START = "https://t.me/IOSTHubBot?start=en"
        private const val TELEGRAM_JOIN_GROUP = "https://t.me/officialios"
        private const val TWITTER_URL = "https://twitter.com/%s"
        private const val TWITTER_APP = "https://twitter.com/#!/%s"
        private const val GOOGLE_PLAY = "market://details?id=%s"
        private const val REDDIT_APP_IOST = "https://www.reddit.com/r/IOStoken/"
    }

    fun doOrClaimTask(view: BaseView, item: DailyData) {
        when (item.status) {
            GO -> {
                when (item.type) {
                    GITHUB -> verifyGitHub(view)
                    REDDIT -> verifyReddit(view)
                    TELEGRAM -> launchTelegram(view, TELEGRAM_START)
                    TWITTER -> verifyTwitter(view)
                    FOLLOW_TWITTER, LIKE_TWEET, RE_TWEET -> launchTwitter(view, TWITTER_IOST)
                    FOLLOW_TWITTER_JIMMY, LIKE_TWEET_JIMMY, RE_TWEET_JIMMY -> launchTwitter(view, TWITTER_JIMMY)
                    BERMINAL_LINK -> berminalTask(view)
                    FOLLOW_REDDIT -> launchReddit(view)
                    FOLLOW_TELEGRAM -> launchTelegram(view, TELEGRAM_JOIN_GROUP)
                }
            }
            CLAIM -> {
                drawTask(view, item)
            }
        }
    }

    fun verifyGitHub(view: BaseView) {
        startHttpUrl(view, String.format(GITHUB_OAUTH_URL, ctx.getString(R.string.github_client_id)))
    }

    fun verifyReddit(view: BaseView) {
        val url = "$REDDIT_OAUTH_URL?client_id=${ctx.getString(R.string.reddit_client_id)}&response_type=code&state=${userState.getToken()}&redirect_uri=${ctx.getString(R.string.reddit_redirect_uri)}&duration=permanent&scope=identity"
        startHttpUrl(view, url)
    }


    fun verifyTelegram(view: BaseView) {
        launchTelegram(view, TELEGRAM_START)
    }

    private fun launchTelegram(view: BaseView, action: String) {
        when {
            ctx.isAppInstall(PKG_TELEGRAM) -> launchTelegram(view, PKG_TELEGRAM, action)
            ctx.isAppInstall(PKG_TELEGRAM_X) -> launchTelegram(view, PKG_TELEGRAM_X, action)
            else -> view.toastMsg(R.string.daily_telegram_not_install)
        }
    }

    private fun launchTelegram(view: BaseView, pkgName: String, action: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.`package` = pkgName
        intent.data = Uri.parse(action)
        startActivityIntent(view, intent)
    }

    fun verifyTwitter(view: BaseView) {
        val platform = ShareSDK.getPlatform(Twitter.NAME)
        platform.platformActionListener = object : PlatformActionListener {
            override fun onComplete(platform: Platform?, action: Int, params: HashMap<String, Any>?) {
                platform?.db?.let { db ->
                    params?.let {
                        socialBind(view, TWITTER_TYPE, it["id"].toString(), db.userName, it["id_str"].toString(),
                                db.token, db.tokenSecret, "")
                    }
                }
            }

            override fun onCancel(p0: Platform?, p1: Int) {
            }

            override fun onError(p0: Platform?, p1: Int, p2: Throwable?) {

            }
        }
        platform.showUser(null)
    }

    private fun launchTwitter(view: BaseView, userName: String) {
        if (ctx.isAppInstall(PKG_TWITTER)) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.`package` = PKG_TWITTER
            intent.data = Uri.parse(String.format(TWITTER_APP, userName))
            startActivityIntent(view, intent)
        } else {
            startHttpUrl(view, String.format(TWITTER_URL, userName))
        }
    }

    private fun launchReddit(view: BaseView) {
        if (ctx.isAppInstall(PKG_REDDIT)) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.`package` = PKG_REDDIT
            intent.data = Uri.parse(REDDIT_APP_IOST)
            startActivityIntent(view, intent)
        } else {
            startHttpUrl(view, String.format(TWITTER_URL, REDDIT_APP_IOST))
        }
    }

    private fun berminalTask(view: BaseView) {
        when {
            ctx.isAppInstall(PKG_BERMINAL) -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.setClassName(PKG_BERMINAL, PKG_BERMINAL_CLASS)
                startActivityIntent(view, intent, BERMINAL_REQUEST_CODE)
            }
            ctx.isAppInstall(PKG_GOOGLE_PLAY) -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.`package` = PKG_GOOGLE_PLAY
                intent.data = Uri.parse(String.format(GOOGLE_PLAY, PKG_BERMINAL))
                startActivityIntent(view, intent, BERMINAL_REQUEST_CODE)
            }
            else -> view.toastMsg(R.string.daily_berminal_no_google)
        }
    }

    fun showBerminalAlert(view: BaseView) {
        view.viewCtx.berminalAlert {
            setSubmitListener {
                setBerminalCode(view, it)
            }
        }.show()
    }

    private fun setBerminalCode(view: BaseView, code: String) {
        view.showLoadingDialog()
        callApi(mJob, api.berminalCode(code), view) {
            view.dismissLoading()
            if (it is DataResult) {
                refreshTaskStatus()
            }
        }
    }

    private fun startHttpUrl(view: BaseView, url: String, requestCode: Int = DAILY_REQUEST_CODE) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivityIntent(view, intent, requestCode)
    }

    private fun startActivityIntent(view: BaseView, intent: Intent, requestCode: Int = DAILY_REQUEST_CODE) {
        try {
            when (view) {
                is AppCompatActivity -> view.startActivityForResult(intent, requestCode)
                is Fragment -> view.startActivityForResult(intent, requestCode)
            }
        } catch (t: Throwable) {
            Logger.d(t)
        }
    }

    private fun socialBind(view: BaseView, type: String, id: String, name: String, idStr: String, accessToken: String,
                           assessTokenSecret: String, refreshToken: String) {
        callApi(mJob, api.socialBind(type, id, name, idStr, accessToken, assessTokenSecret, refreshToken), view) {
            view.dismissLoading()
            if (it is DataResult) {
                refreshTaskStatus()
            }
        }
    }

    private fun drawTask(view: BaseView, item: DailyData) {
        view.showLoadingDialog()
        val itemId = item.id.toString()
        callApi(mJob, api.drawTask(itemId, itemId), view) {
            view.dismissLoading()
            when (it) {
                is DataResult -> {
                    refreshTaskStatus()
                    view.viewCtx.shareAlert(true) {
                        code = userState.getCode()
                        setTaskTxt(item.name, item.bonus, item.coinBonus)
                    }.show()
                }
            }
        }
    }

    fun refreshTaskStatus() {
        mCallbackList.forEach {
            it.refreshTaskStatus()
        }
    }

    fun addTaskCallback(callback: TaskCallback) {
        if (!mCallbackList.contains(callback)) mCallbackList.add(callback)
    }

    fun removeTaskCallback(callback: TaskCallback) {
        if (mCallbackList.contains(callback)) mCallbackList.remove(callback)
        mJob.cancel()
    }

    interface TaskCallback {
        fun refreshTaskStatus()
    }

}