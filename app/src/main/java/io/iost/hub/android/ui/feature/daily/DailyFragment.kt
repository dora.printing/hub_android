package io.iost.hub.android.ui.feature.daily

import android.content.Intent
import io.iost.hub.android.R
import io.iost.hub.android.extensions.decimal
import io.iost.hub.android.extensions.drawableLeft
import io.iost.hub.android.model.CheckInData
import io.iost.hub.android.model.DailyCardData
import io.iost.hub.android.model.DailyData
import io.iost.hub.android.ui.base.fragment.BasePresenterFragment
import io.iost.hub.android.ui.contract.daily.DailyContract
import io.iost.hub.android.ui.presenter.daily.DailyPresenter
import io.iost.hub.android.ui.common.DailyTask
import io.iost.hub.android.ui.view.daily.DailyComponent
import org.jetbrains.anko.dimen
import org.jetbrains.anko.support.v4.startActivity
import javax.inject.Inject


class DailyFragment : BasePresenterFragment<DailyPresenter, DailyComponent>(),
        DailyContract.View, DailyTask.TaskCallback {

    override val ui: DailyComponent = DailyComponent()

    @Inject
    lateinit var dailyTask: DailyTask

    private val mAdapter: DailyAdapter = DailyAdapter({
        mPresenter.checkIn(it)
    }, {
        dailyTask.doOrClaimTask(this, it)
    }) {
        startActivity<DailyQuestActivity>(DailyQuestActivity.DAILY_QUEST to it)
    }

    override fun activityCreated() {
        dailyTask.addTaskCallback(this)
        ui.recyclerView.adapter = mAdapter
        mPresenter.countDown()
    }

    override fun loadData(): Boolean {
        mPresenter.getDailyCardData()
        mPresenter.checkInInfo()
        mPresenter.getDailyData()
        return true
    }

    override fun showCheckIn(data: CheckInData) {
        mAdapter.data = data
        mAdapter.notifyItemChanged(0)
    }

    override fun showData(list: List<DailyData>) {
        mAdapter.items = list
    }

    override fun showCardData(data: DailyCardData) {
        with(ui) {
            rewardTxt.text = getString(R.string.daily_total_share, data.totalShare.decimal(2))
            earnedCard.contentTxt.text = data.pointsEarnedToday.decimal(2)
            rewardCard.contentTxt.text = data.estimatedRewardIost.decimal(2)
            with(rankingCard.contentTxt) {
                text = data.ranking.toString()
                when {
                    data.ranking <= 20 -> drawableLeft(R.drawable.daily_cup_golden, dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32))
                    data.ranking <= 100 -> drawableLeft(R.drawable.daily_cup_silver, dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32))
                    data.ranking <= 200 -> drawableLeft(R.drawable.daily_cup_bronze, dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32))
                    else -> {
                    }
                }
            }
            lifetimeCard.contentTxt.text = data.lifetimeReward.decimal(2)
        }
    }

    override fun onCountDown(time: String) {
        ui.timeTxt.text = getString(R.string.daily_countdown, time)
    }

    override fun refreshTaskStatus() {
        mPresenter.getDailyData()
        mPresenter.getDailyCardData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dailyTask.removeTaskCallback(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            DailyTask.DAILY_REQUEST_CODE -> dailyTask.refreshTaskStatus()
            DailyTask.BERMINAL_REQUEST_CODE -> dailyTask.showBerminalAlert(this)
        }
    }

    fun refreshFragment() {
        loadData()
//        showLoadingLayout()
    }

}