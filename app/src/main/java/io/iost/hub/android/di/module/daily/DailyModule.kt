package io.iost.hub.android.di.module.daily

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.FragmentScope
import io.iost.hub.android.di.module.FragmentModule
import io.iost.hub.android.ui.feature.daily.DailyFragment
import io.iost.hub.android.ui.presenter.daily.DailyPresenter
import kotlinx.coroutines.experimental.Job

@Module
class DailyModule : FragmentModule() {

    @FragmentScope
    @Provides
    fun providePresenter(view: DailyFragment, job: Job, api: HubApiService): DailyPresenter = DailyPresenter(view, job, api)

}