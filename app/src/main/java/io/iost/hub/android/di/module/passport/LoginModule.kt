package io.iost.hub.android.di.module.passport

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.presenter.passport.LoginPresenter
import io.iost.hub.android.ui.common.UserState
import kotlinx.coroutines.experimental.Job

@Module
class LoginModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: LoginActivity, job: Job, api: HubApiService, userState: UserState) =
            LoginPresenter(view, job, api, userState)
}