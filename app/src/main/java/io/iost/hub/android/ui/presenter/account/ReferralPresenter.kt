package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.ReferralContract
import kotlinx.coroutines.experimental.Job

class ReferralPresenter(override val view: ReferralContract.View, override val job: Job, private val api: HubApiService) :
        ReferralContract.Presenter {
    override fun getInviteRewards() {
        callApi(api.getInviteRewards()) {
            when (it) {
                is DataResult -> {
                    val rewardData = it.data
                    view.showInviteRewards(rewardData.inviteNum, rewardData.inviteReward.invite)
                }
            }
        }
    }

    override fun getPosterData() {
        view.showLoadingDialog()
        callApi(api.getSharePictures()) {
            view.dismissLoading()
            when (it) {
                is DataResult -> {
                    view.showPostAlert(it.data)
                }
            }
        }
    }

    override fun getReferralData() {
        callApi(api.getReferralData()) {
            when (it) {
                is DataResult -> {
                    view.showReferralList(it.data)
                }
            }
        }
    }
}