package io.iost.hub.android.di.module.passport

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.passport.RegisterActivity
import io.iost.hub.android.ui.presenter.passport.RegisterPresenter
import io.iost.hub.android.ui.common.SmsCode
import kotlinx.coroutines.experimental.Job

@Module
class RegisterModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: RegisterActivity, job: Job, api: HubApiService) = RegisterPresenter(view, job, api)

    @ActivityScope
    @Provides
    fun provideSmsCode(view: RegisterActivity, job: Job, api: HubApiService) = SmsCode(view, job, api)
}