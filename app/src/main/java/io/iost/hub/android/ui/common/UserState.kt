package io.iost.hub.android.ui.common

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import io.iost.hub.android.extensions.edit
import io.iost.hub.android.extensions.genericType
import io.iost.hub.android.model.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserState @Inject constructor(private val preference: SharedPreferences, private val gson: Gson) {

    private companion object {
        const val USER = "user"
    }

    private var mUser: User? = null

    var isLogin: Boolean = false
        private set(value) {
            field = value
        }

    init {
        val userJson = preference.getString(USER, "")
        if (userJson.isNotEmpty()) {
            mUser = gson.fromJson<User>(userJson, genericType<User>().type)
            isLogin = true
        }
    }

    fun onLoginSuccess(user: User) {
        preference.edit {
            putString(USER, gson.toJson(user))
        }
        mUser = user
        isLogin = true
    }

    fun onLoginOut() {
        preference.edit {
            putString(USER, "")
        }
        mUser = null
        isLogin = false
    }

    fun getToken(): String = mUser?.token ?: ""
    fun getPhone(): String = mUser?.phone ?: ""
    fun getAreaCode(): String = mUser?.areaCode ?: ""
    fun getCode(): String = mUser?.code ?: ""
}