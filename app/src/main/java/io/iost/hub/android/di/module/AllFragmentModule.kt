package io.iost.hub.android.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.iost.hub.android.di.FragmentScope
import io.iost.hub.android.di.component.FragmentSubComponent
import io.iost.hub.android.di.module.daily.DailyModule
import io.iost.hub.android.di.module.incentives.IncentivesModule
import io.iost.hub.android.di.module.news.NewsModule
import io.iost.hub.android.ui.feature.daily.DailyFragment
import io.iost.hub.android.ui.feature.incentives.IncentivesFragment
import io.iost.hub.android.ui.feature.news.NewsFragment

@Module(subcomponents = [FragmentSubComponent::class])
abstract class AllFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [NewsModule::class])
    internal abstract fun contributesNewsFragmentInjector(): NewsFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [IncentivesModule::class])
    internal abstract fun contributesIncentivesFragmentInjector(): IncentivesFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [DailyModule::class])
    internal abstract fun contributesDailyFragmentInjector(): DailyFragment

}