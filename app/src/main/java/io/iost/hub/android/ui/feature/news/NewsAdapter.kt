package io.iost.hub.android.ui.feature.news

import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dataFormat
import io.iost.hub.android.extensions.loadUrl
import io.iost.hub.android.extensions.string
import io.iost.hub.android.model.Banner
import io.iost.hub.android.model.NewsData
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import io.iost.hub.android.ui.base.adapter.BaseHeaderAdapter
import io.iost.hub.android.ui.feature.news.banner.BannerAdapter
import io.iost.hub.android.ui.feature.news.banner.BannerPagerListener
import io.iost.hub.android.ui.view.news.NewsHeaderComponent
import io.iost.hub.android.ui.view.news.NewsItemComponent
import org.jetbrains.anko.imageResource

class NewsAdapter(mClickListener: (NewsData) -> Unit) :
        BaseHeaderAdapter<NewsData, NewsAdapter.Component>(mClickListener) {

    companion object {
        private const val TWITTER_TYPE = 1
        private const val MEDIUM_TYPE = 2
        private const val YOUTUBE_TYPE = 3
        private const val BERMINAL_TYPE = 4
    }

    var mBanner: List<Banner>? = null

    override val bind: Component.(position: Int) -> Unit = { position ->
        when (getItemViewType(position)) {
            ITEM_HEADER -> {
                if (this is NewsHeaderComponent && layout.tag == null) {
                    mBanner?.let {
                        viewPager.adapter = BannerAdapter(it)
                        viewPager.addOnPageChangeListener(BannerPagerListener(viewPager, it.size))
                        layout.tag = "header"
                    }
                }
            }
            ITEM_NORMAL -> {
                if (this is NewsItemComponent) {
                    getItem(position)?.let { item ->
                        contentTxt.text = item.content
                        timeTxt.text = item.date.dataFormat()
                        when (item.type) {
                            TWITTER_TYPE -> {
                                if (item.authorAvatar == null) {
                                    avatarImg.imageResource = R.color.transparent
                                } else {
                                    avatarImg.loadUrl(item.authorAvatar, round = true)
                                }
                                titleTxt.visibility = View.GONE
                                nameTxt.text = item.title
                                infoTxt.text = view.string(R.string.account_at_something, item.handle)
                                typeImg.imageResource = R.drawable.news_item_twitter
                                mediaView.setNewsImage(item.images)
                            }
                            MEDIUM_TYPE -> {
                                avatarImg.imageResource = R.drawable.news_avatar_medium
                                setCommonItem(item, "IOS Foundation", "Decentralized Internet of Services")
                                typeImg.imageResource = R.drawable.news_item_medium
                            }
                            YOUTUBE_TYPE -> {
                                avatarImg.imageResource = R.drawable.news_avatar_youtube
                                setCommonItem(item, "IOS Foundation", "1291 Followers")
                                typeImg.imageResource = R.drawable.news_item_youtube
                            }
                            BERMINAL_TYPE -> {
                                avatarImg.imageResource = R.drawable.news_avatar_berminal
                                setCommonItem(item, "BERMINAL", "IOST,IOSToken")
                                typeImg.imageResource = R.drawable.news_item_berminal
                            }
                        }
                    }
                }
            }
        }
    }

    private fun NewsItemComponent.setCommonItem(item: NewsData, name: String, info: String) {
        titleTxt.visibility = View.VISIBLE
        titleTxt.text = item.title
        nameTxt.text = name
        infoTxt.text = info
        mediaView.setCoverImage(item.converImage)
    }

    override fun onHeaderComponent(parent: ViewGroup) = NewsHeaderComponent(parent)

    override fun onNormalComponent(parent: ViewGroup) = NewsItemComponent(parent)

    abstract class Component : AdapterAnkoComponent
}


