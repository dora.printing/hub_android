package io.iost.hub.android.ui.presenter

import io.iost.hub.android.ui.contract.BaseView
import kotlinx.coroutines.experimental.Job

interface BasePresenter<out V : BaseView> {
    val view: V
    val job: Job
    fun onDestroy() {
        job.cancel()
    }
}