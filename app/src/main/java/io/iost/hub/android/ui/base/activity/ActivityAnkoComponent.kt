package io.iost.hub.android.ui.base.activity

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import org.jetbrains.anko.AnkoComponent

interface ActivityAnkoComponent<A : AppCompatActivity> : AnkoComponent<A> {
    val toolbar: Toolbar?
}
