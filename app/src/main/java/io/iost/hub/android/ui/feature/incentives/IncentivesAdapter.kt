package io.iost.hub.android.ui.feature.incentives

import android.view.ViewGroup
import io.iost.hub.android.R
import io.iost.hub.android.model.IncentivesData
import io.iost.hub.android.ui.base.adapter.BaseHeaderAdapter
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import io.iost.hub.android.ui.view.incentives.IncentivesHeaderComponent
import io.iost.hub.android.ui.view.incentives.IncentivesItemComponent
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource

class IncentivesAdapter(mClickListener: (IncentivesData) -> Unit) :
        BaseHeaderAdapter<IncentivesData, IncentivesAdapter.Component>(mClickListener) {

    override val bind: Component.(position: Int) -> Unit = { position ->
        when (getItemViewType(position)) {
            ITEM_NORMAL -> {
                if (this is IncentivesItemComponent) {
                    getItem(position)?.let { item ->
                        titleTxt.text = item.title
                        contentTxt.text = item.content
                        imageView.imageResource = item.resId
                        if (item.state == 0) {
                            button.isEnabled = false
                            button.textResource = R.string.iost_coming_soon
                        } else {
                            button.isEnabled = true
                            button.textResource = R.string.account_application
                        }
                    }
                }
            }
        }
    }

    override fun onHeaderComponent(parent: ViewGroup) = IncentivesHeaderComponent(parent)

    override fun onNormalComponent(parent: ViewGroup) = IncentivesItemComponent(parent)

    abstract class Component : AdapterAnkoComponent
}