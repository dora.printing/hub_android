package io.iost.hub.android.push

import android.content.Context

interface IPushHandler {

    fun onMessageReceived(context: Context?, message: Message)

    fun onMessageClicked(context: Context?, message: Message)

    fun onError(throwable: Throwable)
}