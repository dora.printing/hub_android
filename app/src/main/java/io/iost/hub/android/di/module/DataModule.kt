package io.iost.hub.android.di.module

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import io.iost.hub.android.BuildConfig
import io.iost.hub.android.HubApplication
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.ui.common.UserState
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providePreferences(application: HubApplication): SharedPreferences {
        return application.getSharedPreferences("ich_store", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideCache(context: Context) =
            Cache(context.cacheDir, 10 * 1024 * 1024.toLong())

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache, userState: UserState): OkHttpClient =
            OkHttpClient().newBuilder()
                    .cache(cache)
                    .readTimeout(10_000, TimeUnit.MILLISECONDS)
                    .connectTimeout(10_000, TimeUnit.MILLISECONDS)
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                    })
                    .addInterceptor {
                        val request = it.request()
                        val builder = request.newBuilder()
                        if (request.url().host().contains("iost")) {
                            builder.addHeader("x-access-token", userState.getToken())
                            builder.addHeader("x-version", BuildConfig.VERSION_NAME)
                            builder.addHeader("x-lang", Locale.getDefault().language)
                            builder.addHeader("x-platform", "2")
                        }
                        it.proceed(builder.build())
                    }
                    .build()

    @Provides
    @Singleton
    fun providesGson(): Gson =
            GsonBuilder()
                    .setLenient()
                    .disableHtmlEscaping()
                    .serializeNulls()
                    .setDateFormat(DateFormat.LONG)
                    .setVersion(1.0)
                    .create()

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, client: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(BuildConfig.HOST)
                    .client(client)
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): HubApiService =
            retrofit.create(HubApiService::class.java)

}
