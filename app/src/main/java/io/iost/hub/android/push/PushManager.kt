package io.iost.hub.android.push

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import cn.jpush.android.api.JPushInterface
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.orhanobut.logger.Logger
import io.iost.hub.android.BuildConfig
import io.iost.hub.android.R
import io.iost.hub.android.extensions.aboveApi
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import kotlin.properties.Delegates

class PushManager private constructor() {

    companion object {
        val instance = PushManager()
    }

    private lateinit var target: PushTarget

    var token: String by Delegates.observable("") { _, oldVal, newVal ->
        //todo report new token to server
        if (oldVal != newVal) {
            Logger.i("on new token, old: $oldVal, new: $newVal, push target: $target")
        }
    }

    fun init(application: Application) {
        val resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(application)
        target = if (ConnectionResult.SUCCESS == resultCode) {
            FirebaseApp.initializeApp(application)
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
                if (!it.isSuccessful) {
                    return@addOnCompleteListener
                }
                token = it.result.token
            }
            Logger.i("Init push type [ ${PushTarget.FCM} ], token: $token")
            PushTarget.FCM
        } else {
            JPushInterface.setDebugMode(BuildConfig.DEBUG)
            JPushInterface.init(application)
            Logger.i("Init push type [ ${PushTarget.JPUSH} ]")
            PushTarget.JPUSH
        }
    }
}

@Suppress("MoveLambdaOutsideParentheses")
fun showNotification(context: Context, message: Message) {
    val intent = Intent(context, NavigationActivity::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    val pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT)
    val channelId = context.getString(R.string.default_notification_channel_id)
    val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.ic_stat_ic_notification)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_launcher_foreground))
            // FIXME set notification content icon color
            .setColor(Color.WHITE)
            .setContentTitle(message.title ?: context.getString(R.string.app_name))
            .setContentText(message.content)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
            .setContentIntent(pendingIntent)

    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    // Since android Oreo notification channel is needed.
    aboveApi(Build.VERSION_CODES.O, {
        val channel = NotificationChannel(
                channelId,
                "iost news",
                NotificationManager.IMPORTANCE_DEFAULT)
        channel.setShowBadge(true)
        notificationManager.createNotificationChannel(channel)
    })
    notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
}