package io.iost.hub.android.ui.feature.passport

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.model.Country
import io.iost.hub.android.ui.base.adapter.BaseAdapter
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import org.jetbrains.anko.*

class AreaAdapter(listener: (Country) -> Unit) : BaseAdapter<Country, AreaAdapter.AreaItemComponent>(listener) {

    override val bind: AreaItemComponent.(position: Int) -> Unit = { position ->
        getItem(position)?.let {
            countryTxt.text = it.en
            countryCode.text = "+${it.code}"
        }
    }

    override fun onCreateComponent(parent: ViewGroup, viewType: Int): AreaItemComponent = AreaItemComponent(parent)

    class AreaItemComponent(override val view: ViewGroup) : AdapterAnkoComponent {

        lateinit var countryTxt: TextView
        lateinit var countryCode: TextView
        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            linearLayout {
                countryTxt = textView {
                    gravity = Gravity.CENTER_VERTICAL
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.black_030303
                }.lparams(0, matchParent, weight = 1f)
                countryCode = textView {
                    gravity = Gravity.CENTER_VERTICAL
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.gray_999999
                }.lparams(wrapContent, matchParent)
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                lparams(matchParent, dip(44))
            }
        }
    }
}