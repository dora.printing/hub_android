package io.iost.hub.android.ui.feature.account.balance

import android.view.View
import android.view.ViewGroup
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dataFormat
import io.iost.hub.android.extensions.decimal
import io.iost.hub.android.extensions.string
import io.iost.hub.android.model.BalanceItemData
import io.iost.hub.android.ui.base.adapter.BaseAdapter
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import io.iost.hub.android.ui.view.account.BalanceItemComponent
import org.jetbrains.anko.textResource
import java.text.SimpleDateFormat
import java.util.*

class BalanceAdapter :
        BaseAdapter<BalanceItemData, BalanceItemComponent>() {

    companion object {
        private const val MONTH_MILLISECOND: Long = 30L * 24 * 60 * 60 * 1000
    }

    override fun onCreateComponent(parent: ViewGroup, viewType: Int) =
            BalanceItemComponent(parent)

    override val bind: BalanceItemComponent.(position: Int) -> Unit = { position ->
        getItem(position)?.let { item ->
            title.text = item.desc
            if (item.amount >= 0) {
                state.visibility = View.VISIBLE
                balanceCount.visibility = View.VISIBLE
                withDrawCount.visibility = View.GONE
                balanceCount.text = view.string(R.string.account_balance_amount, item.amount.decimal(2))
                state.textResource = when {
                    !item.createdAt.isAvailable() -> R.string.balance_item_unavailable
                    !item.drawed -> R.string.balance_item_available
                    else -> R.string.balance_item_withdrawn
                }
            } else {
                state.visibility = View.GONE
                balanceCount.visibility = View.GONE
                withDrawCount.visibility = View.VISIBLE
                withDrawCount.text = view.string(R.string.account_balance_amount_, item.amount.decimal(2))
            }
            time.text = item.createdAt.dataFormat("yyyy-MM-dd HH:mm")
        }
    }

    abstract class Component : AdapterAnkoComponent

    private fun String.isAvailable(): Boolean {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val date = sdf.parse(this)
        return System.currentTimeMillis() - date.time >= MONTH_MILLISECOND
    }

}