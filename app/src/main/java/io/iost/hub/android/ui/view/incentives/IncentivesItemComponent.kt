package io.iost.hub.android.ui.view.incentives

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.displayWidth
import io.iost.hub.android.extensions.font
import io.iost.hub.android.extensions.setShapeBackgroundRes
import io.iost.hub.android.extensions.string
import io.iost.hub.android.ui.feature.incentives.IncentivesAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class IncentivesItemComponent(override val view: ViewGroup) : IncentivesAdapter.Component() {

    lateinit var imageView: ImageView
    lateinit var titleTxt: TextView
    lateinit var contentTxt: TextView
    lateinit var button: Button

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        cardView {
            lparams(matchParent) {
                leftMargin = dimen(R.dimen.cardview_margin)
                rightMargin = dimen(R.dimen.cardview_margin)
                bottomMargin = dimen(R.dimen.cardview_margin)
            }
            radius = dimen(R.dimen.cardview_radius).toFloat()
            cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
            verticalLayout {
                padding = dimen(R.dimen.dimen_4)
                imageView = imageView {
                    imageResource = R.mipmap.ic_launcher_round
                    setPadding(dip(20), 0, dip(20), 0)
                }.lparams(dip(110), dip(90)) {
                    topMargin = dimen(R.dimen.dimen_32)
                    setGravity(Gravity.CENTER_HORIZONTAL)
                }
                titleTxt = textView {
                    textColorResource = R.color.black_333333
                    font(string(R.string.font_medium))
                    textSize = 18f
                }.lparams {
                    topMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_32)
                    rightMargin = dimen(R.dimen.dimen_32)
                }
                contentTxt = textView {
                    textColorResource = R.color.black_333333
                    textSize = 13f
                    gravity = Gravity.CENTER
                }.lparams {
                    topMargin = dimen(R.dimen.dimen_8)
                    leftMargin = dimen(R.dimen.dimen_32)
                    rightMargin = dimen(R.dimen.dimen_32)
                }
                button = button {
                    padding = 0
                    textSizeDimen = R.dimen.material_text_button
                    singleLine = true
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_blue_button
                }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                    margin = dimen(R.dimen.dimen_32)
                }
            }.lparams(matchParent)
        }
    }

}

class IncentivesHeaderComponent(override val view: ViewGroup) : IncentivesAdapter.Component() {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        verticalLayout {
            relativeLayout {
                backgroundResource = R.drawable.incentives_item_bg1
                button(R.string.incentives_learn_more) {
                    padding = 0
                    textSizeDimen = R.dimen.material_text_button
                    singleLine = true
                    textColorResource = R.color.black_333333
                    backgroundResource = R.drawable.bg_yellow_shape
                }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                    alignParentRight()
                    alignParentBottom()
                    bottomMargin = dimen(R.dimen.dimen_32)
                    rightMargin = dimen(R.dimen.dimen_32)
                }
                lparams(matchParent, context.displayWidth * 9 / 16)
            }
            textView(R.string.incentives_bounties) {
                textSize = 18f
                textColorResource = R.color.black_333333
            }.lparams {
                topMargin = dimen(R.dimen.dimen_8)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dip(10)
            }
            lparams(matchParent)
        }
    }

}