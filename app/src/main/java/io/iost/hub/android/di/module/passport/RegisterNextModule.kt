package io.iost.hub.android.di.module.passport

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.passport.RegisterNextActivity
import io.iost.hub.android.ui.presenter.passport.RegisterNextPresenter
import kotlinx.coroutines.experimental.Job

@Module
class RegisterNextModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: RegisterNextActivity, job: Job, api: HubApiService) = RegisterNextPresenter(view, job, api)
}