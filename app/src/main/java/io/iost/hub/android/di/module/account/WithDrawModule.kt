package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.balance.WithdrawActivity
import io.iost.hub.android.ui.common.SmsCode
import io.iost.hub.android.ui.presenter.account.WithdrawPresenter
import kotlinx.coroutines.experimental.Job

@Module
class WithDrawModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: WithdrawActivity, job: Job, api: HubApiService) = WithdrawPresenter(view, job, api)

    @ActivityScope
    @Provides
    fun provideSmsCode(view: WithdrawActivity, job: Job, api: HubApiService) = SmsCode(view, job, api)

}