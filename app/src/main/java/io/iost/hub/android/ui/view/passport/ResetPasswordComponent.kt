package io.iost.hub.android.ui.view.passport

import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.passwordType
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.passport.ResetPasswordActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class ResetPasswordComponent : ActivityAnkoComponent<ResetPasswordActivity> {
    override lateinit var toolbar: Toolbar

    lateinit var countryTxt: TextView
    lateinit var phoneNumEdt: EditText
    lateinit var getCode: Button
    private lateinit var mCodeEdt: EditText

    override fun createView(ui: AnkoContext<ResetPasswordActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.setting_reset_password
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            linearLayout {
                backgroundResource = R.drawable.bg_edittext
                countryTxt = textView {
                    textSizeDimen = R.dimen.material_text_body1
                    textColorResource = R.color.blue_0091dc
                    gravity = Gravity.CENTER
                    singleClick { owner.onCountryClick() }
                }.lparams(width = 0, height = matchParent, weight = 1f)
                view {
                    backgroundColorResource = R.color.grey_e8e8e8
                }.lparams(dimen(R.dimen.divider), matchParent) {
                    topMargin = dimen(R.dimen.dimen_4)
                    bottomMargin = dimen(R.dimen.dimen_4)
                }
                phoneNumEdt = editText {
                    background = null
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_PHONE
                    hintResource = R.string.passport_phone
                    textSizeDimen = R.dimen.material_text_body1
                    setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                }.lparams(width = 0, height = dimen(R.dimen.edit_height), weight = 3f)
            }.lparams(width = matchParent, height = dimen(R.dimen.edit_height)) {
                topMargin = dip(25)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            linearLayout {
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                mCodeEdt = editText {
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_PHONE
                    hintResource = R.string.passport_confirmation_code
                    textSizeDimen = R.dimen.material_text_body1
                    backgroundResource = R.drawable.bg_edittext
                    setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                }.lparams(width = 0, height = matchParent, weight = 0.6f) {
                    gravity = Gravity.CENTER_VERTICAL
                    rightMargin = dimen(R.dimen.dimen_10)
                }
                getCode = button {
                    padding = 0
                    singleLine = true
                    textResource = R.string.iost_get_code
                    textSizeDimen = R.dimen.material_text_button
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_light_blue_button
                    singleClick { owner.onGetCode(phoneNumEdt.content()) }
                }.lparams(width = 0, height = matchParent, weight = 0.4f)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
            }
            val password = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_password
                passwordType()
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            val passwordConfirm = editText {
                gravity = Gravity.CENTER_VERTICAL
                backgroundResource = R.drawable.bg_edittext
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_password_confirm
                passwordType()
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                topMargin = dimen(R.dimen.dimen_10)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            button(R.string.confirm) {
                padding = 0
                singleLine = true
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
                singleClick { owner.confirm(phoneNumEdt.content(), mCodeEdt.content(), password.content(), passwordConfirm.content()) }
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                topMargin = dimen(R.dimen.dimen_20)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
        }
    }

}