package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.aboutus.AboutUsActivity
import io.iost.hub.android.ui.presenter.account.AboutUsPresenter
import kotlinx.coroutines.experimental.Job

@Module
class AboutUsModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: AboutUsActivity, job: Job, api: HubApiService) = AboutUsPresenter(view, job, api)
}