package io.iost.hub.android.widget

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import io.iost.hub.android.R
import io.iost.hub.android.coroutines.jobOnUiThread
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.feature.passport.LoginActivity
import org.jetbrains.anko.*

class LoadStateLayout(context: Context) : _FrameLayout(context) {

    companion object {
        const val LOADING = 1//加载中
        const val LOAD_ERROR = 2//加载失败
        const val LOAD_SUCCESS = 3//加载成功
        const val LOGIN_INVALID = 4//登录状态失效
    }

    private lateinit var loadingLayout: LinearLayout
    private lateinit var retryLayout: LinearLayout
    private lateinit var loginInvalidLayout: LinearLayout

    private lateinit var shadow: View
    private lateinit var retryBtn: Button

    init {
        AnkoContext.createDelegate(this).apply {
            backgroundColorResource = R.color.white
            shadow = view {
                backgroundResource = R.drawable.bg_shadow
                lparams(matchParent, dip(1))
            }
            loadingLayout = verticalLayout {
                gravity = Gravity.CENTER_HORIZONTAL
                view().lparams(0, 0, 1f)
                progressBar().lparams(dimen(R.dimen.dimen_40), dimen(R.dimen.dimen_40))
                view().lparams(0, 0, 2f)
            }.lparams(matchParent, matchParent)
            retryLayout = verticalLayout {
                visibility = View.GONE
                gravity = Gravity.CENTER_HORIZONTAL
                view().lparams(0, 0, 1f)
                imageView(R.drawable.default_loading_error)
                textView(R.string.loading_oops) {
                    textSize = 24f
                    textColorResource = R.color.black_666666
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                }
                textView(R.string.loading_problem_network) {
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.gray_999999
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                }
                retryBtn = button(R.string.loading_retry) {
                    backgroundResource = R.drawable.bg_blue_button
                    padding = 0
                    textColorResource = R.color.white
                }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                    topMargin = dimen(R.dimen.dimen_32)
                }
                view().lparams(0, 0, 2f)
            }.lparams(matchParent, matchParent)
            loginInvalidLayout = verticalLayout {
                visibility = View.GONE
                gravity = Gravity.CENTER_HORIZONTAL
                view().lparams(0, 0, 1f)
                textView(R.string.login_invalid) {
                    textSize = 24f
                    textColorResource = R.color.black_666666
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                }
                textView(R.string.login_again) {
                    textSizeDimen = R.dimen.material_text_body
                    textColorResource = R.color.gray_999999
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dimen(R.dimen.dimen_10)
                }
                button(R.string.login_go) {
                    backgroundResource = R.drawable.bg_blue_button
                    padding = 0
                    textColorResource = R.color.white
                    singleClick { startActivity<LoginActivity>() }
                }.lparams(dimen(R.dimen.button_width), dimen(R.dimen.button_height)) {
                    topMargin = dimen(R.dimen.dimen_32)
                }
                view().lparams(0, 0, 2f)
            }.lparams(matchParent, matchParent)
        }
    }

    fun setLoadingState(state: Int) {
        jobOnUiThread {
            when (state) {
                LOADING -> {
                    retryLayout.visibility = View.GONE
                    loadingLayout.visibility = View.VISIBLE
                    loginInvalidLayout.visibility = View.GONE
                }
                LOAD_ERROR -> {
                    retryLayout.visibility = View.VISIBLE
                    loadingLayout.visibility = View.GONE
                    loginInvalidLayout.visibility = View.GONE
                }
                LOAD_SUCCESS -> {
                    parent?.let {
                        try {
                            (parent as ViewGroup).removeView(this@LoadStateLayout)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
                LOGIN_INVALID -> {
                    retryLayout.visibility = View.GONE
                    loadingLayout.visibility = View.GONE
                    loginInvalidLayout.visibility = View.VISIBLE
                }

                else -> {

                }
            }
        }
    }

    fun setRetryListener(l: (android.view.View?) -> Unit) {
        retryBtn.singleClick(l)
    }

}