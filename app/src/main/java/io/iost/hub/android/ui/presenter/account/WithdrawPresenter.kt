package io.iost.hub.android.ui.presenter.account

import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.contract.account.WithdrawContract
import kotlinx.coroutines.experimental.Job

class WithdrawPresenter(override val view: WithdrawContract.View, override val job: Job, private val api: HubApiService) :
        WithdrawContract.Presenter {

    override fun getDrawAbleAmount() {
        callApi(api.getDrawableAmount()) {
            when (it) {
                is DataResult -> {
                    view.showDrawAbleAmount(it.data.amount)
                }
            }
        }
    }

    override fun drawIost(smsCode: String, transferAddress: String) {
        when {
            smsCode.isEmpty() -> view.toastMsg(R.string.passport_code_empty)
            else -> {
                view.showLoadingDialog()
                callApi(api.drawIost(smsCode, transferAddress)) {
                    view.dismissLoading()
                    when (it) {
                        is DataResult -> {
                            view.onWithdrawSuccess()
                        }
                    }
                }
            }
        }
    }

}