package io.iost.hub.android.ui.view.account

import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.drawable
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.referral.ReferralActivity
import io.iost.hub.android.widget.DividerItemView
import io.iost.hub.android.widget.hubToolbar
import io.iost.hub.android.widget.smartRefreshLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.recyclerview.v7.recyclerView

class ReferralComponent : ActivityAnkoComponent<ReferralActivity> {

    override lateinit var toolbar: Toolbar
    lateinit var recyclerView: RecyclerView
    lateinit var posterBtn: Button
    lateinit var inviteTxt: TextView
    lateinit var referredTxt: TextView
    lateinit var refreshLayout: SmartRefreshLayout

    override fun createView(ui: AnkoContext<ReferralActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.navigation_referral
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            refreshLayout = smartRefreshLayout {
                setEnableLoadMore(false)
                verticalLayout {
                    verticalLayout {
                        backgroundColorResource = R.color.purple_393951
                        textView(R.string.account_my_referral) {
                            textColorResource = R.color.white
                            textSize = 18f
                        }.lparams {
                            topMargin = dimen(R.dimen.dimen_16)
                            leftMargin = dimen(R.dimen.dimen_16)
                        }
                        inviteTxt = textView {
                            textColorResource = R.color.gray_999999
                            textSize = 13f
                        }.lparams {
                            topMargin = dimen(R.dimen.dimen_4)
                            bottomMargin = dimen(R.dimen.dimen_10)
                            leftMargin = dimen(R.dimen.dimen_16)
                        }
                        view {
                            backgroundColorResource = R.color.gray_999999
                        }.lparams(matchParent, dimen(R.dimen.divider)) {
                            leftMargin = dimen(R.dimen.dimen_8)
                            rightMargin = dimen(R.dimen.dimen_8)
                        }
                        relativeLayout {
                            view {
                                id = R.id.referral_center_view
                            }.lparams(0, 0) {
                                centerHorizontally()
                            }
                            textView(R.string.account_referral_friends) {
                                id = R.id.referral_friend
                                textSize = 13f
                                textColorResource = R.color.gray_999999
                            }.lparams(matchParent, wrapContent) {
                                leftOf(R.id.referral_center_view)
                                topMargin = dimen(R.dimen.dimen_10)
                                leftMargin = dimen(R.dimen.dimen_16)
                            }
                            referredTxt = textView("0") {
                                textSize = 14f
                                setShadowLayer(2F, 2F, 2F, Color.LTGRAY)
                                textColorResource = R.color.white
                            }.lparams(matchParent, wrapContent) {
                                alignStart(R.id.referral_friend)
                                bottomMargin = dimen(R.dimen.dimen_16)
                                below(R.id.referral_friend)
                            }
                        }.lparams(matchParent, wrapContent)
                    }.lparams(matchParent, wrapContent)
                    recyclerView = recyclerView {
                        layoutManager = LinearLayoutManager(context)
                        addItemDecoration(DividerItemView(ctx, ctx.drawable(R.color.gray_e5e5e5), dimen(R.dimen.divider)).apply {
                            dividerPadding = dimen(R.dimen.dimen_8)
                            showLastItemDivider = false
                        })
                    }
                }
            }.lparams(width = matchParent, height = 0, weight = 1f)
            linearLayout {
                elevation = 8f
                backgroundColorResource = R.color.white
                posterBtn = button(R.string.account_exclusive_poster) {
                    padding = 0
                    textSizeDimen = R.dimen.material_text_button
                    singleLine = true
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_blue_shape
                    singleClick { owner.sharePostAlert() }
                }.lparams(0, dimen(R.dimen.button_height), weight = 1f) {
                    topMargin = dip(10)
                    bottomMargin = dip(10)
                    rightMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                }
            }
            lparams(matchParent, matchParent)
        }
    }

}