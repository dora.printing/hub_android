package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.social.SocialActivity
import io.iost.hub.android.ui.presenter.account.SocialPresenter
import kotlinx.coroutines.experimental.Job

@Module
class SocialModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: SocialActivity, job: Job, api: HubApiService) = SocialPresenter(view, job, api)
}