package io.iost.hub.android

import android.app.Activity
import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.os.Process
import android.support.multidex.MultiDex
import android.support.v4.app.Fragment
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.mob.MobSDK
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.LogStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.footer.ClassicsFooter
import com.scwang.smartrefresh.layout.header.ClassicsHeader
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.fabric.sdk.android.Fabric
import io.iost.hub.android.di.component.DaggerApplicationComponent
import io.iost.hub.android.push.PushManager
import zendesk.core.AnonymousIdentity
import zendesk.core.Zendesk
import zendesk.support.Support
import javax.inject.Inject

class HubApplication : Application(), HasActivityInjector, HasSupportFragmentInjector {

    private companion object {
        const val ZENDESK_URL = "https://iost.zendesk.com"
    }

    @Inject
    lateinit var dispatchingAndroidInjectorFragment: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjectorFragment
    }

    @Inject
    lateinit var dispatchingAndroidInjectorActivity: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjectorActivity
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        PushManager.instance.init(this)
        Fabric.with(this, Crashlytics())
        LeakCanary.install(this)
        if (isMainProcess()) {
            DaggerApplicationComponent.builder().application(this).build().inject(this)
            setUpRefreshSupport()
            initZendesk()
            MobSDK.init(this)
            val formatStrategy = if (BuildConfig.DEBUG) PrettyFormatStrategy.newBuilder()
                    .logStrategy(LogCatStrategy())
                    .methodCount(1)
                    .tag("IOST-HUB")
                    .build() else PrettyFormatStrategy.newBuilder().build()
            Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
                override fun isLoggable(priority: Int, tag: String?): Boolean {
                    return BuildConfig.DEBUG
                }
            })
        }
    }

    private fun initZendesk() {
        Zendesk.INSTANCE.init(this, ZENDESK_URL,
                getString(R.string.zendesk_app_id),
                getString(R.string.zendesk_client_id))
        Zendesk.INSTANCE.setIdentity(AnonymousIdentity())
        Support.INSTANCE.init(Zendesk.INSTANCE)
    }

    private fun isMainProcess(): Boolean {
        val pid = Process.myPid()
        var processName: String? = null
        val manager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (processInfo in manager.runningAppProcesses) {
            if (processInfo.pid == pid) {
                processName = processInfo.processName
                break
            }
        }
        return processName != null && processName == this.packageName
    }

    private fun setUpRefreshSupport() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
            layout.setPrimaryColorsId(R.color.transparent, R.color.black_333333)//全局设置主题颜色
            ClassicsHeader(context)//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header

        }
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            //指定为经典Footer，默认是 BallPulseFooter
            ClassicsFooter(context).setDrawableSize(20f)
        }
    }

    class LogCatStrategy : LogStrategy {

        override fun log(priority: Int, tag: String?, message: String) {
            Log.println(priority, randomKey() + tag, message)
        }

        private var last: Int = 0

        private fun randomKey(): String {
            var random = (10 * Math.random()).toInt()
            if (random == last) {
                random = (random + 1) % 10
            }
            last = random
            return random.toString()
        }
    }
}