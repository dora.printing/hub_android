package io.iost.hub.android.ui.view.passport

import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.content
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.feature.passport.RegisterActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class RegisterComponent : ActivityAnkoComponent<RegisterActivity> {
    override lateinit var toolbar: Toolbar
    lateinit var countryTxt: TextView
    private lateinit var mPhoneEdt: EditText
    private lateinit var mCodeEdt: EditText
    private lateinit var mInviteCode: EditText
    lateinit var getCode: Button

    override fun createView(ui: AnkoContext<RegisterActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.passport_register
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            imageView(R.drawable.passport_logo).lparams {
                topMargin = dip(25)
                gravity = Gravity.CENTER_HORIZONTAL
            }
            linearLayout {
                backgroundResource = R.drawable.bg_edittext
                countryTxt = textView {
                    textSizeDimen = R.dimen.material_text_body1
                    textColorResource = R.color.blue_0091dc
                    gravity = Gravity.CENTER
                    singleClick { owner.onCountryClick() }
                }.lparams(width = 0, height = matchParent, weight = 1f)
                view {
                    backgroundColorResource = R.color.grey_e8e8e8
                }.lparams(dimen(R.dimen.divider), matchParent) {
                    topMargin = dimen(R.dimen.dimen_4)
                    bottomMargin = dimen(R.dimen.dimen_4)
                }
                mPhoneEdt = editText {
                    background = null
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_PHONE
                    hintResource = R.string.passport_phone
                    textSizeDimen = R.dimen.material_text_body1
                    setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                }.lparams(width = 0, height = dimen(R.dimen.edit_height), weight = 3f)
            }.lparams(width = matchParent, height = dimen(R.dimen.edit_height)) {
                topMargin = dip(25)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            linearLayout {
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                mCodeEdt = editText {
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_PHONE
                    hintResource = R.string.passport_confirmation_code
                    textSizeDimen = R.dimen.material_text_body1
                    backgroundResource = R.drawable.bg_edittext
                    setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
                }.lparams(width = 0, height = dimen(R.dimen.edit_height), weight = 0.6f) {
                    gravity = Gravity.CENTER_VERTICAL
                    rightMargin = dimen(R.dimen.dimen_10)
                }
                getCode = button {
                    padding = 0
                    singleLine = true
                    textResource = R.string.iost_get_code
                    textSizeDimen = R.dimen.material_text_button
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_light_blue_button
                    singleClick { owner.onGetCode(mPhoneEdt.content()) }
                }.lparams(width = 0, height = dimen(R.dimen.edit_height), weight = 0.4f) {
                    topMargin = dimen(R.dimen.dimen_10)
                    bottomMargin = dimen(R.dimen.dimen_10)
                }
            }
            mInviteCode = editText {
                gravity = Gravity.CENTER_VERTICAL
                textSizeDimen = R.dimen.material_text_body1
                hintResource = R.string.passport_invite_code
                backgroundResource = R.drawable.bg_edittext
                setPadding(dimen(R.dimen.dimen_10), 0, dimen(R.dimen.dimen_10), 0)
            }.lparams(matchParent, dimen(R.dimen.edit_height)) {
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            button(R.string.iost_next) {
                padding = 0
                singleLine = true
                textSizeDimen = R.dimen.material_text_button
                textColorResource = R.color.white
                backgroundResource = R.drawable.bg_light_blue_button
                singleClick { owner.onNextClick(mPhoneEdt.content(), mCodeEdt.content(), mInviteCode.content()) }
            }.lparams(matchParent, dimen(R.dimen.button_height)) {
                topMargin = dimen(R.dimen.dimen_20)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
            }
            view().lparams(width = 0, height = 0, weight = 1f)
            view {
                backgroundColorResource = R.color.grey_e8e8e8
            }.lparams(matchParent, dimen(R.dimen.divider))
            textView {
                text = ctx.getText(R.string.passport_to_login)
                textSizeDimen = R.dimen.material_text_body2
                gravity = Gravity.CENTER
                singleClick { startActivity<LoginActivity>() }
            }.lparams(matchParent, dimen(R.dimen.dimen_40))
        }
    }

}