package io.iost.hub.android.ui.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import io.iost.hub.android.extensions.singleClick
import kotlin.properties.Delegates

abstract class BaseAdapter<Item, Component : AdapterAnkoComponent>(
        private val listener: (Item) -> Unit = {}) : RecyclerView.Adapter<BaseAdapter.BaseViewHolder<AdapterAnkoComponent>>() {

    open val isShowEmptyView = true

    companion object {
        const val ITEM_EMPTY = 4001
        const val ITEM_NORMAL = 4002
    }

    abstract val bind: Component.(position: Int) -> Unit

    private var mIsLoadData = false //刚进入页面不显示数据为空

    var items: List<Item> by Delegates.observable(emptyList()) { _, _, _ ->
        mIsLoadData = true
        notifyDataSetChanged()
    }

    fun loadMore(moreList: List<Item>) {
        if (items is ArrayList<Item>) {
            with(items as ArrayList<Item>) {
                addAll(moreList)
                notifyDataSetChanged()
            }
        }
    }

    abstract fun onCreateComponent(parent: ViewGroup, viewType: Int): Component

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<AdapterAnkoComponent> =
            BaseViewHolder(onComponent(parent, viewType))

    private fun onComponent(parent: ViewGroup, viewType: Int): AdapterAnkoComponent = when (viewType) {
        ITEM_EMPTY -> NoneAnkoComponent(parent)
        else -> onCreateComponent(parent, viewType)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: BaseViewHolder<AdapterAnkoComponent>, position: Int) {
        if (getItemViewType(position) != ITEM_EMPTY) {
            holder.ui as Component
            getItem(position)?.let { item ->
                holder.itemView.singleClick { listener(item) }
            }
            holder.ui.bind(position)
        }
    }

    override fun getItemViewType(position: Int) = when {
        isEmptyType() -> ITEM_EMPTY
        else -> ITEM_NORMAL
    }

    open fun getItem(position: Int): Item? {
        return if (position >= 0 && position < items.size) items[position]
        else null
    }

    override fun getItemCount() = if (isEmptyType()) 1 else items.size

    private fun isEmptyType() = isShowEmptyView && mIsLoadData && items.isEmpty()

    class BaseViewHolder<out Component : AdapterAnkoComponent>(val ui: Component) :
            RecyclerView.ViewHolder(ui.inflate())
}