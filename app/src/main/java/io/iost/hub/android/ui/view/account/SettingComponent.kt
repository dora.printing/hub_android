package io.iost.hub.android.ui.view.account

import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.drawableRight
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.setting.SettingActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class SettingComponent : ActivityAnkoComponent<SettingActivity> {

    override lateinit var toolbar: Toolbar

    lateinit var loginVisible: LinearLayout

    override fun createView(ui: AnkoContext<SettingActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_setting
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            textView(R.string.setting_language) {
                gravity = Gravity.CENTER_VERTICAL
                textSize = 14f
                textColorResource = R.color.black_030303
                drawableRight(R.drawable.account_next)
                visibility = View.GONE
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                singleClick { owner.onLanguage() }
            }.lparams(matchParent, dip(44))
            view {
                backgroundColorResource = R.color.gray_d8d8d8
            }.lparams(matchParent, dimen(R.dimen.divider))
            loginVisible = verticalLayout {
                textView(R.string.setting_reset_password) {
                    gravity = Gravity.CENTER_VERTICAL
                    textSize = 14f
                    textColorResource = R.color.black_030303
                    drawableRight(R.drawable.account_next)
                    setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                    singleClick { owner.onResetPassword() }
                }.lparams(matchParent, dip(44))
                view {
                    backgroundColorResource = R.color.gray_d8d8d8
                }.lparams(matchParent, dimen(R.dimen.divider))
                textView(R.string.setting_log_out) {
                    gravity = Gravity.CENTER_VERTICAL
                    textSize = 14f
                    textColorResource = R.color.black_030303
                    drawableRight(R.drawable.account_next)
                    setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                    singleClick { owner.onLogout() }
                }.lparams(matchParent, dip(44))
                view {
                    backgroundColorResource = R.color.gray_d8d8d8
                }.lparams(matchParent, dimen(R.dimen.divider))
            }
            lparams(matchParent, matchParent)
        }
    }

}