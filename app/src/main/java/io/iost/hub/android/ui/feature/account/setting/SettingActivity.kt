package io.iost.hub.android.ui.feature.account.setting

import android.view.View
import io.iost.hub.android.R
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.feature.passport.LoginActivity
import io.iost.hub.android.ui.feature.passport.ResetPasswordActivity
import io.iost.hub.android.ui.view.account.SettingComponent
import org.jetbrains.anko.*

class SettingActivity : BaseActivity<SettingComponent>() {

    override val ui: SettingComponent = SettingComponent()

    override val isInject: Boolean = true


    override fun created() {
    }

    fun onLanguage() {
        val list = ArrayList<CharSequence>()
        list.add(getString(R.string.setting_language_en))
        list.add(getString(R.string.setting_language_zh))
        selector(getString(R.string.setting_language), list) { it, position ->
            toast(list[position])
            it.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        ui.loginVisible.visibility = if (userState.isLogin) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    fun onResetPassword() {
        startActivity<ResetPasswordActivity>()
    }

    fun onLogout() {
        alert(R.string.setting_log_out_confirm, R.string.setting_log_out) {
            yesButton {
                userState.onLoginOut()
                startActivity<LoginActivity>()
            }
            noButton {}
        }.show()

    }

}