package io.iost.hub.android.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.request.RequestOptions
import io.iost.hub.android.R
import io.iost.hub.android.extensions.displayWidth
import io.iost.hub.android.extensions.loadUrl
import io.iost.hub.android.extensions.resize
import io.iost.hub.android.model.NewsImage
import org.jetbrains.anko.*

class NewsMediaView constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        RelativeLayout(context, attrs, defStyleAttr) {

    private var mWidth = 0
    private var mHeight = 0

    private val images = arrayOfNulls<ImageView>(4)

    init {
        mWidth = (context.displayWidth - dip(100)) / 2
        mHeight = mWidth * 9 / 16
        initLayout()
    }

    private fun initLayout() = AnkoContext.createDelegate(this).apply {
        relativeLayout {
            images[2] = imageView {
                id = R.id.news_item_image3
                scaleType = ImageView.ScaleType.FIT_XY
            }.lparams(width = mWidth, height = mHeight) {
                alignParentBottom()
                alignParentRight()
                topMargin = 10
                leftMargin = 10
            }
            images[3] = imageView {
                id = R.id.news_item_image4
                scaleType = ImageView.ScaleType.FIT_XY
            }.lparams(width = matchParent, height = mHeight) {
                leftOf(R.id.news_item_image3)
                alignParentBottom()
                topMargin = 10
            }
            images[1] = imageView {
                id = R.id.news_item_image2
                scaleType = ImageView.ScaleType.FIT_XY
            }.lparams(width = mWidth, height = matchParent) {
                above(R.id.news_item_image3)
                alignParentRight()
                leftMargin = 10
            }
            images[0] = imageView {
                id = R.id.news_item_image1
                scaleType = ImageView.ScaleType.FIT_XY
            }.lparams(matchParent, matchParent) {
                leftOf(R.id.news_item_image2)
                above(R.id.news_item_image4)
            }
        }
    }

    fun setNewsImage(list: List<NewsImage>?) {
        if (list == null || list.isEmpty()) {
            visibility = View.GONE
            return
        }
        visibility = View.VISIBLE
        resize(mWidth * 2 + 10, mHeight * 2 + 10)
        initImageVisible(list.size)
        when (list.size) {
            1 -> {
                images[0]?.loadUrl(list[0].image, requestOptions = RequestOptions().apply {
                    override(mWidth * 2 + 10, mHeight * 2 + 10)
                })
            }
            2 -> {
                val options = RequestOptions().apply {
                    override(mWidth, mHeight * 2 + 10)
                }
                images[0]?.loadUrl(list[0].image, requestOptions = options)
                images[1]?.loadUrl(list[1].image, requestOptions = options)
            }
            3 -> {
                val options = RequestOptions().apply {
                    override(mWidth, mHeight)
                }
                images[0]?.loadUrl(list[0].image, requestOptions = RequestOptions().apply {
                    override(mWidth, mHeight * 2 + 10)
                })
                images[1]?.loadUrl(list[1].image, requestOptions = options)
                images[2]?.loadUrl(list[2].image, requestOptions = options)
            }
            4 -> {
                val options = RequestOptions().apply {
                    override(mWidth, mHeight)
                }
                images[0]?.loadUrl(list[0].image, requestOptions = options)
                images[1]?.loadUrl(list[1].image, requestOptions = options)
                images[2]?.loadUrl(list[3].image, requestOptions = options)
                images[3]?.loadUrl(list[2].image, requestOptions = options)
            }
        }
    }

    fun setCoverImage(url: String?) {
        if (url == null) {
            visibility = View.GONE
        } else {
            visibility = View.VISIBLE
            initImageVisible(1)
            images[0]?.loadUrl(url, requestOptions = RequestOptions().apply {
                override(mWidth * 2 + 10, mHeight * 2 + 10)
            })
        }
    }

    private fun initImageVisible(size: Int) {
        for (item in images.withIndex()) {
            item.value?.let {
                if (item.index < size) it.visibility = View.VISIBLE
                else it.visibility = View.GONE
            }
        }
    }
}
