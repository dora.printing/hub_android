package io.iost.hub.android.widget

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.color
import io.iost.hub.android.extensions.setRadiusBackground
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex

class DailyCheckInView(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        LinearLayout(context, attrs, defStyleAttr) {

    private lateinit var mArr: Array<Int>
    private lateinit var mImages: Array<ImageView?>
    private lateinit var mLineImages: Array<ImageView?>

    init {
        orientation = LinearLayout.VERTICAL
    }

    private fun levelTexts() = _LinearLayout(context).apply {
        if (mArr.isNotEmpty()) {
            textView("+${mArr[0]}") {
                gravity = Gravity.CENTER
                setPadding(0, 0, dip(4), 0)
            }.lparams(width = dip(28), height = dip(21))
            for (i in 1 until mArr.size) {
                view {}.lparams(width = 0, height = 0, weight = 1f)
                textView("+${mArr[i]}") {
                    gravity = Gravity.CENTER
                }.lparams(width = dip(32), height = dip(21))
            }
        }
    }

    private fun levelLayout() = _FrameLayout(context).apply {
        // 7 个圆
        linearLayout {
            mImages.forEachWithIndex { position, _ ->
                if (position != 0) {
                    view {
                    }.lparams(width = 0, height = 0, weight = 1f)
                }
                view {
                    backgroundResource = R.drawable.daily_check_in_circle
                }.lparams(dip(24), dip(24))
            }
        }.lparams(matchParent, wrapContent) {
            gravity = Gravity.CENTER_VERTICAL
        }
        //横线
        linearLayout {
            mLineImages.forEachWithIndex { position, _ ->
                mLineImages[position] = imageView {
                    setRadiusBackground(color(R.color.white), 0, color(R.color.blue_0091dc))
                    setPadding(0, dip(2), 0, dip(2))
                }.lparams(width = 0, height = dip(10), weight = 1f)
            }
        }.lparams(matchParent, wrapContent) {
            leftMargin = dip(12)
            rightMargin = dip(12)
            gravity = Gravity.CENTER_VERTICAL
        }
        linearLayout {
            mImages.forEachWithIndex { position, _ ->
                if (position != 0) {
                    view {}.lparams(width = 0, height = 0, weight = 1f)
                }
                mImages[position] = imageView {
                    padding = dip(1)
                    backgroundResource = R.drawable.bg_white_circle
                    imageResource = R.drawable.daily_check_in_default
                }.lparams(dip(22), dip(22))
            }
        }.lparams(matchParent, wrapContent) {
            leftMargin = dip(1)
            rightMargin = dip(1)
            gravity = Gravity.CENTER_VERTICAL
        }
        //为了适配上边的数字
        lparams(matchParent, wrapContent) {
            rightMargin = dip(4)
            topMargin = dip(4)
        }
    }

    private var mLevel: Int = 0

    fun setDefaultLevels(arr: Array<Int>) {
        mArr = arr
        mImages = arrayOfNulls(mArr.size)
        mLineImages = arrayOfNulls(mArr.size - 1)
        removeAllViews()
        addView(levelTexts())
        addView(levelLayout())
    }

    fun setLevels(level: Int) {
        if (level >= 0) {
            for (i in 0 until mImages.size) {
                if (level >= mArr[i]) mImages[i]?.imageResource = R.drawable.daily_check_in_checked
                else mImages[i]?.imageResource = R.drawable.daily_check_in_default
                if (i > 0) {
                    if (level >= mArr[i]) mLineImages[i - 1]?.imageResource = R.color.blue_0091dc
                    else mLineImages[i - 1]?.imageResource = R.color.transparent
                }
            }
            mLevel = level
        }
    }

}