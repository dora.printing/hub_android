package io.iost.hub.android.ui.base.adapter

import android.view.ViewGroup

abstract class BaseHeaderAdapter<Item, Component : AdapterAnkoComponent>(
        listener: (Item) -> Unit = {}) : BaseAdapter<Item, Component>(listener) {

    companion object {
        const val ITEM_HEADER = 4003
    }

    abstract fun onHeaderComponent(parent: ViewGroup): Component

    abstract fun onNormalComponent(parent: ViewGroup): Component

    override fun onCreateComponent(parent: ViewGroup, viewType: Int): Component = when (viewType) {
        ITEM_HEADER -> onHeaderComponent(parent)
        else -> onNormalComponent(parent)
    }

    override fun getItem(position: Int) = super.getItem(position - 1)

    override fun getItemViewType(position: Int) = when (position) {
        0 -> ITEM_HEADER
        else -> super.getItemViewType(position - 1)
    }

    override fun getItemCount() = super.getItemCount() + 1

}