package io.iost.hub.android.ui.view.account

import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import io.iost.hub.android.BuildConfig
import io.iost.hub.android.R
import io.iost.hub.android.extensions.drawableRight
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.aboutus.AboutUsActivity
import io.iost.hub.android.widget.hubToolbar
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource

class AboutUsComponent : ActivityAnkoComponent<AboutUsActivity> {

    override lateinit var toolbar: Toolbar

    override fun createView(ui: AnkoContext<AboutUsActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_about_us
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            textView(R.string.about_us_contact_us) {
                gravity = Gravity.CENTER_VERTICAL
                textSize = 14f
                textColorResource = R.color.black_030303
                drawableRight(R.drawable.account_next)
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                singleClick { owner.contactUs() }
            }.lparams(matchParent, dip(44))
            view {
                backgroundColorResource = R.color.gray_d8d8d8
            }.lparams(matchParent, dimen(R.dimen.divider))
            textView(R.string.about_us_privacy_policy) {
                gravity = Gravity.CENTER_VERTICAL
                textSize = 14f
                textColorResource = R.color.black_030303
                drawableRight(R.drawable.account_next)
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                singleClick { owner.policy() }
            }.lparams(matchParent, dip(44))
            view {
                backgroundColorResource = R.color.gray_d8d8d8
            }.lparams(matchParent, dimen(R.dimen.divider))
            textView(R.string.about_us_terms_conditions) {
                gravity = Gravity.CENTER_VERTICAL
                textSize = 14f
                textColorResource = R.color.black_030303
                drawableRight(R.drawable.account_next)
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
                singleClick { owner.terms() }
            }.lparams(matchParent, dip(44))
            view {
                backgroundColorResource = R.color.gray_d8d8d8
            }.lparams(matchParent, dimen(R.dimen.divider))
            linearLayout {
                textView(R.string.about_us_version) {
                    gravity = Gravity.CENTER_VERTICAL
                    textSize = 14f
                    textColorResource = R.color.black_030303
                }.lparams(wrapContent, matchParent)
                textView {
                    gravity = Gravity.CENTER_VERTICAL or Gravity.END
                    text = BuildConfig.VERSION_NAME
                    textSize = 14f
                    textColorResource = R.color.gray_999999
                }.lparams(width = 0, height = matchParent, weight = 1f)
                imageView {
                    imageResource = R.drawable.account_next
                }.lparams {
                    gravity = Gravity.CENTER_VERTICAL
                    leftMargin = dimen(R.dimen.dimen_16)
                }
                singleClick { owner.updateCheck() }
                setPadding(dimen(R.dimen.dimen_16), 0, dimen(R.dimen.dimen_16), 0)
            }.lparams(matchParent, dip(44))
            view {
                backgroundColorResource = R.color.gray_d8d8d8
            }.lparams(matchParent, dimen(R.dimen.divider))
            lparams(matchParent, matchParent)
        }
    }

}