package io.iost.hub.android.ui.contract.passport

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface LoginContract {

    interface View : BaseView {
        fun onLoginSuccess()
        fun onLoginFailure(errorMessage: String)
    }

    interface Presenter : BasePresenter<View> {
        fun login(phone: String, password: String)
    }
}