package io.iost.hub.android.ui.view.account

import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.dimenAttr
import io.iost.hub.android.extensions.drawable
import io.iost.hub.android.extensions.font
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.extensions.string
import io.iost.hub.android.ui.base.activity.ActivityAnkoComponent
import io.iost.hub.android.ui.feature.account.balance.BalanceActivity
import io.iost.hub.android.ui.feature.account.balance.BalanceWithdrawActivity
import io.iost.hub.android.widget.DividerItemView
import io.iost.hub.android.widget.hubToolbar
import io.iost.hub.android.widget.smartRefreshLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.recyclerview.v7.recyclerView


class BalanceComponent : ActivityAnkoComponent<BalanceActivity> {

    override lateinit var toolbar: Toolbar
    lateinit var recyclerView: RecyclerView
    lateinit var refreshLayout: SmartRefreshLayout
    lateinit var loadMoreLayout: SmartRefreshLayout
    lateinit var balanceCountTxt: TextView

    override fun createView(ui: AnkoContext<BalanceActivity>): View = with(ui) {
        verticalLayout {
            toolbar = hubToolbar {
                titleResource = R.string.account_balance
                imageView {
                    imageResource = R.drawable.balance_withdraw_icon
                    padding = dimen(R.dimen.dimen_16)
                    singleClick { startActivity<BalanceWithdrawActivity>() }
                }.lparams(dimenAttr(R.attr.actionBarSize), dimenAttr(R.attr.actionBarSize)) {
                    gravity = Gravity.END
                }
            }.lparams(width = matchParent, height = dimenAttr(R.attr.actionBarSize))
            refreshLayout = smartRefreshLayout {
                setEnableLoadMore(false)
                verticalLayout {
                    verticalLayout {
                        balanceCountTxt = textView {
                            textSize = 36f
                            setShadowLayer(2F, 2F, 2F, Color.LTGRAY)
                            font(string(R.string.font_heavy))
                            textColorResource = R.color.white
                        }.lparams {
                            topMargin = dimen(R.dimen.dimen_20)
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        textView(R.string.iost_upper) {
                            textSize = 30f
                            font(string(R.string.font_heavy))
                            textColorResource = R.color.white
                        }.lparams {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        button(R.string.account_withdraw) {
                            padding = 0
                            textSizeDimen = R.dimen.material_text_button
                            singleLine = true
                            backgroundResource = R.drawable.bg_yellow_shape
                            singleClick { owner.startWithdraw() }
                        }.lparams(dimen(R.dimen.button_width1), dimen(R.dimen.button_height1)) {
                            gravity = Gravity.CENTER_HORIZONTAL
                            topMargin = dimen(R.dimen.dimen_10)
                        }
                        textView(R.string.account_at_least_1000) {
                            gravity = Gravity.CENTER_HORIZONTAL
                            textColorResource = R.color.white
                            textSize = 10f
                        }.lparams(matchParent, wrapContent) {
                            topMargin = dimen(R.dimen.dimen_10)
                            bottomMargin = dimen(R.dimen.dimen_16)
                        }
                        backgroundResource = R.drawable.account_balance
                        lparams(matchParent, wrapContent)
                    }
                    loadMoreLayout = smartRefreshLayout {
                        setEnableRefresh(false)
                        recyclerView = recyclerView {
                            layoutManager = LinearLayoutManager(context)
                            addItemDecoration(DividerItemView(ctx, ctx.drawable(R.color.gray_e5e5e5), dimen(R.dimen.divider)).apply {
                                dividerPadding = dimen(R.dimen.dimen_8)
                                showLastItemDivider = false
                            })
                        }
                    }.lparams(matchParent, matchParent)
                }
            }.lparams(width = matchParent, height = 0, weight = 1f)

        }
    }
}