package io.iost.hub.android.di.module.passport

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.passport.ResetPasswordActivity
import io.iost.hub.android.ui.common.SmsCode
import io.iost.hub.android.ui.presenter.passport.ResetPasswordPresenter
import kotlinx.coroutines.experimental.Job

@Module
class ResetPasswordModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: ResetPasswordActivity, job: Job, api: HubApiService) =
            ResetPasswordPresenter(view, job, api)

    @ActivityScope
    @Provides
    fun provideSmsCode(view: ResetPasswordActivity, job: Job, api: HubApiService) = SmsCode(view, job, api)

}