package io.iost.hub.android.ui.feature.passport

import io.iost.hub.android.extensions.countdown
import io.iost.hub.android.ui.contract.passport.RegisterContract
import io.iost.hub.android.ui.presenter.passport.RegisterPresenter
import io.iost.hub.android.ui.common.SmsCode
import io.iost.hub.android.ui.view.passport.RegisterComponent
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class RegisterActivity : BasePassportActivity<RegisterPresenter, RegisterComponent>(),
        RegisterContract.View, SmsCode.SmsCodeCallback {

    override val ui: RegisterComponent = RegisterComponent()

    @Inject
    lateinit var smsCode: SmsCode

    fun onGetCode(phone: String) {
        smsCode.registerCallback(this)
        smsCode.getVerifyCode(phone, mAreaCode, SmsCode.REGISTER_TYPE)
    }

    fun onNextClick(phone: String, code: String, inviteCode: String) {
        mPresenter.next(phone, mAreaCode, code, inviteCode)
    }

    override fun onCountDown(isFinish: Boolean, time: Int) {
        ui.getCode.countdown(isFinish, time)
    }

    override fun onNext(phone: String, code: String, inviteCode: String) {
        startActivity<RegisterNextActivity>(LoginActivity.PHONE_NUMBER to phone,
                RegisterNextActivity.PHONE_CODE to code,
                RegisterNextActivity.AREA_CODE to mAreaCode,
                RegisterNextActivity.PHONE_INVITE to inviteCode)
    }

    override fun setCountryCode(countryArea: String) {
        ui.countryTxt.text = countryArea
    }

    override fun onDestroy() {
        super.onDestroy()
        smsCode.unRegisterCallback()
    }

}