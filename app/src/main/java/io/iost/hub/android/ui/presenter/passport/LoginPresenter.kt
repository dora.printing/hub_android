package io.iost.hub.android.ui.presenter.passport

import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.callApi
import io.iost.hub.android.model.DataResult
import io.iost.hub.android.ui.common.UserState
import io.iost.hub.android.ui.contract.passport.LoginContract
import kotlinx.coroutines.experimental.Job

class LoginPresenter(override val view: LoginContract.View, override val job: Job,
                     private val api: HubApiService, private var userState: UserState) : LoginContract.Presenter {

    override fun login(phone: String, password: String) {
        when {
            phone.isEmpty() -> view.toastMsg(R.string.passport_phone_empty)
            password.isEmpty() -> view.toastMsg(R.string.passport_password_empty)
            else -> {
                view.showLoadingDialog()
                callApi(job, api.login(phone, password), view) {
                    view.dismissLoading()
                    when (it) {
                        is DataResult -> {
                            userState.onLoginSuccess(it.data)
                            view.onLoginSuccess()
                        }
                    }
                }
            }
        }
    }

}