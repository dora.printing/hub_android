package io.iost.hub.android.ui.feature.news.banner

import android.support.v4.view.ViewPager
import android.widget.LinearLayout
import io.iost.hub.android.R
import org.jetbrains.anko.dimen


class BannerPagerListener(private val mPager: ViewPager, private val mSize: Int) :
        ViewPager.OnPageChangeListener {

    companion object {
        private const val NORMAL_SCROLL_STATE = 0
        private const val LEFT_SCROLL_STATE = -1
        private const val RIGHT_SCROLL_STATE = 1
    }

    private var mLastPosOff = 0f//以此来判断滚动状态
    private var mScrollState = 0//当前滚动状态，向左滚还是向右滚，默认静止
    private var mSideState = LEFT_SCROLL_STATE//当前的位置的状态，最左还是最右或者中间,默认在最左边
    private var mPageScrollState = 0//当前viewpager滑动状态，默认静止
    private val normalMargin = mPager.dimen(R.dimen.cardview_margin)
    private val sideSmallMargin = mPager.dimen(R.dimen.cardview_margin)
    private val mediumMargin = mPager.dimen(R.dimen.cardview_margin) * 3 / 2
    private val sideBigMargin = mPager.dimen(R.dimen.cardview_margin) * 2

    private val layoutParams = mPager.layoutParams as LinearLayout.LayoutParams

    init {
        if (mSize > 1) {
            layoutParams.setMargins(sideSmallMargin, normalMargin, sideBigMargin, normalMargin)
        } else {
            layoutParams.setMargins(mediumMargin, normalMargin, mediumMargin, normalMargin)
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if (mSize > 1 && positionOffset != 0f && positionOffset < 1) {
            when {
                positionOffset - mLastPosOff < 0 -> {
                    mScrollState = LEFT_SCROLL_STATE
                }
                positionOffset - mLastPosOff > 0 -> {
                    mScrollState = RIGHT_SCROLL_STATE
                }
            }
            mLastPosOff = positionOffset
            if (mPageScrollState == 2) //当手指离开屏幕后再做处理
                if (position == 0 && mScrollState == LEFT_SCROLL_STATE) {
                    if (mSideState != LEFT_SCROLL_STATE) {//防止多次设置margin
                        layoutParams.setMargins(sideSmallMargin, normalMargin, sideBigMargin, normalMargin)
                        mSideState = LEFT_SCROLL_STATE
                        mPager.layoutParams = layoutParams
                    }
                } else if ((position == mSize - 2 && mScrollState == RIGHT_SCROLL_STATE)) {
                    if (mSideState != RIGHT_SCROLL_STATE) {//防止多次设置margin
                        layoutParams.setMargins(sideBigMargin, normalMargin, sideSmallMargin, normalMargin)
                        mSideState = RIGHT_SCROLL_STATE
                        mPager.layoutParams = layoutParams
                    }
                } else {
                    if (mSideState != NORMAL_SCROLL_STATE) {//防止多次设置margin
                        layoutParams.setMargins(mediumMargin, normalMargin, mediumMargin, normalMargin)
                        mSideState = NORMAL_SCROLL_STATE
                        mPager.layoutParams = layoutParams
                    }
                }
        }
    }

    override fun onPageSelected(position: Int) {
    }


    override fun onPageScrollStateChanged(state: Int) {
        this.mPageScrollState = state
        when (state) {
            ViewPager.SCROLL_STATE_DRAGGING -> {
                mPager.layoutParams = layoutParams
            }
        }
    }
}
