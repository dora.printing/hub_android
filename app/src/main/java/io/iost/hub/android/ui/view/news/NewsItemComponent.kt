package io.iost.hub.android.ui.view.news

import android.graphics.Color
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.displayWidth
import io.iost.hub.android.extensions.font
import io.iost.hub.android.extensions.string
import io.iost.hub.android.ui.feature.news.NewsAdapter
import io.iost.hub.android.widget.NewsMediaView
import io.iost.hub.android.widget.newsMediaView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.support.v4.viewPager

class NewsItemComponent(override val view: ViewGroup) : NewsAdapter.Component() {

    lateinit var nameTxt: TextView
    lateinit var avatarImg: ImageView
    lateinit var infoTxt: TextView
    lateinit var timeTxt: TextView
    lateinit var titleTxt: TextView
    lateinit var contentTxt: TextView
    lateinit var typeImg: ImageView
    lateinit var mediaView: NewsMediaView
    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        cardView {
            lparams(matchParent) {
                leftMargin = dimen(R.dimen.cardview_margin)
                rightMargin = dimen(R.dimen.cardview_margin)
                bottomMargin = dimen(R.dimen.cardview_margin)
            }
            radius = dimen(R.dimen.cardview_radius).toFloat()
            cardElevation = dimen(R.dimen.cardview_elevation).toFloat()
            relativeLayout {
                avatarImg = imageView {
                    id = R.id.news_item_head
                }.lparams(dimen(R.dimen.dimen_32), dimen(R.dimen.dimen_32)) {
                    rightMargin = dimen(R.dimen.dimen_8)
                    leftMargin = dimen(R.dimen.dimen_8)
                    topMargin = dimen(R.dimen.dimen_8)
                }
                nameTxt = textView {
                    id = R.id.news_item_name
                    textSize = 14f
                    textColorResource = R.color.dark_red_3b0808
                }.lparams {
                    rightMargin = dip(50)
                    rightOf(R.id.news_item_head)
                    sameTop(R.id.news_item_head)
                }
                linearLayout {
                    id = R.id.news_item_info
                    infoTxt = textView {
                        textSize = 10f
                        textColorResource = R.color.gray_999999
                    }.lparams(width = 0, weight = 1f) {
                        rightMargin = dimen(R.dimen.dimen_16)
                    }
                    timeTxt = textView {
                        textSize = 10f
                        textColorResource = R.color.gray_999999
                    }.lparams(width = 0, weight = 1f)
                }.lparams(width = matchParent) {
                    rightMargin = dimen(R.dimen.dimen_16)
                    rightOf(R.id.news_item_head)
                    below(R.id.news_item_name)
                }
                titleTxt = textView {
                    id = R.id.news_item_title
                    textSize = 14f
                    font(string(R.string.font_medium))
                    textColorResource = R.color.black_333333
                }.lparams {
                    rightOf(R.id.news_item_head)
                    below(R.id.news_item_info)
                    topMargin = dimen(R.dimen.dimen_8)
                    rightMargin = dimen(R.dimen.dimen_16)
                }
                contentTxt = textView {
                    id = R.id.news_item_content
                    textSize = 13f
                    textColorResource = R.color.black_666666
                }.lparams {
                    rightOf(R.id.news_item_head)
                    below(R.id.news_item_title)
                    topMargin = dimen(R.dimen.dimen_8)
                    rightMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_8)
                }
                mediaView = newsMediaView().lparams(matchParent, 0) {
                    rightOf(R.id.news_item_head)
                    below(R.id.news_item_content)
                    rightMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_16)
                }
                typeImg = imageView {
                    scaleType = ImageView.ScaleType.FIT_START
                }.lparams(dip(50), dip(40)) {
                    alignParentRight()
                }
            }.lparams(matchParent)
        }
    }
}

class NewsHeaderComponent(override val view: ViewGroup) : NewsAdapter.Component() {

    lateinit var viewPager: ViewPager
    lateinit var layout: LinearLayout
    private var mWidth: Int = view.context.displayWidth - view.dimen(R.dimen.cardview_margin) * 3

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        layout = verticalLayout {
            lparams(matchParent)
            clipChildren = false
            viewPager = viewPager {
                pageMargin = dip(10)
                clipToPadding = false
                clipChildren = false
                offscreenPageLimit = 3
            }.lparams(mWidth, mWidth * 9 / 16) {
                topMargin = dimen(R.dimen.cardview_margin)
                bottomMargin = dimen(R.dimen.cardview_margin)
            }
            textView(R.string.news_latest) {
                textSize = 18f
                textColor = Color.BLACK
            }.lparams(wrapContent) {
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dip(10)
            }
        }
        layout
    }
}