package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.balance.BalanceActivity
import io.iost.hub.android.ui.presenter.account.BalancePresenter
import kotlinx.coroutines.experimental.Job

@Module
class BalanceModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: BalanceActivity, job: Job, api: HubApiService) = BalancePresenter(view, job, api)
}