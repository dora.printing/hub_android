package io.iost.hub.android.ui.contract

import android.content.Context
import android.support.annotation.StringRes

interface BaseView {
    val viewCtx: Context
    fun toastMsg(msg: String)
    fun toastMsg(@StringRes resId: Int)
    fun showLoadingDialog()
    fun dismissLoading()
    fun showLoadingLayout()
    fun onLoadError()
    fun onLoadSuccess()
    fun onLoginInvalid()
}
