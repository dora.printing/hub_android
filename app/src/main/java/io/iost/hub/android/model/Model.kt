package io.iost.hub.android.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class Country(
        @SerializedName("_id") val id: String,
        @SerializedName("Alpha3Code") val alpha3Code: String,
        @SerializedName("Name_en") val en: String,
        @SerializedName("Name_zh") val zh: String,
        @SerializedName("TelephoneCode") val code: String,
        @SerializedName("Flag") val flag: String,
        @SerializedName("Order") val order: String
)

data class Banner(
        val id: Int,
        val title: String,
        val converImage: String?,
        val link: String,
        val createdAt: String,
        val updatedAt: String)

data class NewsImage(
        val id: Int,
        val newsId: Int,
        val image: String,
        val createdAt: String,
        val updatedAt: String)

data class NewsData(
        val id: Int,
        val title: String,
        val converImage: String?,
        val link: String,
        val summary: String,
        val content: String,
        val type: Int,
        val handle: String,
        val authorAvatar: String?,
        val date: String,
        val createdAt: String,
        val updatedAt: String,
        val images: List<NewsImage>?)

data class IncentivesData(
        val title: String,
        val content: String,
        val state: Int,
        val resId: Int)

data class CheckInData(
        val checkins: List<CheckInItemData>,
        val canCheck: Boolean,
        val earns: List<Int>)

data class CheckIn(
        val reward: Int)

data class CheckInItemData(
        val id: Int,
        val userId: Int,
        val checkDate: String,
        val continueDay: Int,
        val createdAt: String,
        val updatedAt: String)

@Parcelize
data class DailyData(
        val id: Int,
        val name: String,
        val detail: String,
        val howTo: String,
        val rules: String,
        val shortCut: String,
        val shortCutBtn: String,
        val bonus: Int,
        val bonusType: Int,
        val coinBonus: Int,
        val jobFrequecy: Int,//可执行任务次数：1的话只能执行一次，不是1每天都能执行一次
        val currentFinished: Int,
        val totalCount: Int,
        val userId: Int,
        var status: Int,
        val type: Int,//任务类型
        val createdAt: String,
        val updatedAt: String) : Parcelable

data class DailyCardData(
        @SerializedName("TotalShare") val totalShare: Int,
        val pointsEarnedToday: Float,
        val ranking: Int,
        val estimatedRewardIost: Float,
        val lifetimeReward: Float)

data class DrawTaskData(val id: Int)

data class ReferralData(
        val areaCode: String,
        val phone: String,
        val createdAt: String,
        val answersAllRight: Int)

data class ReferralRewardData(
        val inviteNum: Int,
        val inviteAmount: Int,
        val inviteReward: ReferralInviteRewardData)

data class ReferralInviteRewardData(
        @SerializedName("Register") val register: Int,
        @SerializedName("Invite") val invite: Int)

data class BalanceData(
        val balance: Float,
        val readAll: Boolean)

data class BalanceItems(val items: List<BalanceItemData>)

data class BalanceItemData(
        val id: Int,
        val userId: Int,
        val amount: Float,
        val balance: String,
        val taskType: Int,
        val desc: String,
        val status: Int,
        val t: Int,
        val isDelete: Int,
        val drawed: Boolean,
        val createdAt: String,
        val updatedAt: String)

data class DrawAbleAmount(val amount: Float)

data class SocialData(
        val platform: String,
        val binded: Boolean,
        val enable: Boolean,
        val userName: String)

data class PosterData(
        val text: String,
        val image: String)

data class User(
        val id: Int,
        val name: String,
        val email: String?,
        val balance: Double,
        val password: String,
        val areaCode: String,
        val phone: String,
        val code: String,
        val inviterCode: String?,
        val isTwitterBind: Boolean,
        val isGithubBind: Boolean,
        val isTelegramBind: Boolean,
        val isRedditBind: Boolean,
        val drawDayLimit: Int,
        val token: String)

data class UserInfo(
        val id: Int,
        val name: String?,
        val email: String?,
        val balance: Int,
        val areaCode: String,
        val phone: String,
        val code: String,
        val codeTimes: Int,
        val codeUsedTimes: Int,
        val inviterCode: String?,
        val ethAddress: String?,
        val drawDayLimit: Int,
        val answersAllRight: Boolean,
        val isDelete: Int,
        val createdAt: String,
        val updatedAt: String,
        val questAnswers: Boolean,
        val estimateReward: Int,
        val estimateInvite: Int)

data class AnswerData(
        val questResult: List<Boolean>,
        val rewardIostNum: Int)

data class Version(
        val version: String,
        val versionCode: Int,
        val updateContent: String,
        val url: String,
        val size: Float,
        val md5: String,
        val forceUpgrade: Boolean = false)

data class ConfigPage(
        val contact: String,
        val policy: String,
        val terms: String,
        val support: String)

data class CountryList(var countries: List<Country>)

data class GithubToken(val access_token: String,
                       val token_type: String,
                       val scope: String)

data class GithubUser(val login: String,
                      val id: String,
                      val name: String?)

data class RedditToken(val access_token: String,
                       val token_type: String,
                       val expires_in: String,
                       val scope: String,
                       val refresh_token: String)

data class RedditUser(val id: String,
                      val name: String)