package io.iost.hub.android.ui.view.news

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.iost.hub.android.ui.base.fragment.FragmentAnkoComponent
import io.iost.hub.android.widget.smartRefreshLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.verticalLayout

class NewsComponent : FragmentAnkoComponent {
    lateinit var recyclerView: RecyclerView
    lateinit var refreshLayout: SmartRefreshLayout
    override fun createView(ui: AnkoContext<FrameLayout>): View = with(ui) {
        verticalLayout {
            refreshLayout = smartRefreshLayout {
                recyclerView = recyclerView {
                    layoutManager = LinearLayoutManager(context)
                }
            }.lparams(matchParent, matchParent)
            lparams(matchParent, matchParent)
        }
    }
}