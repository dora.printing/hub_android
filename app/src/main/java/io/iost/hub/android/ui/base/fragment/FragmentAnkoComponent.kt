package io.iost.hub.android.ui.base.fragment

import android.widget.FrameLayout
import org.jetbrains.anko.AnkoComponent

interface FragmentAnkoComponent : AnkoComponent<FrameLayout>