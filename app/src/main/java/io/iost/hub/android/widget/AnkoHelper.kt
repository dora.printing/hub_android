package io.iost.hub.android.widget

import android.content.Context
import android.support.v4.app.Fragment
import android.view.ViewManager
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.iost.hub.android.R
import io.iost.hub.android.extensions.colorAttr
import io.iost.hub.android.ui.feature.account.PosterDialog
import io.iost.hub.android.ui.feature.account.balance.WithdrawSmsDialog
import io.iost.hub.android.ui.feature.daily.BerminalDialog
import io.iost.hub.android.ui.feature.daily.ShareDialog
import io.iost.hub.android.ui.feature.navigation.QaDialog
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7._Toolbar
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dimenAttr
import org.jetbrains.anko.support.v4.ctx

inline fun ViewManager.newsMediaView(theme: Int = 0) = newsMediaView(theme) {}
inline fun ViewManager.newsMediaView(theme: Int = 0, init: NewsMediaView.() -> Unit) = ankoView({ NewsMediaView(it) }, theme, init)

inline fun ViewManager.smartRefreshLayout(theme: Int = 0) = smartRefreshLayout(theme) {}
inline fun ViewManager.smartRefreshLayout(theme: Int = 0, init: SmartRefreshLayout.() -> Unit) = ankoView({ SmartRefreshLayout(it) }, theme, init)

inline fun ViewManager.dailyCardView(theme: Int = 0) = dailyCardView(theme) {}
inline fun ViewManager.dailyCardView(theme: Int = 0, init: DailyCardView.() -> Unit) = ankoView({ DailyCardView(it) }, theme, init)

inline fun ViewManager.dailyCheckInView(theme: Int = 0) = dailyCheckInView(theme) {}
inline fun ViewManager.dailyCheckInView(theme: Int = 0, init: DailyCheckInView.() -> Unit) = ankoView({ DailyCheckInView(it) }, theme, init)

inline fun ViewManager.hubToolbar(theme: Int = R.style.ThemeOverlay_AppCompat_Dark) = hubToolbar {}
inline fun ViewManager.hubToolbar(theme: Int = R.style.ThemeOverlay_AppCompat_Dark, init: _Toolbar.() -> Unit) = themedToolbar(theme = theme, init = init).apply {
    backgroundColor = colorAttr(R.attr.colorPrimary)
    minimumHeight = dimenAttr(R.attr.actionBarSize)
}

inline fun AnkoContext<*>.shareAlert(isSuccess: Boolean, noinline init: ShareDialog.() -> Unit) = ctx.shareAlert(isSuccess, init)
inline fun Fragment.shareAlert(isSuccess: Boolean, noinline init: ShareDialog.() -> Unit) = ctx.shareAlert(isSuccess, init)
fun Context.shareAlert(isSuccess: Boolean, init: ShareDialog.() -> Unit): ShareDialog = ShareDialog(isSuccess, this).apply { init() }

inline fun AnkoContext<*>.posterAlert(noinline init: PosterDialog.() -> Unit) = ctx.posterAlert(init)
inline fun Fragment.posterAlert(noinline init: PosterDialog.() -> Unit) = ctx.posterAlert(init)
fun Context.posterAlert(init: PosterDialog.() -> Unit): PosterDialog = PosterDialog(this).apply { init() }

inline fun AnkoContext<*>.withdrawSmsAlert(noinline init: WithdrawSmsDialog.() -> Unit) = ctx.withdrawSmsAlert(init)
inline fun Fragment.withdrawSmsAlert(noinline init: WithdrawSmsDialog.() -> Unit) = ctx.withdrawSmsAlert(init)
fun Context.withdrawSmsAlert(init: WithdrawSmsDialog.() -> Unit): WithdrawSmsDialog = WithdrawSmsDialog(this).apply { init() }

inline fun AnkoContext<*>.qaAlert(noinline init: QaDialog.() -> Unit) = ctx.qaAlert(init)
inline fun Fragment.qaAlert(noinline init: QaDialog.() -> Unit) = ctx.qaAlert(init)
fun Context.qaAlert(init: QaDialog.() -> Unit): QaDialog = QaDialog(this).apply { init() }

inline fun AnkoContext<*>.berminalAlert(noinline init: BerminalDialog.() -> Unit) = ctx.berminalAlert(init)
inline fun Fragment.berminalAlert(noinline init: BerminalDialog.() -> Unit) = ctx.berminalAlert(init)
fun Context.berminalAlert(init: BerminalDialog.() -> Unit): BerminalDialog = BerminalDialog(this).apply { init() }
