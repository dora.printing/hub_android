package io.iost.hub.android.extensions

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.support.annotation.*
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.TextView
import io.iost.hub.android.R
import org.jetbrains.anko.*

fun Context.attr(@AttrRes attribute: Int): TypedValue {
    val typed = TypedValue()
    ctx.theme.resolveAttribute(attribute, typed, true)
    return typed
}

//returns px
fun Context.dimenAttr(@AttrRes attribute: Int): Int = TypedValue.complexToDimensionPixelSize(attr(attribute).data, resources.displayMetrics)

//returns color
fun Context.colorAttr(@AttrRes attribute: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        resources.getColor(attr(attribute).resourceId, ctx.theme)
    } else {
        @Suppress("DEPRECATION")
        resources.getColor(attr(attribute).resourceId)
    }
}

inline val Context.displayWidth: Int
    get() = resources.displayMetrics.widthPixels
inline val Context.displayHeight: Int
    get() = resources.displayMetrics.heightPixels

fun Context.integer(@IntegerRes id: Int): Int = resources.getInteger(id)
fun Context.boolean(@BoolRes id: Int): Boolean = resources.getBoolean(id)
fun Context.color(@ColorRes id: Int): Int = ContextCompat.getColor(this, id)
fun Context.drawable(@DrawableRes id: Int): Drawable? = ContextCompat.getDrawable(this, id)

fun AnkoContext<*>.dimenAttr(@AttrRes attribute: Int): Int = ctx.dimenAttr(attribute)
fun AnkoContext<*>.colorAttr(@AttrRes attribute: Int): Int = ctx.colorAttr(attribute)
fun AnkoContext<*>.attribute(@AttrRes attribute: Int): TypedValue = ctx.attr(attribute)

fun Fragment.dimenAttr(@AttrRes attribute: Int): Int = requireActivity().dimenAttr(attribute)
fun Fragment.colorAttr(@AttrRes attribute: Int): Int = requireActivity().colorAttr(attribute)
fun Fragment.attr(@AttrRes attribute: Int): TypedValue = requireActivity().attr(attribute)

fun View.dimenAttr(@AttrRes attribute: Int): Int = context.dimenAttr(attribute)
fun View.colorAttr(@AttrRes attribute: Int): Int = context.colorAttr(attribute)
fun View.attr(@AttrRes attribute: Int): TypedValue = context.attr(attribute)

fun View.color(@ColorRes id: Int): Int = context.color(id)
fun View.string(@StringRes id: Int, vararg formatArgs: Any): String = context.resources.getString(id, *formatArgs)
fun View.resize(width: Int, height: Int) {
    layoutParams?.let {
        it.width = width
        it.height = height
        layoutParams = it
    }
}

fun View.setShapeBackgroundRes(@ColorRes startRes: Int, @ColorRes endRes: Int, @DimenRes radiusRes: Int = 0, orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.TOP_BOTTOM) {
    setShapeBackground(color(startRes), color(endRes), dimen(radiusRes), orientation)
}

fun View.setShapeBackground(startColor: Int, endColor: Int, radius: Int = 0, orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.TOP_BOTTOM) {
    backgroundDrawable = GradientDrawable(orientation, intArrayOf(startColor, endColor)).apply {
        cornerRadius = radius.toFloat()
    }
}

fun View.setRadiusBackgroundRes(@ColorRes colorRes: Int, @DimenRes radiusRes: Int, @ColorRes strokeColorRes: Int = R.color.transparent, strokeWidth: Int = dip(1)) {
    setRadiusBackground(color(colorRes), dimen(radiusRes), color(strokeColorRes), strokeWidth)
}

fun View.setRadiusBackground(color: Int, radius: Int, strokeColor: Int = Color.TRANSPARENT, strokeWidth: Int = dip(1)) {
    backgroundDrawable = GradientDrawable().apply {
        setColor(color)
        cornerRadius = radius.toFloat()
        if (strokeColor != Color.TRANSPARENT) {
            setStroke(strokeWidth, strokeColor)
        }
    }
}

fun View.singleClick(l: (android.view.View?) -> Unit) {
    setOnClickListener(SingleClickListener(l))
}

fun TextView.drawableRight(@DrawableRes id: Int, width: Int = 0, height: Int = 0) {
    context.drawable(id)?.let {
        it.setBoundsWithDefault(width, height)
        setCompoundDrawables(null, null, it, null)
    }
}

fun TextView.drawableLeft(@DrawableRes id: Int, width: Int = 0, height: Int = 0) {
    context.drawable(id)?.let {
        it.setBoundsWithDefault(width, height)
        setCompoundDrawables(it, null, null, null)
    }
}

fun Drawable.setBoundsWithDefault(width: Int = 0, height: Int = 0) {
    setBounds(0, 0, if (width == 0) {
        minimumWidth
    } else {
        width
    }, if (height == 0) {
        minimumHeight
    } else {
        height
    })
}

