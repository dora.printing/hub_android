package io.iost.hub.android.ui.base.fragment

import android.os.Bundle
import dagger.android.support.AndroidSupportInjection
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter
import javax.inject.Inject

abstract class BasePresenterFragment<P : BasePresenter<BaseView>, out UI : FragmentAnkoComponent> :
        BaseFragment<UI>() {
    @Inject
    lateinit var mPresenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }

}