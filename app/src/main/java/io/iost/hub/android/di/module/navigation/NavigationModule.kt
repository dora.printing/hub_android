package io.iost.hub.android.di.module.navigation

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.navigation.NavigationActivity
import io.iost.hub.android.ui.presenter.navigation.NavigationPresenter
import kotlinx.coroutines.experimental.Job

@Module
class NavigationModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: NavigationActivity, job: Job, api: HubApiService) = NavigationPresenter(view, job, api)
}