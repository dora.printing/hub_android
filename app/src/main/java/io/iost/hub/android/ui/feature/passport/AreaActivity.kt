package io.iost.hub.android.ui.feature.passport

import android.app.Activity
import android.content.Intent
import io.iost.hub.android.coroutines.jobOnUiThread
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.common.AreaManager
import io.iost.hub.android.ui.view.passport.AreaComponent
import javax.inject.Inject

class AreaActivity : BaseActivity<AreaComponent>() {

    companion object {
        const val COUNTRY_ID = "country_id"
        const val COUNTRY_CODE = "country_code"
    }

    override val ui: AreaComponent = AreaComponent()

    @Inject
    lateinit var areaManager: AreaManager

    override val isInject: Boolean = true

    override fun created() {
        val mAdapter = AreaAdapter {
            val intent = Intent()
            intent.putExtra(COUNTRY_ID, it.id)
            intent.putExtra(COUNTRY_CODE, it.code)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        ui.recyclerView.adapter = mAdapter
        jobOnUiThread {
            areaManager.getCountryList().await()?.let {
                mAdapter.items = it
            }
        }
    }

}