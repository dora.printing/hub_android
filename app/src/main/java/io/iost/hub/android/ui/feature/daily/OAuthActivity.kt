package io.iost.hub.android.ui.feature.daily

import com.orhanobut.logger.Logger
import io.iost.hub.android.R
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.coroutines.jobOnUiThread
import io.iost.hub.android.ui.base.activity.BaseActivity
import io.iost.hub.android.ui.view.daily.OAuthComponent
import okhttp3.Credentials
import javax.inject.Inject

class OAuthActivity : BaseActivity<OAuthComponent>() {

    private companion object {
        const val GITHUB_TOKEN_URL = "https://github.com/login/oauth/access_token"
        const val GITHUB_USER_URL = "https://api.github.com/user"
        const val GITHUB_TYPE = "github"
        const val REDDIT_TOKEN_URL = "https://www.reddit.com/api/v1/access_token"
        const val REDDIT_USER_URL = "https://oauth.reddit.com/api/v1/me"
        const val REDDIT_TYPE = "reddit"
        const val REDDIT_GRANT_TYPE = "authorization_code"
    }

    override val ui: OAuthComponent = OAuthComponent()

    override val isInject: Boolean = true

    @Inject
    lateinit var api: HubApiService

    override fun created() {
    }

    override fun onResume() {
        super.onResume()
        intent.data?.let {
            Logger.d(it)
            if (it.encodedPath.contains(GITHUB_TYPE) && it.queryParameterNames.contains("code")) {
                jobOnUiThread {
                    showLoadingLayout()
                    try {
                        val token = api.getGithubToken(GITHUB_TOKEN_URL,
                                getString(R.string.github_client_id),
                                getString(R.string.github_client_secret),
                                it.getQueryParameter("code")).await()
                        val user = api.getGithubUser(GITHUB_USER_URL,
                                token.access_token).await()
                        val data = api.socialBind(GITHUB_TYPE, user.id, user.login,
                                "", "", "", "").await()
                        Logger.d(data)
                    } catch (t: Throwable) {
                        Logger.d(t)
                    } finally {
                        dismissLoading()
                        finish()
                    }
                }
            }
            if (it.encodedPath.contains(REDDIT_TYPE) && it.queryParameterNames.contains("code")) {
                jobOnUiThread {
                    showLoadingLayout()
                    try {
                        val token = api.getRedditToken(REDDIT_TOKEN_URL,
                                Credentials.basic(getString(R.string.reddit_client_id), ""),
                                REDDIT_GRANT_TYPE,
                                it.getQueryParameter("code"),
                                getString(R.string.reddit_redirect_uri)).await()
                        val user = api.getRedditUser(REDDIT_USER_URL,
                                "bearer ${token.access_token}").await()
                        val bindData = api.socialBind(REDDIT_TYPE, user.id, user.name,
                                "", token.access_token, "", token.refresh_token).await()
                        Logger.d(bindData)
                    } catch (t: Throwable) {
                        Logger.d(t)
                    } finally {
                        dismissLoading()
                        finish()
                    }
                }
            }
        }
    }

}