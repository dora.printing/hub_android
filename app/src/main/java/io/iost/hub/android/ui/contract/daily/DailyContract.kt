package io.iost.hub.android.ui.contract.daily

import io.iost.hub.android.model.CheckInData
import io.iost.hub.android.model.DailyCardData
import io.iost.hub.android.model.DailyData
import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter
import io.iost.hub.android.ui.view.daily.DailyCheckInComponent

interface DailyContract {
    interface View : BaseView {
        fun showCheckIn(data: CheckInData)
        fun showData(list: List<DailyData>)
        fun showCardData(data: DailyCardData)
        fun onCountDown(time: String)
    }

    interface Presenter : BasePresenter<View> {
        fun checkInInfo()
        fun getDailyData()
        fun getDailyCardData()
        fun countDown()
        fun checkIn(component: DailyCheckInComponent)
    }
}