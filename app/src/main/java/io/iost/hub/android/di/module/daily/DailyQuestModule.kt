package io.iost.hub.android.di.module.daily

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.daily.DailyQuestActivity
import io.iost.hub.android.ui.presenter.daily.DailyQuestPresenter
import kotlinx.coroutines.experimental.Job

@Module
class DailyQuestModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: DailyQuestActivity, job: Job, api: HubApiService): DailyQuestPresenter = DailyQuestPresenter(view, job, api)

}