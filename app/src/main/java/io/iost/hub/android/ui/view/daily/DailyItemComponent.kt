package io.iost.hub.android.ui.view.daily

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import io.iost.hub.android.R
import io.iost.hub.android.extensions.font
import io.iost.hub.android.extensions.string
import io.iost.hub.android.ui.feature.daily.DailyAdapter
import io.iost.hub.android.widget.DailyCheckInView
import io.iost.hub.android.widget.dailyCheckInView
import org.jetbrains.anko.*

class DailyItemComponent(override val view: ViewGroup) : DailyAdapter.Component() {
    lateinit var backLayout: RelativeLayout
    lateinit var titleTxt: TextView
    lateinit var contentTxt: TextView
    lateinit var claimBtn: TextView

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        backLayout = relativeLayout {
            gravity = Gravity.CENTER_VERTICAL
            titleTxt = textView {
                id = R.id.daily_item_title
                textSize = 16f
                textColorResource = R.color.black_333333
            }.lparams {
                margin = dimen(R.dimen.dimen_16)
            }
            contentTxt = textView {
                textSize = 18f
                gravity = Gravity.CENTER_VERTICAL
                font(string(R.string.font_medium))
                textColorResource = R.color.black_333333
                singleLine = true
            }.lparams(wrapContent, dimen(R.dimen.button_height1)) {
                below(R.id.daily_item_title)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dimen(R.dimen.dimen_16)
            }
            claimBtn = button(R.string.iost_claim) {
                padding = 0
                textSizeDimen = R.dimen.material_text_button
                singleLine = true
                textColorResource = R.color.white
            }.lparams(dimen(R.dimen.button_width2), dimen(R.dimen.button_height1)) {
                below(R.id.daily_item_title)
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dimen(R.dimen.dimen_16)
                alignParentRight()
            }
            lparams(matchParent, dip(120))
        }
        backLayout
    }
}

class DailyCheckInComponent(override val view: ViewGroup) : DailyAdapter.Component() {
    lateinit var layout: LinearLayout
    lateinit var checkInBtn: Button
    lateinit var checkInView: DailyCheckInView
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        layout = verticalLayout {
            backgroundColorResource = R.color.blue_e1f5ff
            gravity = Gravity.CENTER_VERTICAL
            linearLayout {
                textView(R.string.daily_check_in) {
                    textSize = 18f
                    textColorResource = R.color.black_333333
                }.lparams(width = 0, height = dimen(R.dimen.button_height1), weight = 2f) {
                    topMargin = dimen(R.dimen.dimen_16)
                    leftMargin = dimen(R.dimen.dimen_16)
                    bottomMargin = dimen(R.dimen.dimen_8)
                }
                checkInBtn = button(R.string.check_in) {
                    padding = 0
                    singleLine = true
                    textSizeDimen = R.dimen.material_text_button
                    textColorResource = R.color.white
                    backgroundResource = R.drawable.bg_blue_button
                }.lparams(width = dimen(R.dimen.button_width2), height = dimen(R.dimen.button_height1)) {
                    rightMargin = dimen(R.dimen.dimen_16)
                    gravity = Gravity.CENTER_VERTICAL
                }
            }.lparams(matchParent, wrapContent)
            checkInView = dailyCheckInView {}.lparams(matchParent, wrapContent) {
                leftMargin = dimen(R.dimen.dimen_16)
                rightMargin = dimen(R.dimen.dimen_16)
                bottomMargin = dimen(R.dimen.dimen_16)
            }
            lparams(matchParent, dip(120))
        }
        layout
    }
}