package io.iost.hub.android.ui.feature.incentives

import io.iost.hub.android.model.IncentivesData
import io.iost.hub.android.ui.base.fragment.BasePresenterFragment
import io.iost.hub.android.ui.contract.incentives.IncentivesContract
import io.iost.hub.android.ui.presenter.incentives.IncentivesPresenter
import io.iost.hub.android.ui.view.incentives.IncentivesComponent
import org.jetbrains.anko.support.v4.toast

class IncentivesFragment : BasePresenterFragment<IncentivesPresenter, IncentivesComponent>(), IncentivesContract.View {

    override val ui: IncentivesComponent = IncentivesComponent()

    private lateinit var mAdapter: IncentivesAdapter

    override fun activityCreated() {
        mAdapter = IncentivesAdapter {
            toast(it.title)
        }
        ui.recyclerView.adapter = mAdapter
    }

    override fun loadData(): Boolean {
        mPresenter.loadData()
        return true
    }

    override fun showData(list: List<IncentivesData>) {
        mAdapter.items = list
    }
}