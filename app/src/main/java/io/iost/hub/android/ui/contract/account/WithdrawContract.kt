package io.iost.hub.android.ui.contract.account

import io.iost.hub.android.ui.contract.BaseView
import io.iost.hub.android.ui.presenter.BasePresenter

interface WithdrawContract {
    interface View : BaseView {
        fun showDrawAbleAmount(amount: Float)
        fun onWithdrawSuccess()
    }

    interface Presenter : BasePresenter<View> {
        fun getDrawAbleAmount()
        fun drawIost(smsCode: String, transferAddress: String)
    }
}