package io.iost.hub.android.ui.feature.daily

import android.view.ViewGroup
import io.iost.hub.android.R
import io.iost.hub.android.extensions.singleClick
import io.iost.hub.android.model.CheckInData
import io.iost.hub.android.model.DailyData
import io.iost.hub.android.ui.base.adapter.AdapterAnkoComponent
import io.iost.hub.android.ui.base.adapter.BaseHeaderAdapter
import io.iost.hub.android.ui.common.DailyTask
import io.iost.hub.android.ui.view.daily.DailyCheckInComponent
import io.iost.hub.android.ui.view.daily.DailyItemComponent
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textResource
import kotlin.math.min

class DailyAdapter(
        private val checkInListener: (DailyCheckInComponent) -> Unit,
        private val itemBtnListener: (DailyData) -> Unit,
        itemListener: (DailyData) -> Unit) :
        BaseHeaderAdapter<DailyData, DailyAdapter.Component>(itemListener) {

    override val isShowEmptyView: Boolean = false

    var data: CheckInData? = null

    override val bind: Component.(position: Int) -> Unit = { position ->
        when (getItemViewType(position)) {
            ITEM_HEADER -> {
                if (this is DailyCheckInComponent && layout.tag == null) {
                    data?.let { item ->
                        if (item.earns.isNotEmpty()) {
                            checkInView.setDefaultLevels(item.earns.toTypedArray())
                            if (item.checkins.isNotEmpty()) {
                                checkInView.setLevels(item.earns[min(item.earns.size - 1, item.checkins.size - 1)])
                            }
                        }
                        checkInBtn.textResource = if (item.canCheck) {
                            R.string.check_in
                        } else {
                            R.string.iost_claimed
                        }
                        checkInBtn.isEnabled = item.canCheck
                        checkInBtn.singleClick { checkInListener(this) }
                        layout.tag = "header"
                    }
                }
            }
            ITEM_NORMAL -> {
                if (this is DailyItemComponent) {
                    getItem(position)?.let { item ->
                        setBackground(item)
                        titleTxt.text = item.detail
                        val bonus = (if (item.bonus > 0) "+${item.bonus} IOST${if (item.coinBonus > 0) ", " else ""}" else "") +
                                (if (item.coinBonus > 0) "+${item.coinBonus} CP" else "")
                        contentTxt.text = bonus
                        claimBtn.singleClick { itemBtnListener(item) }
                    }
                }
            }
        }
    }

    private fun DailyItemComponent.setBackground(item: DailyData) {
        when (item.status) {
            DailyTask.GO -> {
                //去完成
                backLayout.backgroundColorResource = R.color.transparent
                claimBtn.text = item.shortCutBtn
                claimBtn.isEnabled = true
                claimBtn.backgroundResource = R.drawable.bg_blue_shape
            }
            DailyTask.CLAIM -> {
                //完成 待领取
                backLayout.backgroundColorResource = R.color.yellow_fff2de
                claimBtn.textResource = R.string.iost_claim
                claimBtn.isEnabled = true
                claimBtn.backgroundResource = R.drawable.bg_yellow_shape
            }
            DailyTask.CLAIMED -> {
                //完成 已领取
                backLayout.backgroundColorResource = R.color.transparent
                claimBtn.textResource = R.string.iost_claimed
                claimBtn.isEnabled = false
                claimBtn.backgroundResource = R.drawable.bg_gray_button
            }
            else -> {
                backLayout.backgroundColorResource = R.color.transparent
                claimBtn.text = item.shortCutBtn
                claimBtn.isEnabled = false
                claimBtn.backgroundResource = R.drawable.bg_gray_button
            }
        }
    }

    override fun onHeaderComponent(parent: ViewGroup) = DailyCheckInComponent(parent)

    override fun onNormalComponent(parent: ViewGroup) = DailyItemComponent(parent)

    abstract class Component : AdapterAnkoComponent

}