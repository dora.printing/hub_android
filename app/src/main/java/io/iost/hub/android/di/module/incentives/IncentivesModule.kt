package io.iost.hub.android.di.module.incentives

import dagger.Module
import dagger.Provides
import io.iost.hub.android.di.FragmentScope
import io.iost.hub.android.di.module.FragmentModule
import io.iost.hub.android.ui.feature.incentives.IncentivesFragment
import io.iost.hub.android.ui.presenter.incentives.IncentivesPresenter
import kotlinx.coroutines.experimental.Job

@Module
class IncentivesModule : FragmentModule() {
    @FragmentScope
    @Provides
    fun providePresenter(view: IncentivesFragment, job: Job): IncentivesPresenter = IncentivesPresenter(view, job)

}
