package io.iost.hub.android.di.module.account

import dagger.Module
import dagger.Provides
import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.di.ActivityScope
import io.iost.hub.android.di.module.ActivityModule
import io.iost.hub.android.ui.feature.account.balance.BalanceWithdrawActivity
import io.iost.hub.android.ui.presenter.account.BalanceWithdrawPresenter
import kotlinx.coroutines.experimental.Job

@Module
class BalanceWithdrawModule : ActivityModule() {

    @ActivityScope
    @Provides
    fun providePresenter(view: BalanceWithdrawActivity, job: Job, api: HubApiService) = BalanceWithdrawPresenter(view, job, api)

}