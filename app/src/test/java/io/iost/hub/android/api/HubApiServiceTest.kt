package io.iost.hub.android.api

import io.iost.hub.android.coroutines.HubApiService
import io.iost.hub.android.model.BalanceData
import io.iost.hub.android.model.ResponseData
import io.iost.hub.android.model.Version
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class HubApiServiceTest {


    private val version = Version("0.0.1", 1, "must update", "downloadurl", 8.7f, "", false)


    @Mock
    private lateinit var hubApiService: HubApiService

    @Before
    fun initMocks() {
        given(hubApiService.getBalance()).willReturn(async {
            ResponseData<BalanceData>(1, null, "mock test wrong situation")
        })
        given(hubApiService.checkVersion()).willReturn(async {
            ResponseData<Version?>(0,
                    version,
                    "success")
        })
    }

    @Test
    fun testCheckVersion() {
        launch {
            assert(hubApiService.checkVersion().await().code == 0) {
                "code is not 0"
            }
        }
    }


}