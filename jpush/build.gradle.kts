plugins {
    id("com.android.library")
}

val config = HubConfiguration()

android {
    compileSdkVersion(config.android.compileSdkVersion)
    buildToolsVersion(config.android.buildToolsVersion)

    defaultConfig {
        minSdkVersion(config.android.minSdkVersion)
        targetSdkVersion(config.android.targetSdkVersion)
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"

        ndk {
            abiFilters("armeabi-v7a")
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    api("cn.jiguang.sdk:jpush:3.1.6")
    api("cn.jiguang.sdk:jcore:1.2.5")

    testImplementation("junit:junit:4.12")
    androidTestImplementation("com.android.support.test:runner:1.0.2")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
}
